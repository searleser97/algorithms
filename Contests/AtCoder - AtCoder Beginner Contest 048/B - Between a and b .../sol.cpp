// 27
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, (int)v.size()) o << setw(7) << left << v[i];
  return o << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 3

int sign(li x) { return x < 0 ? -1 : x > 0; }

//          a / b
li floor(li a, li b) {
  if (sign(a) != sign(b) && a % b)
    return a / b - 1;
  else
    return a / b;
}

//         a / b
li ceil(li a, li b) {
  if (sign(a) == sign(b) && a % b)
    return a / b + 1;
  else
    return a / b;
}

void _main(int tc) {
  li a, b, x;
  cin >> a >> b >> x;
  li leftLimit = x * ceil(a, x);
  li rightLimit = x * floor(b, x);
  // L + x * Z = R --> Z = (R - L) / x
  // Since L is multiple of x and R is also, we can add x to L, Z times
  // until we reach R.
  // therefore the distance (R - L) between multiples of x is always divisible
  // by x
  cout << 1 + (rightLimit - leftLimit) / x << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}