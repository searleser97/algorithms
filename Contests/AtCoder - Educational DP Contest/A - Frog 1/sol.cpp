// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  cin >> n;
  assert(n >= 2);
  vector<li> h(n);
  rep(i, n) cin >> h[i];
  
  /************* Recursive *************
  vector<li> mem(n, -1);
  mem[n - 1] = 0;
  mem[n - 2] = abs(h[n - 2] - h[n - 1]);
  function<li(li)> fcost = [&](int pos) -> li {
    if (mem[pos] != -1) return mem[pos];
    return mem[pos] = min(abs(h[pos] - h[pos + 1]) + fcost(pos + 1),
                          abs(h[pos] - h[pos + 2]) + fcost(pos + 2));
  };
  cout << fcost(0) << endl;
  *********** End Recursive ***********/

  vector<li> mem(n, -1);
  mem[n - 1] = 0;
  mem[n - 2] = abs(h[n - 2] - h[n - 1]);
  rrep(i, n - 2) {
    mem[i] = min(abs(h[i] - h[i + 1]) + mem[i + 1],
                 abs(h[i] - h[i + 2]) + mem[i + 2]);
  }
  cout << mem[0] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}