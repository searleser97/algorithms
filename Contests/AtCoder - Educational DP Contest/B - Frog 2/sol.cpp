// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
const int inf = 1e9 + 10;
void _main(int tc) {
  int n, k;
  cin >> n >> k;
  assert(n >= 2);
  vector<int> h(n);
  rep(i, n) cin >> h[i];
  
  /************* Recursive *************
  vector<int> mem(n, inf);
  mem[n - 1] = 0;
  mem[n - 2] = abs(h[n - 2] - h[n - 1]);

  function<int(int)> fcost = [&](int i) -> int {
    if (mem[i] != inf) return mem[i];
    forn(j, i + 1, (i + 1) + min(k, n - 1 - i)) {
      mem[i] = min(mem[i], abs(h[i] - h[j]) + fcost(j));
    }
    return mem[i];
  };
  cout << fcost(0) << endl;
  *********** End Recursive ***********/

  vector<int> mem(n, inf);
  mem[n - 1] = 0;
  mem[n - 2] = abs(h[n - 2] - h[n - 1]);
  rrep(i, n - 2) {
    forn(j, i + 1, (i + 1) + min(k, n - 1 - i)) {
      mem[i] = min(mem[i], abs(h[i] - h[j]) + mem[j]);
    }
  }
  cout << mem[0] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}