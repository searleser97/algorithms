// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  cin >> n;
  vector<vector<int>> abc(n, vector<int>(3));
  rep(i, n) {
    cin >> abc[i][0] >> abc[i][1] >> abc[i][2];
  }

  /************* Recursive *************
  vector<vector<int>> mem(n, vector<int>(3));
  mem[n - 1][0] = abc[n - 1][0];
  mem[n - 1][1] = abc[n - 1][1];
  mem[n - 1][2] = abc[n - 1][2];
  function<int(int, int)> fhappy = [&](int i, int k) -> int {
    if (mem[i][k] != 0) return mem[i][k];
    rep(j, 3) {
      if (k != j) mem[i][k] = max(mem[i][k], abc[i][k] + fhappy(i + 1, j));
    }
    return mem[i][k];
  };
  auto ans = max({fhappy(0, 0), fhappy(0, 1), fhappy(0, 2)});
  cout << ans << endl;
  *********** End Recursive ***********/

  vector<vector<int>> mem(n, vector<int>(3));
  mem[n - 1][0] = abc[n - 1][0];
  mem[n - 1][1] = abc[n - 1][1];
  mem[n - 1][2] = abc[n - 1][2];
  rrep(i, n - 1) {
    mem[i][0] = abc[i][0] + max(mem[i + 1][1], mem[i + 1][2]);
    mem[i][1] = abc[i][1] + max(mem[i + 1][0], mem[i + 1][2]);
    mem[i][2] = abc[i][2] + max(mem[i + 1][0], mem[i + 1][1]);
  }
  auto ans = *max_element(all(mem[0]));
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}