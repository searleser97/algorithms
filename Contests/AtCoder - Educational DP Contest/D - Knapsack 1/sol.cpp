// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
const li inf = 1 << 60;

void _main(int tc) {
  li n, W;
  cin >> n >> W;
  vector<int> w(n), v(n);
  rep(i, n) { cin >> w[i] >> v[i]; }

  /************* Recursive *************
  vector<vector<li>> mem(n, vector<li>(W + 1, -1));

  function<li(int, int)> F = [&](int i, int k) -> li {
    if (i == n) return 0;
    if (mem[i][k] != -1) return mem[i][k];
    if (k - w[i] >= 0)
      mem[i][k] = max(v[i] + F(i + 1, k - w[i]), F(i + 1, k));
    else
      mem[i][k] = F(i + 1, k);
    return mem[i][k];
  };
  cout << F(0, W) << endl;
  *********** End Recursive ***********/

  vector<vector<li>> mem(n + 1, vector<li>(W + 1, 0));
  rrep(i, n) {
    rrep(k, W + 1) {
      if (k - w[i] >= 0)
        mem[i][k] = max(v[i] + mem[i + 1][k - w[i]], mem[i + 1][k]);
      else
        mem[i][k] = mem[i + 1][k];
    }
  }
  cout << mem[0][W] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}