// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
#define lli __int128_t
#define li long long int
#define ld long double
const li inf = 1e14;
// 12
void _main(int tc) {
  int n, W, sumV = 0;
  cin >> n >> W;
  vector<int> w(n), v(n);
  rep(i, n) {
    cin >> w[i] >> v[i];
    sumV += v[i];
  }

  /************* Recursive *************
  vector<vector<li>> mem(n, vector<li>(sumV + 1, -1));

  function<li(int, int)> F = [&](int i, int V) -> li {
    if (V == 0) return 0;
    if (i == n && V != 0) return inf;
    if (mem[i][V] != -1) return mem[i][V];
    if (V - v[i] >= 0)
      mem[i][V] = min(w[i] + F(i + 1, V - v[i]), F(i + 1, V));
    else
      mem[i][V] = F(i + 1, V);
    return mem[i][V];
  };

  rrep(i, sumV + 1) {
    if (F(0, i) <= W) {
      cout << i << endl;
      break;
    }
  }
  *********** End Recursive ***********/

  vector<vector<li>> mem(n + 1, vector<li>(sumV + 1, inf));

  rep(i, n + 1) mem[i][0] = 0;

  rrep(i, n) {
    rep(V, sumV + 1) {
      if (V - v[i] >= 0)
        mem[i][V] = min(w[i] + mem[i + 1][V - v[i]], mem[i + 1][V]);
      else
        mem[i][V] = mem[i + 1][V];
    }
  }

  rrep(i, sumV + 1) {
    if (mem[0][i] <= W) {
      cout << i << endl;
      break;
    }
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}