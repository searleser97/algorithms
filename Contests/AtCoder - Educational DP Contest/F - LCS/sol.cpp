// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#ifdef LOCAL
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" s << "\033[0m"
#else
#define cerr(s)
#endif
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

void _main(int tc) {
  string s, t;
  cin >> s >> t;

  /************* Recursive *************
  vector<vector<int>> mem(s.size() + 1, vector<int>(t.size() + 1, -1));

  function<int(int, int)> lcs = [&](int i, int j) -> int {
    if (i == s.size() or j == t.size()) return 0;
    if (mem[i][j] != -1) return mem[i][j];
    if (s[i] == t[j])
      return mem[i][j] = 1 + lcs(i + 1, j + 1);
    else
      return mem[i][j] = max(lcs(i + 1, j), lcs(i, j + 1));
  };

  int i = 0, j = 0;
  string ans;
  ans.reserve(lcs(0, 0));
  while (i < s.size() && j < t.size()) {
    if (s[i] == t[j])
      ans += s[i], i++, j++;
    else if (mem[i + 1][j] > mem[i][j + 1])
      i++;
    else
      j++;
  }
  std::cout << ans << endl;
  *********** End Recursive ***********/

  vector<vector<int>> mem(s.size() + 1, vector<int>(t.size() + 1));

  rrep(i, s.size()) {
    rrep(j, t.size()) {
      if (s[i] == t[j])
        mem[i][j] = 1 + mem[i + 1][j + 1];
      else
        mem[i][j] = max(mem[i + 1][j], mem[i][j + 1]);
    }
  }

  mem[s.size()] = vector<int>(t.size(), -1);
  rep(i, s.size()) mem[i][t.size()] = -1;

  int i = 0, j = 0;
  string ans;
  ans.reserve(mem[0][0]);
  while (i < s.size() && j < t.size()) {
    if (s[i] == t[j])
      ans += s[i], i++, j++;
    else if (mem[i + 1][j] > mem[i][j + 1])
      i++;
    else
      j++;
  }
  std::cout << ans << endl;

#ifdef LOCAL
  cerr(<< setw(4) << " ");
  rep(i, t.size()) cerr(<< setw(4) << t[i]);
  cerr(<< endl);
  rep(i, s.size() + 1) {
    if (i < s.size())
      cerr(<< setw(4) << s[i]);
    else
      cerr(<< setw(4) << " ");
    rep(j, t.size() + 1) cerr(<< setw(4) << mem[i][j]);
    cerr(<< endl);
  }
#endif
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}