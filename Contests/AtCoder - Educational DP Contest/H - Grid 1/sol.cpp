// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#ifdef LOCAL
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" s << "\033[0m"
#else
#define cerr(s)
#endif
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
const int mod = 1e9 + 7;
// 12
void _main(int tc) {
  int h, w;
  cin >> h >> w;
  vector<string> mat(h);
  rep(i, h) cin >> mat[i];

  /************* Recursive *************
  vector<vector<int>> mem(h, vector<int>(w));
  mem[h - 1][w - 1] = 1;
  const vector<pairii> moves = {{0, 1}, {1, 0}};

  function<int(int, int)> f = [&](int i, int j) {
    if (i == h or j == w) return 0;
    if (mat[i][j] == '#') return 0;
    if (mat[i][j] == 'v') return mem[i][j];
    mat[i][j] = 'v';
    for (auto& mv : moves) {
      int new_i = i + mv.fi, new_j = j + mv.se;
      mem[i][j] += f(new_i, new_j);
      if (mem[i][j] >= mod) mem[i][j] -= mod;
    }
    return mem[i][j];
  };
  
  cout << f(0, 0) << endl;
  *********** End Recursive ***********/

  vector<vector<int>> mem(h, vector<int>(w));
  mem[h - 1][w - 1] = 1;
  const vector<pairii> moves = {{0, 1}, {1, 0}};
  rrep(i, h) {
    rrep(j, w) {
      for (auto& mv : moves) {
        int new_i = i + mv.fi, new_j = j + mv.se;
        if (new_i < h && new_j < w && mat[i][j] == '.') {
          mem[i][j] += mem[new_i][new_j];
          if (mem[i][j] >= mod) mem[i][j] -= mod;
        }
      }
    }
  }
  cout << mem[0][0] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}