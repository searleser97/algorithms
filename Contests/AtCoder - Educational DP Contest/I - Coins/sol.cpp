// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &os, pair<A, B> &p) {
  return os << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &os, vector<T> &v) {
  rep(i, (int)v.size()) os << setw(7) << left << v[i];
  return os << endl;
}
// 6
template <class A, class B>
ostream &operator<<(ostream &os, map<A, B> &m) {
  for (auto &kv : m) os << "{" << kv.fi << " " << kv.se << "}";
  return os;
}
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<ld> p(n);
  rep(i, n) cin >> p[i];
  
  /************* Recursive *************
  vector<vector<ld>> mem(n, vector<ld>(n + 1, -1));
  function<ld(int, int)> f = [&](int i, int c) -> ld {
    if (n - i + c <= i - c) return 0;
    if (i == n) return 1;
    if (mem[i][c] != -1) return mem[i][c];
    return mem[i][c] = p[i] * f(i + 1, c) + (1 - p[i]) * f(i + 1, c - 1);
  };
  cout << setprecision(10) << f(0, n) << endl;
  *********** End Recursive ***********/

  vector<vector<ld>> mem(n + 1, vector<ld>(n + 1));

  int mid = n & 1 ? (n + 1) / 2 : n / 2 + 1;
  forn(i, mid, n + 1) mem[n][i] = 1;

  rrep(i, n) {
    forn(c, mid, n + 1) {
      mem[i][c] = p[i] * mem[i + 1][c] + (1 - p[i]) * mem[i + 1][c - 1];
    }
  }
  
  cout << setprecision(10) << mem[0][n] << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}