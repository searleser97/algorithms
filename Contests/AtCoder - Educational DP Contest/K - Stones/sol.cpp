// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &os, pair<A, B> &p) {
  return os << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &os, vector<T> &v) {
  rep(i, (int)v.size()) os << setw(7) << left << v[i];
  return os << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &os, map<A, B> &m) {
  for (auto &kv : m) os << kv;
  return os;
}
// 3
void _main(int tc) {
  int n, k;
  cin >> n >> k;
  vector<int> a(n);
  rep(i, n) cin >> a[i];
  
  /************* Recursive *************
  vector<int> isWinning(k + 1, -1);
  function<bool(int)> f = [&](int stones) -> bool {
    if (stones == 0) return 0;
    if (isWinning[stones] != -1) return isWinning[stones];
    for (int& x : a) {
      if (stones - x >= 0 && !f(stones - x)) {
        isWinning[stones] = 1;
      }
    }
    if (isWinning[stones] == -1) isWinning[stones] = 0;
    return isWinning[stones];
  };
  cout << (f(k) ? "First" : "Second") << endl;
  *********** End Recursive ***********/

  vector<bool> isWinning(k + 1);
  rep(stones, k + 1) {
    for (int &x : a) {
      if (stones - x >= 0 && !isWinning[stones - x]) isWinning[stones] = true;
    }
  }
  cout << (isWinning[k] ? "First" : "Second") << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}