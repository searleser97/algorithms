// 21
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
const int mod = 1e9 + 7;

// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

template <class A, class B>
void addSelf(A &a, B &b) {
  a += b;
  if (a >= mod) a -= mod;
}

template <class A, class B>
void subSelf(A &a, B &b) {
  a -= b;
  if (a < 0) a += mod;
}

// 3
void _main(int tc) {
  int n, K;
  cin >> n >> K;
  vector<int> v(n);
  rep(i, n) cin >> v[i];
  /************* Recursive ************* O(N*K*K) too slow
  vector<vector<li>> mem(n, vector<li>(K + 1, -1));
  function<int(int, int)> f = [&](int i, int k) -> int {
    if (i == n && k != 0) return 0;
    if (k < 0) return 0;
    if (i == n && k == 0) return 1;
    if (mem[i][k] != -1) return mem[i][k];
    li limit = min(k, v[i]);
    li ans = f(i + 1, k - limit);
    rep(given, limit - 1, -1) { ans = (ans + f(i + 1, k - given)) % mod; }
    return mem[i][k] = ans;
  };
  cout << f(0, K) << endl;
  *********** End Recursive ***********/

  /************* Iterative ************* O(N*K*K) too slow still
  vector<vector<int>> mem(n + 1, vector<int>(K + 1));
  rep(i, n + 1) mem[i][0] = 1;
  rep(i, n - 1, -1) {
    rep(rest, K + 1) {
      mem[i][rest] = 0;
      rep(given, min(v[i], rest) + 1) {
        addSelf(mem[i][rest], mem[i + 1][rest - given]);
      }
    }
  }
  cout << mem[0][K] << endl;
  *********** End Iterative ***********/

  /* ************ Iterative Prettier ************* O(N*K*K) too slow still
  vector<vector<int>> mem(n + 1, vector<int>(K + 1));
  rep(i, n + 1) mem[i][0] = 1;
  rep(i, n - 1, -1) {
    rep(rest, K + 1) {
      mem[i][rest] = 0;
      int left = rest - min(v[i], rest);
      int right = rest;
      // mem[i][rest] = sum(mem[i + 1][left] + ... + mem[i + 1][right])
      rep(newRest, left, right + 1) {
        addSelf(mem[i][rest], mem[i + 1][newRest]);
      }
    }
  }
  cout << mem[0][K] << endl;
  *********** End Iterative Prettier ********** */

  vector<vector<int>> mem(n + 1, vector<int>(K + 1));
  rep(i, n + 1) mem[i][0] = 1;
  rep(i, n - 1, -1) {
    vector<int> pref = mem[i + 1];
    rep(rest, 1, K + 1) { addSelf(pref[rest], pref[rest - 1]); }
    rep(rest, K + 1) {
      mem[i][rest] = 0;
      int left = rest - min(v[i], rest);
      int right = rest;
      mem[i][rest] = pref[right];
      if (left > 0) subSelf(mem[i][rest], pref[left - 1]);
    }
  }
  cout << mem[0][K] << endl;
}
// 7
int main() {
  // ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
