from math import *

n = int(input())
suma = 0
sumb = 0
for i in range(n):
  t, a = map(int, input().split())
  if i == 0:
    suma = t
    sumb = a
  else:
    multi = max(ceil(suma / t), ceil(sumb / a))
    suma = t * multi
    sumb = a * multi

print(str(suma + sumb))