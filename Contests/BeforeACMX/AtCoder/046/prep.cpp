using namespace std;
typedef long long int li;
typedef long double ld;
int sign(li x) { return x < 0 ? -1 : x > 0; }
li ceil(li a, li b) {
  if (sign(a) == sign(b) && a % b) return a / b + 1;
  else return a / b;
}
void _main(int tc) {
  int n;
  cin >> n;
  li sumt = 0, suma = 0;
  for (int i = 0; i < n; i += 1) {
    li t, a;
    cin >> t >> a;
    if (i == 0) sumt = t, suma = a;
    else {
      li m1 = ceil(sumt, t);
      li m2 = ceil(suma , a);
      li multi = max(m1, m2);
      sumt = t * multi, suma = a * multi;
    }
  }
  cout << sumt + suma << '\n';
}
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  for (int i = 0; i < tc; i += 1) _main(i + 1);
}
