#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

struct SuffixAutomaton {
  vector<int> len, link, isClone, first;
  vector<map<char, int>> next;
  int size, last;
  void init(int n) {
    first = isClone = len = link = vector<int>(2 * n);
    next.resize(2 * n);
    len[0] = 0, link[0] = -1, size = 1, last = 0;
  }
  SuffixAutomaton(const string& s) {
    init(s.size());
    for (const auto& c : s) add(c);
  }
  void add(const char& c) {
    int p = last, u = size++;
    len[u] = len[p] + 1, first[u] = len[p];
    while (p != -1 && !next[p].count(c))
      next[p][c] = u, p = link[p];
    if (p == -1) link[u] = 0;
    else {
      int q = next[p][c];
      if (len[p] + 1 == len[q]) link[u] = q;
      else {
        int clone = size++;
        first[clone] = first[q];
        len[clone] = len[p] + 1, isClone[clone] = 1;
        link[clone] = link[q], next[clone] = next[q];
        while (p != -1 && next[p][c] == q)
          next[p][c] = clone, p = link[p];
        link[q] = link[u] = clone;
      }
    }
    last = u;
  }
  unordered_set<int> getTerminals() {
    unordered_set<int> terminals;
    for (int p = last; p; p = link[p])
      terminals.insert(p);
    return terminals;
  }
};

void _main(int tc, SuffixAutomaton &sa) {
  string p;
  cin >> p;
  int node = 0;
  auto terminals = sa.getTerminals();
  bool wasTerminal = 0;
  rep(i, p.size()) {
    if (sa.next[node].count(p[i])) {
      node = sa.next[node][p[i]];
    } else {
      cout << "NO" << endl;
      return;
    }
    if (terminals.count(node)) {
      node = 0;
    }

  }
  cout << "YES" << endl;
}

int main() {
  SuffixAutomaton sa("abcdefghijklmnopqrstuvwxyz");
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1, sa);
}