#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

struct Interval {
  int isOne, left, right;
  bool operator<(const Interval & i) {
    return left == i.left ? right < i.right : left < i.left;
  }
};

void _main(int tc) {
  int n, m;
  cin >> n;
  int minN = 1 << 30, maxN = 0;
  rep(i, n) {
    int a, b;
    cin >> a >> b;
    minN = min(minN, b);
    maxN = max(maxN, a);
  }
  cin >> m;
  int minM = 1 << 30, maxM = 0;
  rep(i, m) {
    int a, b;
    cin >> a >> b;
    minM = min(minM, b);
    maxM = max(maxM, a);
  }

  bool f1 = false, f2 = false;
  int ans = 0;
  if (minN < maxM) {
    ans = maxM - minN;
    f1 = true;
  }
  if (minM < maxN) {
    ans = max(ans, maxN - minM);
    f2 = true;
  }
  if (f1 or f2) {
    cout << ans << endl;
    return;
  }
  if (!f1 && !f2)
    cout << 0 << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}