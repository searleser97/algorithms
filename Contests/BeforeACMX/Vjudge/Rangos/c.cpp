
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;

  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
  }

  inline T I(T a, T b) {
    return a - b;
  }

  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }

  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }

  T query(int l, int r) {
    return I(query(r), query(--l));
  }
};

struct Query {
  int l, r, pos;
  bool operator<(const Query& q) {
    return r < q.r;
  }
};

void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(i, n) {
    cin >> v[i];
  }

  int q;
  cin >> q;
  vector<Query> qs(q);
  rep(i, q) {
    cin >> qs[i].l >> qs[i].r;
    qs[i].l--, qs[i].r--;
    qs[i].pos = i;
  }
  sort(all(qs));
  vector<int> lastPos(1e6 + 5, -1);
  BIT<int> bit(n);
  int j = 0;
  vector<int> ans(q);
  for (auto& qi : qs) {
    while (j <= qi.r) {
      if (lastPos[v[j]] != -1) {
        bit.update(lastPos[v[j]], -1);
      }
      lastPos[v[j]] = j;
      bit.update(j, 1);
      j++;
    }
    ans[qi.pos] = bit.query(qi.l, qi.r);
  }
  rep(i, q) {
    cout << ans[i] << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}