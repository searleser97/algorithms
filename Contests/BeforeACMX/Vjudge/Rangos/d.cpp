#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

int msb(int n) {
  return 1 << (sizeof(n) * 8 - __builtin_clz(n) - 1);
}

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;

  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
  }

  inline T I(T a, T b) {
    return a - b;
  }

  void build() {
    for (int i = 1; i < bit.size(); i++) {
      int j = i + (i & -i);
      if (j < bit.size()) bit[j] = F(bit[j], bit[i]);
    }
  }

  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }

  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }

  T query(int l, int r) {
    return I(query(r), query(--l));
  }

  T lowerBound(T x) {
    T acum = neutro;
    int pos = 0;
    for (int i = msb(bit.size() - 1); i; i >>= 1)
      if ((pos | i) < bit.size() && F(acum, bit[pos | i]) < x)
        acum = F(acum, bit[pos |= i]);
    return pos;
  }

  T& operator[](int i) { return bit[++i]; }
};

int main() {
  int n, q;
  cin >> n;
  BIT<li> bit(n);
  rep(i, n) {
    cin >> bit[i];
  }
  int l = 0, r = n - 1;
  while (l <= r) {
    swap(bit[l], bit[r]);
    l++, r--;
  }

  bit.build();
  cin >> q;
  rep(i, q) {
    int type;
    cin >> type;
    if (type == 2) {
      li z;
      cin >> z;
      int pos = bit.lowerBound(z);
      li ans = bit.query(pos);
      if (pos == n) cout << "NO" << endl;
      else if (ans == z) {
        cout << "YES" << endl;
      } else
        cout << "NO" << endl;
    } else {
      int pos, x;
      cin >> pos >> x;
      int npos = n - pos;
      bit.update(npos, -bit.query(npos, npos) + x);
    }
    
  }
}
