
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

struct myhash {
  size_t operator()(uint64_t x) const {
    static const uint64_t FIXED_RANDOM = chrono::steady_clock::now().time_since_epoch().count();
    x ^= FIXED_RANDOM;
    return x ^ (x >> 16);
  }
};

template <class T> // don't pass n as param
vector<int> compress(vector<T>& v, int n = 0) {
  unordered_map<T, int, myhash> Map;
  set<T> s(v.begin(), v.end());
  vector<int> c(v.size());
  for (auto& e : s) Map[e] = n++;
  for (int i = 0; i < v.size(); i++)
    c[i] = Map[v[i]];
  return c;
}

struct Point {
  int x, y;
  bool operator<(const Point& p) {
    return x == p.x ? y < p.y : x < p.x;
  }
};

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;

  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
  }

  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }

  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }
};

void _main(int tc) {
  int n, k;
  cin >> n >> k;
  vector<int> X(n), Y(n);
  rep(i, n) {
    cin >> X[i] >> Y[i];
  }
  X = compress(X), Y = compress(Y);
  BIT<int> bit(n);
  vector<Point> pts(n);
  rep(i, n) {
    pts[i] = {X[i], Y[i]};
  }
  sort(all(pts));
  int cntAns = 0;
  rep(i, n) {
    int dominatedByCurr = bit.query(pts[i].y);
    int notDominatedByCurr = n - 1 - dominatedByCurr;
    if (abs(dominatedByCurr - notDominatedByCurr) >= k) cntAns++;
    bit.update(pts[i].y, 1);
  }
  cout << cntAns << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}