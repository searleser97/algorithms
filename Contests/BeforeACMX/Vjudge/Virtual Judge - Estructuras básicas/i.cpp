
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
#define all(x) x.begin(), x.end()
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

const int inf = 1 << 30;

struct Point {
  int time, start, end, isStart;
  bool operator<(const Point &p) {
    return time == p.time ? p.isStart < isStart : time < p.time;
  }
};

void _main(int tc) {
  int n;
  cin >> n;
  vector<Point> pA, pB;
  rep(i, n) {
    int sa, ea, sb, eb;
    cin >> sa >> ea >> sb >> eb;
    pA.pb({sa, sb, eb, 1});
    pA.pb({ea, sb, eb, 0});

    pB.pb({sb, sa, ea, 1});
    pB.pb({eb, sa, ea, 0});
  }

  auto fun = [](vector<Point>& v) {
    sort(all(v));
    multiset<int> msS, msE;
    for (int i = 0; i < v.size(); i++) {
      if (v[i].isStart) {
        if (msS.size()) {
          int maxL = *msS.rbegin();
          int minR = *msE.begin();
          if (v[i].end < maxL or v[i].start > minR) {
            return false;
          }
        }
        msS.insert(v[i].start);
        msE.insert(v[i].end);
      } else {
        msS.erase(msS.find(v[i].start));
        msE.erase(msE.find(v[i].end));
      }
    }
    return true;
  };
  if (fun(pA) && fun(pB)) {
    cout << "YES" << endl;
  } else {
    cout << "NO" << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}