// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

template <class T>
struct SegmentTree {
  T neutro = 0;
  int N, H;
  vector<T> st, d;
  vector<bool> u;
  SegmentTree(int n, T val)
      : st(2 * n, val), d(n), u(n) {
    H = sizeof(int) * 8 - __builtin_clz(N = n);
  }
  inline T F(T a, T b) {
    return a + b;
  }
  void apply(int i, T val, int k) {
    st[i] += val * k;
    if (i < N) d[i] += val, u[i] = 1;
  }
  void calc(int i) {
    if (!u[i]) st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  void build() {
    for (int i = N - 1; i > 0; i--) calc(i);
  }
  void build(int p) {
    while (p > 1) p >>= 1, calc(p);
  }
  void push(int p) {
    for (int s = H, k = 1 << (H - 1); s > 0;
         s--, k >>= 1) {
      int i = p >> s;
      if (u[i]) {
        apply(i << 1, d[i], k);
        apply(i << 1 | 1, d[i], k);
        u[i] = 0, d[i] = 0;
      }
    }
  }
  void update(int l, int r, T val) {
    push(l += N);
    push(r += N);
    int ll = l, rr = r, k = 1;
    for (; l <= r; l >>= 1, r >>= 1, k <<= 1) {
      if (l & 1) apply(l++, val, k);
      if (~r & 1) apply(r--, val, k);
    }
    build(ll);
    build(rr);
  }
  T query(int l, int r) {
    push(l += N);
    push(r += N);
    T ans = neutro;
    for (; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  T& operator[](int i) { return st[i + N]; }
};

// 12
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  string s;
  cin >> s;
  vector<int> chars(26);
  SegmentTree<int> st(n, 0);
  
  rep(i, m) {
    int x;
    cin >> x;
    x--;
    st.update(0, x, 1);
  }

  st.update(0, n - 1, 1);

  rep(i, n) {
    chars[s[i] - 'a'] += st.query(i, i);
  }

  rep(i, 26) {
    if (i) cout << " ";
    cout << chars[i];
  }
  cout << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}