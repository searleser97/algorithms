// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  cin >> n;
  string s;
  cin >> s;
  char minim = s[0];
  bool isEqual = true;
  vector<int> positions;
  for (int i = 0; i < n - 1; i++) {
    if (s[i] != s[i + 1]) isEqual = false;
    minim = min(s[i], minim);
  }
  minim = min(s[n - 1], minim);
  for (int i = 0; i < n; i++) {
    if (s[i] == minim) {
      positions.pb(i);
    }
  }
  if (isEqual) {
    cout << s << endl;
    cout << 1 << endl;
    return;
  }

  vector<string> possibles;
  vector<int> ks;
  for (int i = 0; i < positions.size(); i++) {

    string aux = s.substr(positions[i], s.size() - positions[i]);
    string aux2 = s.substr(0, positions[i]);
    int possibleK = positions[i] + 1;
    if ((n & 1) == (possibleK & 1)) reverse(all(aux2));
    if (positions[i] == n - 1) {
      aux2 = "";
      aux = s;
      reverse(all(aux));
    }
    // cout << aux + aux2 << endl;
    possibles.pb(aux + aux2);
    ks.pb(positions[i]);
  }
  int k = ks[0] + 1;
  string ans = possibles[0];
  for (int i = 0; i < possibles.size(); i++) {
    if (possibles[i] < ans) {
      ans = possibles[i];
      k = ks[i] + 1;
    }
  }

  cout << ans << endl;
  cout << k << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}