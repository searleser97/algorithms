// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  string s;
  cin >> s;
  int mostLeft = -1, mostRight = -1;
  for (int l = 0; l < s.size(); l++) {
    if (mostLeft == -1 && s[l] == 'R')  { mostLeft = l + 1; break; }
  }
  for (int r = s.size(); ~r; r--) {
    if (mostRight == -1 && s[r] == 'R') { mostRight = r + 1; break; }
  }
  if (mostLeft == -1) {
    cout << s.size() + 1 << endl;
    return;
  }
  int diff = 0;
  for (int l = mostLeft - 1, r = mostLeft - 1; r < s.size(); r++) {
    if (s[l] == 'R' && s[r] == 'R') {
      diff = max(diff, r - l);
      l = r;
    }
  }
  int ans = max(diff, max((int)s.size() + 1 - mostRight, mostLeft));
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}