// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  cin >> n;
  vector<vector<int>> adj(n);
  set<int> s;

  rep(i, n) {
    s.insert(i);
  }
  int girl = -1;
  rep(i, n) {
    int k;
    cin >> k;
    adj[i] = vector<int>(k);
    bool haspick = 0;
    rep(j, k) {
      cin >> adj[i][j];
      adj[i][j]--;
      if (!haspick && s.count(adj[i][j])) {
        s.erase(adj[i][j]);
        haspick = 1;
      }
    }
    if (!haspick) {
      girl = i;
    }
  }

  if (s.size()) {
    cout << "IMPROVE" << endl;
    int e = *s.begin();
    cout << girl + 1 << " " << e + 1 << endl;
  } else {
    cout << "OPTIMAL" << endl;
  }

}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}