// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

const int MAX_CHAR = 26; 
const int MAX_FACT = 20; 
li fact[MAX_FACT]; 

// 12
void _main(int tc) {
  li n, k;
  cin >> n >> k;
  li l = 1, r = 1e9;
  li mid;
  while (l <= r) {
    mid = l + (r - l) / 2;
    if (k <= (mid * (mid + 1)) / 2) {
      r = mid - 1;
    } else {
      l = mid + 1;
    }
  }
  li sum = 1 + ((l - 1) * l) / 2;
  string ans = "";
  int i;
  for (i = 0; i < n - l - 1; i++) {
    ans += 'a';
  }
  ans += 'b';
  li diff = k - sum;
  for (i++; i < n - diff - 1; i++) ans += 'a';
  ans += 'b';
  
  for (i++; i < n; i++) ans += 'a';
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}