MAX_CHAR = 26
MAX_FACT = 20
fact = [None] * (MAX_FACT)


def precomputeFactorials():

    fact[0] = 1
    for i in range(1, MAX_FACT):
        fact[i] = fact[i - 1] * i


def nPermute(string, n):
    length = len(string)

    freq = [0] * (MAX_CHAR)
    for i in range(0, length):
        freq[ord(string[i]) - ord('a')] += 1

    out = [None] * (MAX_CHAR)

    Sum, k = 0, 0

    while Sum != n:

        Sum = 0

        for i in range(0, MAX_CHAR):
            if freq[i] == 0:
                continue

            freq[i] -= 1
            xsum = fact[length - 1 - k]
            for j in range(0, MAX_CHAR):
                xsum = xsum // fact[freq[j]]
            Sum += xsum
            if Sum >= n:
                out[k] = chr(i + ord('a'))
                n -= Sum - xsum
                k += 1
                break
            if Sum < n:
                freq[i] += 1
    i = MAX_CHAR - 1
    while k < length and i >= 0:
        if freq[i]:
            out[k] = chr(i + ord('a'))
            freq[i] -= 1
            i += 1
            k += 1

        i -= 1

    print(''.join(out[:k]))


if __name__ == "__main__":
    precomputeFactorials()

    tc = int(input())
    for i in range(tc):
        n, k = map(int, input().split())
        print(n, k)
        string = ''
        for i in range(n - 2):
            string += 'a'
        string += 'bb'

        nPermute(string, k)
