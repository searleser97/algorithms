// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  string x;
  cin >> n >> x;
  string s1, s2;
  rep(i, x.size()) {
    if (i == 0) {
      if (x[i] == '0') {
        s1 += '1', s2 += '2';
      } else if (x[i] == '1') {
        s1 += '2', s2 += '2';
      } else {
        s1 += '1', s2 += '1';
      }
    } else {
      if (x[i] == '0') {
        s1 += '0', s2 += '0';
      } else if (x[i] == '1') {
        if (s1 > s2) {
          s1 += '0', s2 += '1';
        } else {
          s1 += '1', s2 += '0';
        }
      } else {
        if (s1 > s2) {
          s1 += '0', s2 += '2';
        } else if (s2 > s1) {
          s1 += '2', s2 += '0';
        } else {
          s1 += '1', s2 += '1';
        }
      }
    }
  }
  cout << s1 << endl;
  cout << s2 << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}