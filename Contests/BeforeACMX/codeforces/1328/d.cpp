// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

int ngroups(vector<int> &v) {
  int cnt = 1;
  rep(i, v.size()) {
    if (i && v[i] != v[i - 1]) cnt++;
  }
  return cnt;
}

void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(i, n) {
    cin >> v[i];
  }

  int groups = ngroups(v);

  if (groups == 1) {
    cout << 1 << endl;
    rep(i, n) {
      if (i) cout << " ";
      cout << 1;
    }
    cout << endl;
    return;
  }

  if (~n & 1) {
    bool isOne = 0;
    cout << 2 << endl;
    rep(i, n) {
      if (i) cout << " ";
      cout << isOne + 1;
      isOne = !isOne;
    }
    cout << endl;
    return;
  }
  if (groups % 2 == 0) {
    cout << 2 << endl;
    bool isOne = 0;
    rep(i, n) {
      if (i) cout << " ";
      if (i && v[i] != v[i - 1]) isOne = !isOne;
      cout << isOne + 1;
    }
    cout << endl;
    return;
  }

  if (v[0] == v[v.size() - 1]) {
      cout << 2 << endl;
      bool isOne = 0;
      rep(i, n) {
        if (i) cout << " ";
        if (i && v[i] != v[i - 1]) isOne = !isOne;
        cout << isOne + 1;
      }
      cout << endl;
      return;
  }

  if (groups == n) {
    cout << 3 << endl;
    bool isOne = 0;
    rep(i, n - 1) {
      if (i) cout << " ";
      cout << isOne + 1;
      isOne = !isOne;
    }
    cout << " " << 3;
    cout << endl;
    return;
  }

  bool changed = 0;
  rep(i, n) {
    if (i && v[i] == v[i - 1]) {
      v[i - 1] = -v[i - 1];
      changed = 1;
      break;
    }
  }

  if (changed) {
    cout << 2 << endl;
    bool isOne = 0;
    rep(i, n) {
      if (i) cout << " ";
      if (i && v[i] != v[i - 1]) isOne = !isOne;
      cout << isOne + 1;
    }
    cout << endl;
  } else {
    cout << 3 << endl;
    bool isOne = 0;
    rep(i, n - 1) {
      if (i) cout << " ";
      if (i && v[i] != v[i - 1]) isOne = !isOne;
      cout << isOne + 1;
    }
    cout << " " << 3;
    cout << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}