// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

// 12
vector<int> dad, dist;
vector<vector<int>> adj;
int n, m;

template <class T>
struct SparseTable {
  vector<vector<T>> st;
  function<T(T, T)> F;
  SparseTable() {}
  SparseTable(vector<T> &v,
      T f(T, T) = [](T a, T b) { return a + b; })
      : F(f) {
    st.assign(1 + log2(v.size()),
              vector<T>(v.size()));
    st[0] = v;
    for (int i = 1; (1 << i) <= v.size(); i++)
      for (int j = 0; j + (1 << i) <= v.size(); j++)
        st[i][j] = F(st[i - 1][j],
                     st[i - 1][j + (1 << (i - 1))]);
  }
  // 5
  // O(1), [l, r]
  T query(int l, int r) {
    int i = log2(r - l + 1);
    return F(st[i][l], st[i][r + 1 - (1 << i)]);
  }
};

// 6
// st = sparse table, p = parent
vector<int> firstPos;
vector<pair<int, int>> tour;
SparseTable<pairii> st;

void init(int N) { adj.assign(N, vector<int>()); }
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}
// 10
// O(N)
void eulerTour(int u, int p, int h) {
  firstPos[u] = tour.size();
  tour.push_back({h, u});
  for (int v : adj[u])
    if (v != p) {
      eulerTour(v, u, h + 1);
      tour.push_back({h, u});
    }
}
// 11
// O(N * lg(N))
void preprocess() {
  tour.clear();
  firstPos.assign(adj.size(), -1);
  vector<int> roots = {0};
  for (auto &root : roots) eulerTour(root, -1, 0);
  st = SparseTable<pairii>(
      tour, [](pairii a, pairii b) {
        return a.first < b.first ? a : b;
      });
}
// 6
// O(1)
int lca(int u, int v) {
  int l = min(firstPos[u], firstPos[v]);
  int r = max(firstPos[u], firstPos[v]);
  return st.query(l, r).second;
}

void dfs(int u) {
  for (auto &v : adj[u]) {
    if (v == dad[u]) continue;
    dad[v] = u;
    dist[v] = dist[u] + 1;
    dfs(v);
  }
}

void auxF() {

  int k;
  cin >> k;
  vector<int> vertices(k);
  unordered_set<int> s;
  rep(j, k) {
    cin >> vertices[j];
    vertices[j]--;
  }
  int maxim = -1, currMax = -(1 << 30);
  rep(j, k) {
    if (dist[vertices[j]] >= currMax) {
      maxim = vertices[j], currMax = dist[vertices[j]];
    }
  }
  
  int cnt = 0;
  rep(j, k) {
    if (lca(maxim, vertices[j]) == vertices[j]) {
      s.insert(vertices[j]);
      cnt++;
    }
    else if (lca(maxim, dad[vertices[j]]) == dad[vertices[j]]) {
      s.insert(vertices[j]);
      cnt++;
    }
  }

  if (cnt == k) {
    cout << "YES" << endl;
    return;
  }
  cout << "NO" << endl;
}

void _main(int tc) {
  cin >> n >> m;
  dad.assign(n, -1);
  dist.assign(n, 1 << 30);
  adj.assign(n, vector<int>());
  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  dist[0] = 1;
  dad[0] = 0;
  dfs(0);
  preprocess();
  rep(i, m) {
    auxF();
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}