// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  vector<vector<int>> mat(n, vector<int>(m));
  short flag = 0;
  int b = 0, w = 0;
  rep(i, n) {
    rep(j, m) {
      mat[i][j] = flag;
      if (flag)
        w++;
      else
        b++;
      flag ^= 1;
    }
  }

  if (w + 1 != b) {
    flag = 0;
    rep(i, n) {
      rep(j, m) {
        if (mat[i][j] == 1) {
          flag = 1;
          mat[i][j] = 0;
          break;
        }
      }
      if (flag == 1) break;
    }
  }

  rep(i, n) {
    rep(j, m) {
      cout << (mat[i][j] == 1 ? 'W' : 'B');
    }
    cout << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}