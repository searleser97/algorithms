// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

struct Interval {
  int s, e;
};

// Function used in sort
bool mycomp(Interval a, Interval b) { return a.s < b.s; }

vector<Interval> mergeIntervals(vector<Interval> arr, int n) {
  // Sort Intervals in increasing order of
  // start time
  sort(all(arr), mycomp);

  int index = 0;  // Stores index of last element
  // in output array (modified arr[])

  // Traverse all input Intervals
  for (int i = 1; i < n; i++) {
    // If this is not first Interval and overlaps
    // with the previous one
    if (arr[index].e >= arr[i].s) {
      // Merge previous and current Intervals
      arr[index].e = max(arr[index].e, arr[i].e);
      arr[index].s = min(arr[index].s, arr[i].s);
    } else {
      arr[index] = arr[i];
      index++;
    }
  }
  return arr;
}

// 12
void _main(int tc) {
  li n;
  cin >> n;
  vector<li> a(n);
  rep(i, n) cin >> a[i];
  li possibleAns = n;
  li sum = 0, zeroSubArray = 0;
  set<pair<li, int>> acum;
  acum.insert({0, -1});
  // li total = n * n - (n - 1);
  vector<Interval> rngs;

  rep(i, n) {
    sum += a[i];
    for (auto &e : acum) {
      cerr(e.fi << " " << e.se) << endl;
    }
    cerr("======") << endl;
    auto leftt = acum.lower_bound({sum, std::numeric_limits<int>::min()});
    if (leftt == acum.end()) {
      acum.insert({sum, i});
      continue;
    }
    auto left = *leftt;
    cerr(left.fi << " " << left.se) << endl;
    if (left.fi == sum) {
      rngs.pb({left.se + 1, i});
    }
    acum.insert({sum, i});
  }

  for (auto &e : rngs) {
    cout << e.s << " " << e.e << endl;
  }

  auto merged = mergeIntervals(rngs, rngs.size());
  li ans = 0;
  rep(i, merged.size()) {
    auto e = merged[i];
    li len = e.s - (e.e - 1);
    ans += (len * len) - (n - 1);
  }
  cout << (ans - rngs.size()) << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}