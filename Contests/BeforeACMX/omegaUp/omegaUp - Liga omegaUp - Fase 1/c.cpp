// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> fib(30000);
  fib[0] = 1;
  fib[1] = 1;
  int i = 2;
  for (; true; i++) {
      fib[i] = fib[i - 1] + fib[i - 2];
      if (fib[i] >= n) break;
  }
  unordered_set<int> s;
  for (int f = 1; f <= i; f++) {
    s.insert(fib[f]);
  }
  bool isFirst = 1;
  for (int k = 1; k < n; k++) {
    if (!s.count(k)) {
      if (!isFirst) cout << " ";
      isFirst = 0;
      cout << k;
    }
  }
  cout << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}