import math


def isPerfectSquare(x):
    s = int(math.sqrt(x))
    return s * s == x


def isFibo(n):
    if isPerfectSquare(5 * n * n + 4):
        return True
    if isPerfectSquare(5 * n * n - 4):
        return True
    return False


n = int(input())
eps = 10**-10
phi = (1 + math.sqrt(5)) / 2
if (isFibo(n)):
    fibonacci_index = int(round(math.log(n * math.sqrt(5) + eps) / math.log(phi)))
    print(fibonacci_index)
else:
    print(-1)
