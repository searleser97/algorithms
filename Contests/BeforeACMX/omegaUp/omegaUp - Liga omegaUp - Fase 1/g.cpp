// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

typedef vector<li> V;

// O(sqrt(N))
V getDivisors(li n) {
  V divs;
  for (li i = 1; i * i <= n; i++)
    if (!(n % i)) {
      divs.push_back(i);
      divs.push_back(n / i);
    }
  return divs;
}

void _main(int tc) {
  li k;
  cin >> k;
  auto h = getDivisors(k * k);
  cout << h.size() / 2 << endl;
  for (int i = 0; i + 1 < h.size(); i += 2) {
    cout << 1 << "/" << k << " = " << 1 << "/" << h[i + 1] + k << " + " << 1 << "/" << h[i] + k << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}