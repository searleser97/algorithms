// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

ld eps = 1e-9, inf = numeric_limits<ld>::max();
// For use with integers, just set eps=0 and everything remains the same
bool geq(ld a, ld b) { return a - b >= -eps; }     // a >= b
bool leq(ld a, ld b) { return b - a >= -eps; }     // a <= b
bool ge(ld a, ld b) { return a - b > eps; }        // a > b
bool le(ld a, ld b) { return b - a > eps; }        // a < b
bool eq(ld a, ld b) { return abs(a - b) <= eps; }  // a == b
bool neq(ld a, ld b) { return abs(a - b) > eps; }  // a != b

struct point {
  ld x, y;
  point() : x(0), y(0) {}
  point(ld x, ld y) : x(x), y(y) {}

  point operator+(const point& p) const { return point(x + p.x, y + p.y); }
  point operator-(const point& p) const { return point(x - p.x, y - p.y); }
  point operator*(const ld& k) const { return point(x * k, y * k); }
  point operator/(const ld& k) const { return point(x / k, y / k); }

  point operator+=(const point& p) {
    *this = *this + p;
    return *this;
  }
  point operator-=(const point& p) {
    *this = *this - p;
    return *this;
  }
  point operator*=(const ld& p) {
    *this = *this * p;
    return *this;
  }
  point operator/=(const ld& p) {
    *this = *this / p;
    return *this;
  }

  point rotate(const ld& angle) const {
    return point(x * cos(angle) - y * sin(angle),
                 x * sin(angle) + y * cos(angle));
  }
  point perp() const { return point(-y, x); }

  ld dot(const point& p) const { return x * p.x + y * p.y; }
  ld cross(const point& p) const { return x * p.y - y * p.x; }
  ld norm() const { return x * x + y * y; }
  ld length() const { return sqrtl(x * x + y * y); }
  point unit() const { return (*this) / length(); }

  bool operator==(const point& p) const { return eq(x, p.x) && eq(y, p.y); }
  bool operator!=(const point& p) const { return !(*this == p); }
  bool operator<(const point& p) const {
    return le(x, p.x) || (eq(x, p.x) && le(y, p.y));
  }
  bool operator>(const point& p) const {
    return ge(x, p.x) || (eq(x, p.x) && ge(y, p.y));
  }
  bool half(const point& p) const {
    return le(p.cross(*this), 0) ||
           (eq(p.cross(*this), 0) && le(p.dot(*this), 0));
  }
};

istream& operator>>(istream& is, point& p) { return is >> p.x >> p.y; }

point intersectLines(const point& a1, const point& v1, const point& a2,
                     const point& v2) {
  // lines a1+tv1, a2+tv2
  // assuming that they intersect
  ld det = v1.cross(v2);
  return a1 + v1 * ((a2 - a1).cross(v2) / det);
}

pair<point, ld> getCircle(const point& m, const point& n, const point& p) {
  // find circle that passes through points p, q, r
  point c =
      intersectLines((n + m) / 2, (n - m).perp(), (p + n) / 2, (p - n).perp());
  ld r = (c - m).length();
  return {c, r};
}
pair<point, ld> mec2(vector<point>& S, const point& a, const point& b, int n) {
  ld hi = inf, lo = -hi;
  for (int i = 0; i < n; ++i) {
    ld si = (b - a).cross(S[i] - a);
    if (eq(si, 0)) continue;
    point m = getCircle(a, b, S[i]).first;
    ld cr = (b - a).cross(m - a);
    if (le(si, 0))
      hi = min(hi, cr);
    else
      lo = max(lo, cr);
  }
  ld v = (ge(lo, 0) ? lo : le(hi, 0) ? hi : 0);
  point c = (a + b) / 2 + (b - a).perp() * v / (b - a).norm();
  return {c, (a - c).norm()};
}

pair<point, ld> mec(vector<point>& S, const point& a, int n) {
  random_shuffle(S.begin(), S.begin() + n);
  point b = S[0], c = (a + b) / 2;
  ld r = (a - c).norm();
  for (int i = 1; i < n; ++i) {
    if (ge((S[i] - c).norm(), r)) {
      tie(c, r) = (n == S.size() ? mec(S, S[i], i) : mec2(S, a, S[i], i));
    }
  }
  return {c, r};
}

pair<point, ld> smallestEnclosingCircle(vector<point> S) {
  assert(!S.empty());
  auto r = mec(S, S[0], S.size());
  return {r.first, sqrt(r.second)};
}

vector<point> convexHull(vector<point> P) {
  sort(P.begin(), P.end());
  vector<point> L, U;
  for (int i = 0; i < P.size(); i++) {
    while (L.size() >= 2 &&
           leq((L[L.size() - 2] - P[i]).cross(L[L.size() - 1] - P[i]), 0)) {
      L.pop_back();
    }
    L.push_back(P[i]);
  }
  for (int i = P.size() - 1; i >= 0; i--) {
    while (U.size() >= 2 &&
           leq((U[U.size() - 2] - P[i]).cross(U[U.size() - 1] - P[i]), 0)) {
      U.pop_back();
    }
    U.push_back(P[i]);
  }
  L.pop_back();
  U.pop_back();
  L.insert(L.end(), U.begin(), U.end());
  return L;
}

void _main(int tc) {
  int n, m, l;
  cin >> n >> m >> l;
  vector<point> ps(l);
  rep(i, l) { cin >> ps[i]; }

  int q;
  cin >> q;
  rep(i, q) {
    int k;
    cin >> k;
    vector<point> aux(k);
    rep(j, k) {
      int x;
      cin >> x;
      x--;
      aux[j] = ps[x];
    }
    auto ans = smallestEnclosingCircle(aux);
    cout << setprecision(9) << fixed << ans.second << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}