import itertools
from sys import stdin, stdout

# from arr choose k = > combinations(arr, k)
n = int(input())
v = list(map(int, stdin.readline().split()))

cnt = 0

for ii in range(1, n):
  a = list(itertools.combinations(v, ii))
  for i in range(len(a)):
    an = a[i][0]
    for j in range(ii):
      an = an ^ a[i][j]
    if (an == 0):
      cnt = (cnt + 1) % (1e9 + 7)

print(int(cnt))