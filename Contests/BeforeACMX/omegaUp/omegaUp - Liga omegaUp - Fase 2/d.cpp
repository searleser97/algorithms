// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

vector<int> lowerBound(vector<int>& v, int e, int l, int r) {
  auto it = v.begin();
  int i = lower_bound(it + l, it + r, e) - it;
  return {v[i] == e, i};
}

void _main(int tc) {
  int n, m;
  cin >> n >> m;
  vector<int> v(n);
  string s;
  cin >> s;
  rep(i, n) { v[i] = s[i] - '0'; }
  vector<int> onesPos;
  rep(i, n) {
    if (v[i] == 1) onesPos.pb(i);
  }


  int currPos = 0;
  int steps = 0;

  while (currPos < n - 1) {
    steps++;
    if (currPos + m >= n) {
      break;
    }
    
    auto it = (lowerBound(onesPos, currPos + m, 0, onesPos.size()));
    if (!it[0]) it[1]--;

    if ((it[0] == 0 && it[1] == onesPos.size() - 1) or currPos == onesPos[it[1]]) {
      cout << -1 << endl;
      return;
    }

    currPos = onesPos[it[1]];
  }
  cout << steps << endl;
}

int main() {
  // ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}