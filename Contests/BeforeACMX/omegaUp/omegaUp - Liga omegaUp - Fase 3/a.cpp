
// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
const int inf = 1e9 + 10;
vector<int> dp;
map<char, set<int>> m;
int limit = 1e4 + 100;


void _main(int tc) {
  int n;
  cin >> n;
  if (dp[n] != 0) {
    cout << dp[n] << endl;
    return;
  }
  int minDist = inf;
  int ans = 0;
  forn(i, 1, limit) {
    string number = to_string(i);
    bool flag = true;
    rep(j, number.size() - 1) {
      if (!m[number[j]].count(number[j + 1] - '0')) {
        flag = false;
        break;
      }
    }
    if (flag) {
      int posans = abs(n - i);
      if (posans < minDist) {
        minDist = posans;
        ans = i;
      }
    }
  }
  dp[n] = ans;
  cout << ans << endl;
}

int main() {
  // ios_base::sync_with_stdio(0), cin.tie(0);
  dp = vector<int>(limit, 0);
  m['1'] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m['2'] = {0, 2, 3, 5, 6, 8, 9};
  m['3'] = {3, 6, 9};
  m['4'] = {0, 4, 5, 6, 7, 8, 9};
  m['5'] = {0, 5, 6, 8, 9};
  m['6'] = {6, 9};
  m['7'] = {0, 7, 8, 9};
  m['8'] = {0, 8, 9};
  m['9'] = {9};
  m['0'] = {0};
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}