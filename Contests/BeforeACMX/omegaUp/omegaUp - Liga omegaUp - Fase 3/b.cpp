// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

// 8
vector<string> split(string str, char token) {
  stringstream ss(str);
  vector<string> v;
  while (getline(ss, str, token)) v.push_back(str);
  return v;
}

string input() {
  string ans;
  // cin >> ws;
  // cin.ignore(numeric_limits<streamsize>::max(), '\n');
  getline(cin, ans);
  return ans;
}

void _main(int tc) {
  int d, a;
  d = stoi(input());
  a = stoi(input());

  map<string, int> m;
  int id = 0;
  rep(i, d) {
    m[split(input(), ' ')[1]] = id++;
  }
  rep(i, a) {
    auto strs = split(input(), ' ');
    string a = strs[0];
    string b = strs[2];
    b.pop_back();
    m[a] = m[b];
  }
  set<int> ids;
  for (auto & kv : m) {
    ids.insert(kv.se);
  }
  cout << ids.size() << endl;
}

int main() {
  // ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  // int tc;
  // cin >> tc;
  // rep(i, tc) _main(i + 1);
}