// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
const int inf = 1e9 + 10;
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  vector<vector<char>> mat(n, vector<char>(m));
  bool flag = false;
  rep(i, n) {
    rep(j, m) {
      cin >> mat[i][j];
      if (mat[i][j] == 'X') flag = true;
    }
  }
  if (!flag) {
    cout << 0 << endl;
    cout << 0 << endl;
    return;
  }
  vector<vector<bool>> vis(n, vector<bool>(m));
  vector<vector<short>> movs = {
    {0, 1}, {1, 0}, {-1, 0}, {0, -1}
  };
  int auxSuma = 0;
  function<void(int, int)> dfs = [&](int i, int j) {
    if (i < 0 or i >= n or j < 0 or j >= m or mat[i][j] == '0' or vis[i][j] == 1) return;
    vis[i][j] = 1;
    auxSuma++;
    for (auto & mov : movs) {
      dfs(i + mov[0], j + mov[1]);
    }
  };
  int ansMin = inf;
  int ansMax = -inf;

  rep(i, n) {
    rep(j, m) {
      if (vis[i][j] == 0 && mat[i][j] == 'X') {
        auxSuma = 0;
        dfs(i, j);
        ansMin = min(ansMin, auxSuma);
        ansMax = max(ansMax, auxSuma);
      }
    }
  }
  cout << ansMax << endl;
  cout << ansMin << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}