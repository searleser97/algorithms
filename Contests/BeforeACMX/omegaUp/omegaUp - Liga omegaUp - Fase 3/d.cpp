// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  vector<vector<short>> mat(n, vector<short>(m));
  rep(i, n) {
    rep(j, m) { cin >> mat[i][j]; }
  }
  pairii prevmov = {0, 1};
  pairii pos = {0, 0};

  while (1) {
    if (mat[pos.fi][pos.se] == 0) {
      pos.fi += prevmov.fi;
      pos.se += prevmov.se;
    } else if (mat[pos.fi][pos.se] == 1) {
      if (prevmov.fi == 1 && prevmov.se == 0) {
        prevmov = {0, -1};
        pos.fi += 0, pos.se += -1;
      } else if (prevmov.fi == 0 && prevmov.se == 1) {
        prevmov = {1, 0};
        pos.fi += 1, pos.se += 0;
      } else if (prevmov.fi == 0 && prevmov.se == -1) {
        prevmov = {-1, 0};
        pos.fi += -1, pos.se += 0;
      } else if (prevmov.fi == -1 && prevmov.se == 0) {
        prevmov = {0 , 1};
        pos.fi += 0, pos.se += 1;
      }
    } else if (mat[pos.fi][pos.se] == 2) {
      if (prevmov.fi == 1 && prevmov.se == 0) {
        prevmov = {0, 1};
        pos.fi += 0, pos.se += 1;
      } else if (prevmov.fi == 0 && prevmov.se == 1) {
        prevmov = {-1, 0};
        pos.fi += -1, pos.se += 0;
      } else if (prevmov.fi == 0 && prevmov.se == -1) {
        prevmov = {1, 0};
        pos.fi += 1, pos.se += 0;
      } else if (prevmov.fi == -1 && prevmov.se == 0) {
        prevmov = {0, -1};
        pos.fi += 0, pos.se += -1;
      }
    } else if (mat[pos.fi][pos.se] == 3) {
      if (prevmov.fi == 1 && prevmov.se == 0) {
        prevmov = {-1, 0};
        pos.fi += -1, pos.se += 0;
      } else if (prevmov.fi == 0 && prevmov.se == 1) {
        prevmov = {0, -1};
        pos.fi += 0, pos.se += -1;
      } else if (prevmov.fi == 0 && prevmov.se == -1) {
        prevmov = {0, 1};
        pos.fi += 0, pos.se += 1;
      } else if (prevmov.fi == -1 && prevmov.se == 0) {
        prevmov = {1, 0};
        pos.fi += 1, pos.se += 0;
      }
    }

    if (pos.fi < 0 or pos.fi >= n or pos.se < 0 or pos.se >= m) {
      cout << "te perdiste" << endl;
      return;
    }
    if (pos.fi == n - 1 && pos.se == m - 1) {
      cout << "llegaste" << endl;
      return;
    }
  }
  if (pos.fi == n - 1 && pos.se == m - 1) {
    cout << "llegaste" << endl;
    return;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
