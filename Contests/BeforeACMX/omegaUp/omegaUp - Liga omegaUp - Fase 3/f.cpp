// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;
  // 6
  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
    // return a * b;
  }
  // 5
  // Inverse of F
  inline T I(T a, T b) {
    return a - b;
    // return a / b;
  }
  // 7
  // O(N)
  void build() {
    for (int i = 1; i < bit.size(); i++) {
      int j = i + (i & -i);
      if (j < bit.size()) bit[j] = F(bit[j], bit[i]);
    }
  }
  // 4
  // O(lg(N))
  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }
  // 6
  // O(lg(N))
  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }
  // 6
  // O(lg(N)), [l, r]
  T query(int l, int r) {
    return I(query(r), query(--l));
  }
  // 10
  // O(lg(N)) binary search in prefix sum array
  T lowerBound(T x) {
    T acum = neutro;
    int pos = 0;
    for (int i = msb(bit.size() - 1); i; i >>= 1)
      if ((pos | i) < bit.size() &&
          F(acum, bit[pos | i]) < x)
        acum = F(acum, bit[pos |= i]);
    return pos;
  }

  T& operator[](int i) { return bit[++i]; }
};

void _main(int tc) {
  int m;
  cin >> m;
  BIT<int> bit(1e6 + 10);
  rep(i, m) {
    int p, x;
    cin >> p >> x;
    if (p == 1) {
      bit.update(x, 1);
    } else {
      cout << bit.query(x) << endl;
    }
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}