// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &os, pair<A, B> &p) {
  return os << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &os, vector<T> &v) {
  rep(i, (int)v.size()) os << setw(7) << left << v[i];
  return os << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &os, map<A, B> &m) {
  for (auto &kv : m) os << kv;
  return os;
}
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<vector<char>> mat(n, vector<char>(n, '.'));
  int m;
  cin >> m;
  rep(i, m) {
    int x, y;
    char c;
    cin >> x >> y >> c;
    x--, y--;
    mat[x][y] = c;
  }

  debug(mat)

      vector<pair<int, int>>
          ans;

  rep(i, n) {
    rep(j, n) {
      if (mat[i][j] == '.') {
        mat[i][j] = '0';
        int cnt = 0;

        for (int y = 0, x = i; y < n; y++) {
          if (mat[x][y] == '1' or mat[x][y] == '.') cnt = 0;
          if (mat[x][y] == '0') cnt++;
          if (cnt >= 5) {
            ans.pb({i, j});
          }
        }
        cnt = 0;
        for (int x = 0, y = j; x < n; x++) {
          if (mat[x][y] == '1' or mat[x][y] == '.') cnt = 0;
          if (mat[x][y] == '0') cnt++;
          if (cnt >= 5) {
            ans.pb({i, j});
          }
        }

        cnt = 0;
        int r, c;
        if (j - i >= 0) {
          r = 0, c = j - i;
        } else {
          r = i - j, c = 0;
        }
        for (; r < n && c < n; r++, c++) {
          if (mat[r][c] == '1' or mat[r][c] == '.') cnt = 0;
          if (mat[r][c] == '0') cnt++;
          if (cnt >= 5) {
            ans.pb({i, j});
          }
        }

        cnt = 0;
        if (i > n - 1 - j) {
          r = i - (n - 1 - j), c = n - 1;
        } else {
          r = 0, c = j + i;
        }
        for (; r >= 0 && c >= 0; r--, c--) {
          if (mat[r][c] == '1' or mat[r][c] == '.') cnt = 0;
          if (mat[r][c] == '0') cnt++;
          if (cnt >= 5) {
            ans.pb({i, j});
          }
        }

        mat[i][j] = '.';
      }
    }
  }

  rep(i, (int)ans.size()) {
    cout << ans[i].fi + 1 << " " << ans[i].se + 1 << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}