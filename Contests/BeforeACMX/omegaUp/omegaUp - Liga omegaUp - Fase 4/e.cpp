// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &os, pair<A, B> &p) {
  return os << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &os, vector<T> &v) {
  rep(i, (int)v.size()) os << setw(7) << left << v[i];
  return os << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &os, map<A, B> &m) {
  for (auto &kv : m) os << kv;
  return os;
}

// 6
// 5
// st = segment tree, st[1] = root, H = height of d
// u = updates, d = delayed updates
// neutro = operation neutral val
// e.g. for sum is 0, for multiplication
// is 1, for gcd is 0, for min is INF, etc.
// 7
template <class T>
struct SegmentTree {
  T neutro = 0;
  int N, H;
  vector<T> st, d;
  vector<bool> u;
  function<T(T, T)> F;
  // 5
  SegmentTree(int n, T val,
      T f(T, T) = [](T a, T b) { return a + b; })
      : st(2 * n, val), d(n), u(n), F(f) {
    H = sizeof(int) * 8 - __builtin_clz(N = n);
  }
  // 7
  void apply(int i, T val, int k) {
    // operation to update st[i] in O(1) time
    st[i] += val * k;  // updates the tree
    // operation to update d[i] in O(1) time
    // which updates values for future updates
    if (i < N) d[i] += val, u[i] = 1;
  }
  // 3
  void calc(int i) {
    if (!u[i]) st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 4
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--) calc(i);
  }
  // 4
  // O(lg(N))
  void build(int p) {
    while (p > 1) p >>= 1, calc(p);
  }
  // 12
  // O(lg(N))
  void push(int p) {
    for (int s = H, k = 1 << (H - 1); s;
         s--, k >>= 1) {
      int i = p >> s;
      if (u[i]) {
        apply(i << 1, d[i], k);
        apply(i << 1 | 1, d[i], k);
        u[i] = 0, d[i] = 0;
      }
    }
  }
  // 10
  // O(lg(N)), [l, r]
  void update(int l, int r, T val) {
    push(l += N), push(r += N);
    int ll = l, rr = r, k = 1;
    for (; l <= r; l >>= 1, r >>= 1, k <<= 1) {
      if (l & 1) apply(l++, val, k);
      if (~r & 1) apply(r--, val, k);
    }
    build(ll), build(rr);
  }
  // 10
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    push(l += N), push(r += N);
    T ans = neutro;
    for (; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};
// p = parent;
typedef int T;
vector<vector<int>> adj;
vector<int> p, heavy, depth, root, stPos, vals;
SegmentTree<T> st(0, 0);
// 7
void init(int n) {
  adj.assign(n, vector<int>());
  heavy.assign(n, -1);
  vals.assign(n, 0);
  p = root = stPos = depth = heavy;
  st = SegmentTree<T>(n, 0);
}
// 4
void addEdge(int u, int v, T val) {
  adj[u].push_back(v);
  p[v] = u, vals[v] = val;
}

T F(T a, T b) { return max(a, b); }
// 13
// O(N)
int dfs(int u) {
  int size = 1, maxSubtree = 0;
  for (int &v : adj[u]) {
    depth[v] = depth[u] + 1;
    int subtree = dfs(v);
    if (subtree > maxSubtree)
      heavy[u] = v, maxSubtree = subtree;
    size += subtree;
  }
  return size;
}
// 10
// O(N)
void initHeavyLight() {
  for (int i = 0; i < adj.size(); i++)
    if (p[i] < 0) dfs(i);
  for (int i = 0, pos = 0; i < adj.size(); i++)
    if (p[i] < 0 || heavy[p[i]] != i)
      for (int j = i; ~j; j = heavy[j])
        st[stPos[j] = pos++] = vals[j], root[j] = i;
  st.build();
}
// 13
// O(lg^2 (N))
template <class Op>
void processPath(int u, int v, Op op) {
  for (; root[u] != root[v]; v = p[root[v]]) {
    if (depth[root[u]] > depth[root[v]]) swap(u, v);
    op(stPos[root[v]], stPos[v]);
  }
  if (depth[u] > depth[v]) swap(u, v);
  // for values on edges
  // if (u != v) op(stPos[u] + 1, stPos[v]);
  // for values on nodes
  op(stPos[u], stPos[v]);
}
// 6
// O(lg^2 (N))
void update(int u, int v, T val) {
  processPath(u, v, [&val](int l, int r) {
    st.update(l, r, val);
  });
}
// 8
// O(lg^2 (N))
T query(int u, int v) {
  T ans = T();
  processPath(u, v, [&ans](int l, int r) {
    ans = F(ans, st.query(l, r));
  });
  return ans;
}


void _main(int tc) {
  int n;
  cin >> n;
  init(n + 1);
  vector<int> h(n);
  rep(i, n) cin >> h[i];
  int total = ((n - 1) * n ) / 2;
  addEdge(n, 0, h[0]);
  rep(i, n - 1) {
    int u, v;
    u--, v--;
    cin >> u >> v;
    addEdge(u, v, h[v]);
  }
  rep(u, n) {
    forn(v, u + 1, n) {

    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}