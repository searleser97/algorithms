// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

#define NIL 0
#define INF INT_MAX

struct BipGraph {
  int m, n;

  list<int> *adj;

  int *pairU, *pairV, *dist;

  BipGraph(int m, int n);
  void addEdge(int u, int v);

  bool bfs();

  bool dfs(int u);

  int hopcroftKarp();
};

int BipGraph::hopcroftKarp() {
  pairU = new int[m + 1];

  pairV = new int[n + 1];

  dist = new int[m + 1];

  for (int u = 0; u <= m; u++) pairU[u] = NIL;
  for (int v = 0; v <= n; v++) pairV[v] = NIL;

  int result = 0;

  while (bfs()) {
    for (int u = 1; u <= m; u++)

      if (pairU[u] == NIL && dfs(u)) result++;
  }
  return result;
}

bool BipGraph::bfs() {
  queue<int> Q;

  for (int u = 1; u <= m; u++) {
    if (pairU[u] == NIL) {
      dist[u] = 0;
      Q.push(u);
    }

    else
      dist[u] = INF;
  }

  dist[NIL] = INF;

  while (!Q.empty()) {
    int u = Q.front();
    Q.pop();

    if (dist[u] < dist[NIL]) {
      list<int>::iterator i;
      for (i = adj[u].begin(); i != adj[u].end(); ++i) {
        int v = *i;

        if (dist[pairV[v]] == INF) {
          dist[pairV[v]] = dist[u] + 1;
          Q.push(pairV[v]);
        }
      }
    }
  }

  return (dist[NIL] != INF);
}

bool BipGraph::dfs(int u) {
  if (u != NIL) {
    list<int>::iterator i;
    for (i = adj[u].begin(); i != adj[u].end(); ++i) {
      int v = *i;

      if (dist[pairV[v]] == dist[u] + 1) {
        if (dfs(pairV[v]) == true) {
          pairV[v] = u;
          pairU[u] = v;
          return true;
        }
      }
    }

    dist[u] = INF;
    return false;
  }
  return true;
}

BipGraph::BipGraph(int m, int n) {
  this->m = m;
  this->n = n;
  adj = new list<int>[m + 1];
}
void BipGraph::addEdge(int u, int v) { adj[u].push_back(v); }

typedef string Val;
map<Val, int> intForVal;
int mapId = 1;
// 5
int Map(Val val) {
  if (intForVal.count(val)) return intForVal[val];
  return intForVal[val] = mapId++;
}

void _main(int tc) {
  int n;
  cin >> n;
  mapId = n + 1;
  vector<vector<int>> v(n);
  rep(n) {
    string girl;
    cin >> girl;
    int k;
    cin >> k;
    v[i] = vector<int>(k);
    rep(j, k) {
      string boy;
      cin >> boy;
      v[i][j] = Map(boy);
    }
  }
  BipGraph g(mapId + 10, mapId + 10);

  rep(n) {
    rep(j, len(v[i])) {
      debug(i + 1, v[i][j]);
      g.addEdge(i + 1, v[i][j]);
    }
  }
  cout << n + (mapId - 1 - n) - g.hopcroftKarp() << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}