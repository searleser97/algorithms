// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

// 12
int editDistance(string str1, string str2) {
  int len1 = len(str1);
  int len2 = len(str2);
  int DP[2][len1 + 1];
  memset(DP, 0, sizeof DP);
  for (int i = 0; i <= len1; i++) DP[0][i] = i;

  for (int i = 1; i <= len2; i++) {
    for (int j = 0; j <= len1; j++) {
      if (j == 0) DP[i % 2][j] = i;

      else if (str1[j - 1] == str2[i - 1]) {
        DP[i % 2][j] = DP[(i - 1) % 2][j - 1];
      }

      else {
        DP[i % 2][j] = 1 + min(DP[(i - 1) % 2][j],
                               min(DP[i % 2][j - 1], DP[(i - 1) % 2][j - 1]));
      }
    }
  }
  return DP[len2 % 2][len1];
}

void _main(int tc) {
  string a, b;
  cin >> a >> b;
  if (editDistance(a, b) > 3) {
    cout << "No" << endl;
  } else {
    cout << "Yes" << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}