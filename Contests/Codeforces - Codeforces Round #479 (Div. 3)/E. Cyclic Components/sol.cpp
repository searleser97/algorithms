// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, (int)v.size()) o << setw(7) << left << v[i];
  return o << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
int compId;
vector<vector<int>> adj;
vector<int> getComp;
vector<int> indegree;
// 5
void init(int N) {
  adj.assign(N, vector<int>());
  getComp.assign(N, -1);
  indegree.assign(N, 0);
  compId = 0;
}
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
  indegree[u]++, indegree[v]++;
}
// 6
void dfsCC(int u, vector<int> &comp) {
  if (getComp[u] > -1) return;
  getComp[u] = compId;
  comp.push_back(u);
  for (auto &v : adj[u]) dfsCC(v, comp);
}
// 10
// O(N)
vector<vector<int>> connectedComponents() {
  vector<vector<int>> comps;
  for (int u = 0; u < adj.size(); u++) {
    vector<int> comp;
    dfsCC(u, comp);
    if (!comp.empty()) comps.push_back(comp), compId++;
  }
  return comps;
}

// 3
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  init(n);
  rep(i, m) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    addEdge(u, v);
  }
  auto aux = connectedComponents();
  int cnt = 0;
  for (auto & c : aux) {
    bool flag = 1;
    for (auto & e : c) {
      if (indegree[e] != 2) flag = 0;
    }
    if (flag) cnt++;
  }

  cout << cnt << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}