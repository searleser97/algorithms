// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// timre required: 1:20 min
void _main(int tc) {
  int n;
  cin >> n;
  map<int, int> mp;
  map<int, vector<int>> imp;
  rep(n * n) {
    int x;
    cin >> x;
    mp[x]++;
  }

  for (auto &e : mp) {
    imp[e.se].pb(e.fi);
  }
  vector<int> ans;
  if (~n & 1) {
    for (auto &e : mp) {
      rep(e.se / 4) ans.pb(e.fi);
      e.se -= 4 * (e.se / 4);
    }

    for (auto &e : mp) {
      if (e.se > 0) {
        cout << "NO" << endl;
        return;
      }
    }

    cout << "YES" << endl;

    rep(n / 2) {
      rep(j, i * (n / 2), i * (n / 2) + n / 2) { cout << ans[j] << " "; }

      rep(j, i * (n / 2) + n / 2 - 1, i * (n / 2) - 1) {
        cout << ans[j] << " \n"[j == i * (n / 2)];
      }
    }
    rep(i, n / 2 - 1, -1) {
      rep(j, i * (n / 2), i * (n / 2) + n / 2) { cout << ans[j] << " "; }

      rep(j, i * (n / 2) + n / 2 - 1, i * (n / 2) - 1) {
        cout << ans[j] << " \n"[j == i * (n / 2)];
      }
    }
  } else {
    for (auto &e : mp) {
      int aux = len(ans);
      rep(min(e.se / 4, ((n - 1) * (n - 1) / 4) - aux)) ans.pb(e.fi);
      e.se -= 4 * min(e.se / 4, ((n - 1) * (n - 1) / 4) - aux);
      if (len(ans) == ((n - 1) * (n - 1)) / 4) break;
    }

    debug(mp);

    vector<int> vert, hori;
    for (auto &e : mp) {
      int aux = len(vert);
      rep(min(e.se / 2, n / 2 - aux)) vert.pb(e.fi);
      e.se -= 2 * min(e.se / 2, n / 2 - aux);
      if (len(vert) == n / 2) break;
    }

    for (auto &e : mp) {
      int aux = len(hori);
      rep(min(e.se / 2, n / 2 - aux)) hori.pb(e.fi);
      e.se -= 2 * min(e.se / 2, n / 2 - aux);
      if (len(hori) == n / 2) break;
    }
    int cnt = 0;
    int unique = 0;
    for (auto &e : mp) {
      if (e.se > 0) {
        unique = e.fi;
        cnt++;
      }
    }
    if (cnt > 1) {
      cout << "NO" << endl;
      return;
    }

    debug(ans);
    debug(vert);
    debug(hori);
    debug(unique);
    // return;

    cout << "YES" << endl;

    rep(n / 2) {
      rep(j, i * (n / 2), i * (n / 2) + n / 2) { cout << ans[j] << " "; }
      cout << vert[i] << " ";
      rep(j, i * (n / 2) + n / 2 - 1, i * (n / 2) - 1) {
        cout << ans[j] << " \n"[j == i * (n / 2)];
      }
    }

    rep(n / 2) { cout << hori[i] << " "; }
    cout << unique << " \n"[n == 1];
    rep(i, n / 2 - 1, -1) { cout << hori[i] << " \n"[i == 0]; }

    rep(i, n / 2 - 1, -1) {
      rep(j, i * (n / 2), i * (n / 2) + n / 2) { cout << ans[j] << " "; }
      cout << vert[i] << " ";
      rep(j, i * (n / 2) + n / 2 - 1, i * (n / 2) - 1) {
        cout << ans[j] << " \n"[j == i * (n / 2)];
      }
    }
  }
}

// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}