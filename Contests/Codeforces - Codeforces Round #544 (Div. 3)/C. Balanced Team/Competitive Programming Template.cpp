// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
}
#else
#define debug(...)
#endif
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(n) cin >> v[i];
  sort(all(v));
  multiset<int> mzt;
  debug(v);
  int ans = -1;
  int init = 0;
  for (int i = 0; i < n; ) {
    if (len(mzt) == 0) mzt.insert(v[i]), i++;
    debug("if debug");
    debug(vector<int>(all(mzt)));
    while (i < n && *mzt.rbegin() - *mzt.begin() <= 5) {
      mzt.insert(v[i]);
      i++;
    }
    debug(vector<int>(all(mzt)));
    if (*mzt.rbegin() - *mzt.begin() > 5)
      ans = max(ans, len(mzt) - 1);
    else
      ans = max(ans, len(mzt));
    debug("ans", ans);
    while (init < i && *mzt.rbegin() - *mzt.begin()> 5) {
      mzt.erase(mzt.find(v[init]));
      init++;
    }
    debug(vector<int>(all(mzt)));
  }
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}