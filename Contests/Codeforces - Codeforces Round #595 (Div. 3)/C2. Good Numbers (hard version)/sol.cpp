// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

// O(log(n)) [msb, ..., lsb]
vector<int> toBaseB(li n, int b) {
  vector<int> ans;
  while (n) ans.push_back(n % b), n /= b;
  // reverse(ans.begin(), ans.end());
  return ans;
}

// O(log(n)) [msb, ..., lsb]
li toBase10(vector<int>& n, int b) {
  li ans = 0;
  for (li i = n.size() - 1, p = 1; ~i; i--, p *= b)
    ans += n[i] * p;
  return ans;
}

void _main(int tc) {
  li n;
  cin >> n;
  auto b3 = toBaseB(n, 3);
  debug(b3);
  int carry = 0, lastZeroInc = 0;
  rep(len(b3)) {
    if (carry) {
      b3[i] += 1;
      if (b3[i] == 1) lastZeroInc = i;
      carry = 0;
    }
    if (b3[i] >= 2) {
      carry = 1;
      b3[i] = 0;
    }
  }
  if (carry) {
    b3 = vector<int>(b3.size(), 0);
    b3.pb(1);
  } else if (lastZeroInc) {
    rep(lastZeroInc) b3[i] = 0;
  }
  debug(b3);
  reverse(all(b3));
  auto ans = toBase10(b3, 3);
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}