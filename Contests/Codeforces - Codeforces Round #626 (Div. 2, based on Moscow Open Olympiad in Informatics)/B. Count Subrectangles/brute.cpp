#include <bits/stdc++.h>

using namespace std;

#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc));
#endif
}

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);

  int n;
  cin >> n;
  int m;
  cin >> m;
  int k;
  cin >> k;

  vector<int> row;
  vector<int> col;

  for (int i = 0; i < n; ++i) {
    int x;
    cin >> x;
    if (x) row.pb(i);
  }
  for (int j = 0; j < m; ++j) {
    int x;
    cin >> x;
    if (x) col.pb(j);
  }
  debug(row);
  debug(col);

  li ans = 0;

  auto cal = [&](int R, int C) {
    cerr << endl;
    debug(R, C);
    cerr << endl;
    int cnt0 = 0;
    int cnt1 = 0;
    for (int i = R - 1; i < len(row); ++i) {
      debug(i, " ");
      if (row[i - R + 1] == row[i] - R + 1) cnt0++;
    }
    cerr << endl;
    for (int i = C - 1; i < len(col); ++i) {
      debug(i, " ");
      if (col[i - C + 1] == col[i] - C + 1) cnt1++;
    }
    cerr << endl;
    debug(cnt0, cnt1);
    cerr << endl;
    ans += 1ll * cnt0 * cnt1;
  };

  for (int i = 1; i * i <= k; ++i)
    if (k % i == 0) {
      cal(i, k / i);
      if (i * i < k) cal(k / i, i);
    }
  cout << ans << endl;
}