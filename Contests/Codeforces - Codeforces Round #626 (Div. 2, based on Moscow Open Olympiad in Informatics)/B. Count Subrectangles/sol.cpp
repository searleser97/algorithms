// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3
void _main(int tc) {
  int n, m, k;
  cin >> n >> m >> k;
  vector<int> rowZeroPos = {0}, colZeroPos = {0};
  rep(n) {
    int x;
    cin >> x;
    if (!x) rowZeroPos.pb(i + 1);
  }
  rowZeroPos.pb(n + 1);
  rep(m) {
    int x;
    cin >> x;
    if (!x) colZeroPos.pb(i + 1);
  }
  colZeroPos.pb(m + 1);
  debug(rowZeroPos);
  debug(colZeroPos);
  auto count = [&] (int r, int c) -> li {
    debug("r:", r, "c:", c);
    int rowscnt = 0, colscnt = 0;
    rep(i, 1, len(rowZeroPos)) {
      if (rowZeroPos[i] - rowZeroPos[i - 1] - 1 >= r) {
        rowscnt += rowZeroPos[i] - rowZeroPos[i - 1] - r;
      }
    }

    rep(i, 1, len(colZeroPos)) {
      if (colZeroPos[i] - colZeroPos[i - 1] - 1 >= c) {
        colscnt += colZeroPos[i] - colZeroPos[i - 1] - c;
      }
    }
    debug(rowscnt, colscnt);
    return (li)rowscnt * colscnt;
  };
  li ans = 0;
  rep(i, 1, (int)sqrt(k) + 1) {
    if (k % i == 0) {
      ans += count(i, k / i);
      if (k / i != i) {
        ans += count(k / i, i);
      }
    }
  }
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}