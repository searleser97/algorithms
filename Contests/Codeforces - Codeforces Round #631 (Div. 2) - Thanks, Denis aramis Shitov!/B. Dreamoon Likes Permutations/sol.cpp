// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;

#define ordered_set                            \
  tree<int, null_type, less<int>, rb_tree_tag, \
       tree_order_statistics_node_update>
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(n) cin >> v[i];
  vector<bool> lr(n), rl(n);
  ordered_set os;
  rep(n) {
    if (os.find(v[i]) != os.end()) break;
    os.insert(v[i]);
    if (*os.rbegin() == i + 1 && (int)os.order_of_key(i + 1) == i)
      lr[i] = 1;
  }
  debug(lr);
  os.clear();
  rep(i, n - 1, -1) {
    if (os.find(v[i]) != os.end()) break;
    os.insert(v[i]);
    if (*os.rbegin() == n - i && (int)os.order_of_key(n - i) == n - i - 1)
      rl[i] = 1;
  }
  debug(rl);
  vector<pairii> ans;
  rep(n - 1) {
    if (lr[i] == 1 && rl[i + 1] == 1) {
      ans.pb({i + 1, n - i - 1});
    }
  }
  cout << len(ans) << endl;
  rep(len(ans)) {
    cout << ans[i].fi << " " << ans[i].se << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}