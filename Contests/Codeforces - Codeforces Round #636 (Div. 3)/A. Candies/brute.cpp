#include <bits/stdc++.h>
#define f first
#define s second
#define fore(i, a, b) for (int i = (a), ThxMK = (b); i < ThxMK; ++i)
#define pb push_back
#define all(s) begin(s), end(s)
#define _                       \
  ios_base::sync_with_stdio(0); \
  cin.tie(0);                   \
  cout.tie(0);
#define sz(s) int(s.size())
#define ENDL '\n'
using namespace std;
typedef long double ld;
typedef long long lli;
typedef pair<int, int> ii;
#define deb(x) cout << #x ": " << (x) << endl;

int main() {
  _ int n, a;
  cin >> n;
  vector<int> pot(32);
  fore(i, 0, 32) pot[i] = (1 << i);
  while (n--) {
    cin >> a;
    fore(i, 2, 31) if (a % (pot[i] - 1) == 0) {
      cout << a / (pot[i] - 1) << ENDL;
      break;
    }
  }
  return 0;
}