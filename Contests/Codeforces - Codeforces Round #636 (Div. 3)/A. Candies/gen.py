from random import randint

t = randint(1, int(1e4))
print(t)

for _ in range(t):
    n = randint(3, int(1e9))
    print(n)
