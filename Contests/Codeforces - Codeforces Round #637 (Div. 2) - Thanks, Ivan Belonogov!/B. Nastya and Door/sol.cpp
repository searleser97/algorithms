// 27
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) \
  for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) \
  for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e)                             \
  cerr << "\033[48;5;196m\033[38;5;15m" << e \
       << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, (int)v.size()) o << setw(7) << left << v[i];
  return o << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 3

template <class T>
struct SegmentTree {
  T neutro = 0;
  int N;
  vector<T> st;
  function<T(T, T)> F;
  // 3
  SegmentTree(int n, int neut,
      T f(T, T) = [](T a, T b) { return a + b; })
      : neutro(neut), st(2 * n, neutro), N(n), F(f) {}
  // 5
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--)
      st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 5
  // O(lg(2N)), works like replacing arr[i] with val
  void update(int i, T val) {
    for (st[i += N] = val; i > 1; i >>= 1)
      st[i >> 1] = F(st[i], st[i ^ 1]);
  }
  // 9
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    T ans = neutro;
    for (l += N, r += N; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};

void _main(int tc) {
  int n, k;
  cin >> n >> k;
  vector<int> v(n);
  rep(i, n) cin >> v[i];
  vector<int> peaks(n);

  for (int i = 0; i <= n - 2; i++) {
    while (i + 1 < n && v[i] <= v[i + 1]) {
      i++;
    }
    if (i != 0 && i != n - 1)
      peaks[i] = 1;
    while (i + 1 < n && v[i] > v[i + 1]) {
      i++;
    }
  }

  SegmentTree<int> st(n, 0);
  rep(i, n) {
    st[i] = peaks[i];
  }
  debug(peaks);

  st.build();

  int ans = 0;
  int L = 0;
  for (int i = 0; i < n - k + 1; i++) {
    int l = i + 1;
    int r = i + k - 2; // check wich ones are really peaks
    int q = st.query(l, r);
    if (q > ans) {
      ans = q;
      L = i;
    }
  }
  cout << ans + 1 << " " << L + 1 << endl;

}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) {
    _main(i + 1);
  }
}