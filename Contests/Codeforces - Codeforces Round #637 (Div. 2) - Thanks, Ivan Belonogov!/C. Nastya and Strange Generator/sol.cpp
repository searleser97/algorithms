// 27
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) \
  for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) \
  for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e)                             \
  cerr << "\033[48;5;196m\033[38;5;15m" << e \
       << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, (int)v.size()) o << setw(7) << left << v[i];
  return o << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 3

// 5
// st = segment tree, st[1] = root, H = height of d
// u = updates, d = delayed updates
// neutro = operation neutral val
// e.g. for sum is 0, for multiplication
// is 1, for gcd is 0, for min is INF, etc.
// 7
template <class T>
struct SegmentTree {
  T neutro = 0;
  int N, H;
  vector<T> st, d;
  vector<bool> u;
  function<T(T, T)> F;
  // 5
  SegmentTree(int n, T val,
      T f(T, T) = [](T a, T b) { return a + b; })
      : st(2 * n, val), d(n), u(n), F(f) {
    H = sizeof(int) * 8 - __builtin_clz(N = n);
  }
  // 7
  void apply(int i, T val, int k) {
    // operation to update st[i] in O(1) time
    st[i] += val * k;  // updates the tree
    // operation to update d[i] in O(1) time
    // which updates values for future updates
    if (i < N) d[i] += val, u[i] = 1;
  }
  // 3
  void calc(int i) {
    if (!u[i]) st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 4
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--) calc(i);
  }
  // 4
  // O(lg(N))
  void build(int p) {
    while (p > 1) p >>= 1, calc(p);
  }
  // 12
  // O(lg(N))
  void push(int p) {
    for (int s = H, k = 1 << (H - 1); s;
         s--, k >>= 1) {
      int i = p >> s;
      if (u[i]) {
        apply(i << 1, d[i], k);
        apply(i << 1 | 1, d[i], k);
        u[i] = 0, d[i] = 0;
      }
    }
  }
  // 10
  // O(lg(N)), [l, r]
  void update(int l, int r, T val) {
    push(l += N), push(r += N);
    int ll = l, rr = r, k = 1;
    for (; l <= r; l >>= 1, r >>= 1, k <<= 1) {
      if (l & 1) apply(l++, val, k);
      if (~r & 1) apply(r--, val, k);
    }
    build(ll), build(rr);
  }
  // 10
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    push(l += N), push(r += N);
    T ans = neutro;
    for (; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};

void _main(int tc) {
   int n;
   cin >> n;
   vector<int> v(n);
   rep(i, n) cin >> v[i];
   set<int> s;
   rep(i, n) {
     s.insert(i);
   }

   for (int i = 0; i <= n; i++) {

   }
   
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}