#include <bits/stdc++.h>
using namespace std;
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.first << ", " << p.second << ")";
}
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  for (int i = 0; ((0 < int(v.size()) ? 1 : -1) < 0) ? i > int(v.size()) : i < int(v.size()); i += (0 < int(v.size()) ? 1 : -1)) o << setw(7) << left << v[i];
  return o << "\033[0m" << '\n' << "\033[48;5;196m\033[38;5;15m";
}
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
}
void _main(int tc) {
  int n, k;
  cin >> n >> k;
  vector<int> v(n);
  for (int i = 0; ((0 < n ? 1 : -1) < 0) ? i > n : i < n; i += (0 < n ? 1 : -1)) cin >> v[i];
  for (int obj = 1; ((1 < 10001 ? 1 : -1) < 0) ? obj > 10001 : obj < 10001; obj += (1 < 10001 ? 1 : -1)) {
    deque<int> dq;
    dq.push_back(0);
    vector<int> ans;
    int currsum = 0;
    for (int i = 0; ((0 < k - 1 ? 1 : -1) < 0) ? i > k - 1 : i < k - 1; i += (0 < k - 1 ? 1 : -1)) {
      currsum += v[i];
      dq.push_back(v[i]);
      ans.push_back(v[i]);
    }
    int fail = 0;
    for (int i = k - 1; i < n;) {
      if (fail > k) {
        fail = -1;
        break;
      }
      currsum -= dq.front();
      dq.pop_front();
      if (currsum + v[i] == obj) {
        currsum = obj;
        dq.push_back(v[i]);
        ans.push_back(v[i]);
        fail = 0;
        i++;
      } else {
        if (obj - currsum < 1 or obj - currsum > n) {
          fail = -1;
          break;
        }
        dq.push_back(obj - currsum);
        ans.push_back(obj - currsum);
        currsum = obj;
        fail++;
      }
    }
    if (fail == -1) {
      continue;
    } else {
      debug(obj);
      cout << int(ans.size()) << '\n';
      for (int i = 0; ((0 < int(ans.size()) ? 1 : -1) < 0) ? i > int(ans.size()) : i < int(ans.size()); i += (0 < int(ans.size()) ? 1 : -1)) { cout << ans[i] << " \n"[i + 1 == int(ans.size())]; }
      return;
    }
  }
  cout << -1 << '\n';
}
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  int tc;
  cin >> tc;
  for (int i = 0; ((0 < tc ? 1 : -1) < 0) ? i > tc : i < tc; i += (0 < tc ? 1 : -1)) _main(i + 1);
}
