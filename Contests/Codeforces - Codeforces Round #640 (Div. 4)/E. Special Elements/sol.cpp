// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(n) cin >> v[i];
  int especial = 0;

  auto find = [&](int e) -> bool {
    int currSum = 0;
    for (int l = 0, r = 0; l < n;) {
      while (r < n && r - l < 2 && currSum < e) {
        currSum += v[r];
        r++;
      }
      debug("1-", l, r, r - l, currSum, e);
      while (r < n && currSum < e) {
        currSum += v[r];
        r++;
      }
      debug("2-", l, r, r - l, currSum, e);
      if (r - l >= 2 && currSum == e) {
        debug("found:", l, r);
        return true;
      }
      currSum -= v[l];
      l++;
    }
    return false;
  };

  rep(n) {
    bool hasSum = find(v[i]);
    if (hasSum) especial++;
    debug((hasSum ? "true" : "false"));
  }
  cout << especial << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}