// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

#define SIZE 100000 + 1

int P[SIZE * 2];

string convertToNewString(const string &s) {
    string newString = "@";

    for (int i = 0; i < s.size(); i++) {
        newString += "#" + s.substr(i, 1);
    }

    newString += "#$";
    return newString;
}

bool isPalindrome(const string &str, int l, int h) {
  while (h > l) {
    if (str[l++] != str[h--]) {
      return false;
    }
  }
  return true;
}

void longestPalindromeSubstring(const string &s) {
    string Q = convertToNewString(s);
    int c = 0, r = 0;

    for (int i = 1; i < Q.size() - 1; i++) {
        int iMirror = c - (i - c);

        if(r > i) {
            P[i] = min(r - i, P[iMirror]);
        }
        while (Q[i + 1 + P[i]] == Q[i - 1 - P[i]]){
            P[i]++;
        }
        if (i + P[i] > r) {
            c = i;
            r = i + P[i];
        }
    }
    int maxPalindrome = 0;
    int centerIndex = 0;
    for (int i = 1; i < Q.size() - 1; i++) {

        if (P[i] > maxPalindrome) {
            maxPalindrome = P[i];
            centerIndex = i;
        }
    }
    cout << maxPalindrome << "\n";

    if (maxPalindrome == 1) {
      cout << s.size() << endl;
    } else {
      int cnt = 0;
      for (int i = 0, h = maxPalindrome - 1; h < s.size(); i++, h++) {
        if (isPalindrome(s, i, h)) cnt++;
      }
      cout << cnt << endl;
    }
    // return s.substr( (centerIndex - 1 - maxPalindrome) / 2, maxPalindrome);
}

void _main(int tc) {
  int n;
  cin >> n;
  string s;
  cin >> s;
  longestPalindromeSubstring(s);
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}