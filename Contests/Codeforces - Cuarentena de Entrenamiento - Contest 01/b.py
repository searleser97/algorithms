def main():
    n = int(input())
    s = []
    for i in range(n):
        aux = int(input())
        if (aux >= 0):
            s.append(aux)
    if (len(s) == 0):
        print(0)
        return

    s.sort()

    if s[0] != 0:
        print(0)
        return

    for i in range(1, len(s)):
        diff = s[i] - s[i - 1]
        if diff <= 1:
            continue
        else:
            print(s[i - 1] + 1)
            return

    print(s[len(s) - 1] + 1)


main()
