// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

// 9
typedef long double ld;
const ld eps = 1e-9;

bool eq(ld a, ld b) { return abs(a - b) <= eps; }
bool neq(ld a, ld b) { return abs(a - b) > eps; }
bool gt(ld a, ld b) { return a - b > eps; }
bool lt(ld a, ld b) { return b - a > eps; }
bool gte(ld a, ld b) { return a - b >= -eps; }
bool lte(ld a, ld b) { return b - a >= -eps; }

const ld pi = acos(-1), inf = 1 << 30;
// (PointB - PointA) = vector from A to B.
struct Point {
  ld x, y;
  Point() : x(0), y(0) {}
  Point(ld x, ld y) : x(x), y(y) {}
  // 3
  Point operator+(const Point &p) const {
    return Point(x + p.x, y + p.y);
  }
  // 3
  Point operator-(const Point &p) const {
    return Point(x - p.x, y - p.y);
  }
  // 3
  Point operator*(const ld &k) const {
    return Point(x * k, y * k);
  }
  // 3
  Point operator/(const ld &k) const {
    return Point(x / k, y / k);
  }
  // 3
  bool operator==(const Point &p) const {
    return eq(x, p.x) && eq(y, p.y);
  }
  // 3
  bool operator!=(const Point &p) const {
    return !(*this == p);
  }
  // 3
  bool operator<(const Point &p) const {
    return eq(x, p.x) ? lt(y, p.y) : lt(x, p.x);
  }
  // 3
  bool operator>(const Point &p) const {
    return eq(x, p.x) ? gt(y, p.y) : gt(x, p.x);
  }

  ld norm() const { return sqrt(x * x + y * y); }

  ld dot(const Point &p) { return x * p.x + y * p.y; }
  // 3
  ld cross(const Point &p) {
    return x * p.y - y * p.x;
  }

  Point perpendicularLeft() { return Point(-y, x); }

  Point perpendicularRight() { return Point(y, -x); }
  // 5
  Point rotate(ld deg) {
    ld rad = (deg * pi) / 180.0;
    return Point(x * cos(rad) - y * sin(rad),
                 x * sin(rad) + y * cos(rad));
  }

  Point unit() const { return (*this) / norm(); }
  // 3
  Point projectOn(const Point &p) {
    return p.unit() * (dot(p) / p.norm());
  }
  // 4
  ld angleWith(const Point &p) {
    ld x = dot(p) / norm() / p.norm();
    return acos(max(-1.0L, min(1.0L, x)));
  }
  // 4
  bool isPerpendicularWith(const Point &p) {
    return dot(p);
  }
  // 5
  // ans > 0 p is on the left
  // ans < 0 p is on the right
  // ans == 0 p has our same direction
  ld positionOf(const Point &p) { return cross(p); }
};
// 3
istream &operator>>(istream &is, Point &p) {
  return is >> p.x >> p.y;
}
// 3
ostream &operator<<(ostream &os, const Point &p) {
  return os << "(" << p.x << ", " << p.y << ")";
}

bool isAbove(Point& a, Point& b, Point& p) {

  if ((b - a).positionOf(p - a) <= 0) return true;
  return false;
}

void intersections(vector<Point> &ps, int index) {
  if (index == 0) {
    cout << 0 << endl;
    return;
  }
  vector<int> visiblePeaks;
  int ans = 0;
  Point tocheck = ps[index - 1];
  for (int i = index - 1; ~i; i--) {
    if (isAbove(ps[index], tocheck, ps[i])) {
      visiblePeaks.pb(i + 1);
      ans++;
      tocheck = ps[i];
    }
  }
  cout << ans << " ";
  if (ans) {
    reverse(all(visiblePeaks));
    rep(i, visiblePeaks.size()) {
      if (i) cout << " ";
      cout << visiblePeaks[i];
    }
  }
  cout << endl;
}

void _main(int tc) {
  int n;
  cin >> n;
  Point start(0, 0);
  vector<Point> ps;
  ld rad = (45 * pi) / 180.0;
  rep(i, n) {
    int u, d;
    cin >> u >> d;
    ld uNewx = start.x + u / tan(rad);
    ld uNewy = start.y + u;
    start = Point(uNewx, uNewy);
    ps.pb(start);
    ld dNewx = start.x + d * tan(rad);
    ld dNewy = start.y - d;
    start = Point(dNewx, dNewy);
  }


  rep(i, ps.size()) {
    intersections(ps, i);
  }

}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}