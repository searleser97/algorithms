// https://codeforces.com/group/PzLcHnJr9l/contest/273892/problem/D
// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define rrep(_, n) forr(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

// s = source
typedef long long int T;  // sum of costs might overflow
T inf = 1ll << 62;
typedef pair<T, int> DistNode;
vector<vector<int>> adj;
vector<vector<T>> cost;

void init(int N) {
  adj.assign(N, vector<int>());
  cost.assign(N, vector<T>());
}

// Assumes Directed Graph
void addEdge(int u, int v, T c) { adj[u].push_back(v), cost[u].push_back(c); }

// ~ O(E * lg(V))
vector<T> dijkstra(int s) {
  vector<T> dist(adj.size(), inf);
  priority_queue<DistNode> q;
  q.push({0, s}), dist[s] = 0;
  while (q.size()) {
    DistNode top = q.top();
    q.pop();
    int u = top.second;
    if (dist[u] < -top.first) continue;
    for (int i = 0; i < adj[u].size(); i++) {
      int v = adj[u][i];
      T d = dist[u] + cost[u][i];
      if (d < dist[v]) q.push({-(dist[v] = d), v});
    }
  }
  return dist;
}

// val = value
typedef pair<int, int> Val;
map<Val, int> intForVal;
int mapId = 2;
int Map(Val val) {
  if (intForVal.count(val)) return intForVal[val];
  return intForVal[val] = mapId++;
}

void _main(int tc) {
  int m, n, offset = 2;
  cin >> m >> n;
  init(offset + 1440 * n);
  vector<int> us(m), vs(m);
  vector<li> ds(m), cs(m), dt(m), ws(n), ks(n);
  // ds: delay time, cs: costs, dt: departure time
  // ws: wait time, ks: hotel cost
  rep(i, m) {
    cin >> us[i] >> vs[i] >> ds[i] >> cs[i];
    us[i]--, vs[i]--;
    string dtime;
    cin >> dtime;
    int hh = stoi(dtime.substr(0, 2));
    int mm = stoi(dtime.substr(3, 2));
    int time = 60 * hh + mm;
    dt[i] = time;
  }
  rep(i, n) { cin >> ws[i] >> ks[i]; }
  rep(u, n) {
    rep(t, 1440) {
      int um = Map({u, t});
      int vm = Map({u, (t + 1) % 1440});
      if (t == 479)  // 7:59 am
        addEdge(um, vm, ks[u]);
      else
        addEdge(um, vm, 0);
    }
  }
  rep(i, m) {
    int u = Map({us[i], dt[i]});
    int v = Map({vs[i], (dt[i] + ds[i] + ws[vs[i]]) % 1440});
    addEdge(u, v, cs[i]);
  }
  rep(t, 1440) {
    int source = Map({0, t});
    int target = Map({n - 1, t});
    addEdge(0, source, 0);
    addEdge(target, 1, 0);
  }
  auto dist = dijkstra(0);
  if (dist[1] == inf)
    cout << -1 << endl;
  else
    cout << dist[1] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}