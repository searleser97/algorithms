// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define rrep(_, n) forr(_, n, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

struct WaveletTree {
  WaveletTree *l, *r;
  int lo, hi;
  vector<int> lcount, pref;
  // 17
  // O(N*lg(A))
  WaveletTree(vector<int>::iterator from,
              vector<int>::iterator to, int lo,
              int hi) {
    this->lo = lo, this->hi = hi;
    if (lo == hi or from >= to) return;
    int mid = (lo + hi) >> 1;
    auto f = [mid](int x) { return x <= mid; };
    lcount.reserve(to - from + 1);
    lcount.push_back(0);
    for (auto it = from; it != to; it++)
      lcount.push_back(lcount.back() + f(*it));
    auto pivot = stable_partition(from, to, f);
    l = new WaveletTree(from, pivot, lo, mid);
    r = new WaveletTree(pivot, to, mid + 1, hi);
  }
  // 8
  // O(lg(A)) count of elements <= to k in [a, b]
  int lte(int a, int b, int k) {
    if (a > b or k < lo) return 0;
    if (hi <= k) return b - a + 1;
    int lc = lcount[a - 1], rc = lcount[b];
    return l->lte(lc + 1, rc, k) +
           r->lte(a - lc, b - rc, k);
  }
};


 // don't pass n as param
vector<int> compress(vector<int>& v, int n = 0) {
  unordered_map<int, int> Map;
  set<int> s(v.begin(), v.end());
  vector<int> c(v.size());
  for (auto& e : s) Map[e] = n++;
  for (int i = 0; i < v.size(); i++)
    c[i] = Map[v[i]];
  return c;
}

void _main(int tc) {
  li emergencia = 0;
  int n;
  cin >> n;
  vector<int> c(n);
  rep(i, n) cin >> c[i];
  c = compress(c);
  vector<int> mapa(n);
  vector<int> dp(n);

  rep(i, n) {
    mapa[c[i]]++;
    dp[i] = mapa[c[i]];
  }

  WaveletTree wt(dp.begin(), dp.end(), *min_element(all(dp)), *max_element(all(dp)));

  mapa.assign(n, 0);

  for (int i = n - 1; ~i; i--) {
    mapa[c[i]]++;
    dp[i] = mapa[c[i]];
  }

  li cnt = 0;
  forn(i, 1, n) {
    cnt += i - wt.lte(1, i, dp[i]);
  }
  cout << cnt << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
} 
