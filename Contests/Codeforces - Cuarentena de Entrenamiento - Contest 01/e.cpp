// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef int li;
typedef long double ld;
// 12

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;
  // 6
  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
    // return a * b;
  }
  // 5
  // Inverse of F
  inline T I(T a, T b) {
    return a - b;
    // return a / b;
  }
  // 7
  // O(N)
  void build() {
    for (int i = 1; i < bit.size(); i++) {
      int j = i + (i & -i);
      if (j < bit.size()) bit[j] = F(bit[j], bit[i]);
    }
  }
  // 4
  // O(lg(N))
  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }
  // 6
  // O(lg(N))
  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }
  // 6
  // O(lg(N)), [l, r]
  T query(int l, int r) {
    return I(query(r), query(--l));
  }

  T& operator[](int i) { return bit[++i]; }
};

unordered_map<li, int> Map;

 // don't pass n as param
vector<li> compress(vector<li>& v, int n = 0) {
  set<li> s(v.begin(), v.end());
  vector<li> c(v.size());
  for (auto& e : s) Map[e] = n++;
  for (int i = 0; i < v.size(); i++)
    c[i] = Map[v[i]];
  return c;
}

void _main(int tc) {
  li emergencia = 1;
  li n;
  cin >> n;
  vector<li> v(n);
  rep(i, n) cin >> v[i];
  vector<li> c = compress(v);
  vector<li> mapa(c.size() + emergencia), mapa2(c.size() + emergencia);
  vector<li> dp(c.size() + emergencia), dp2(c.size() + emergencia);
  
  rep(i, n) {
    mapa[c[i]]++;
    dp[i] = mapa[c[i]];
  }

  for (int i = n - 1; ~i; i--) {
    mapa2[c[i]]++;
    dp2[i] = mapa2[c[i]];
  }

  BIT<li> bit(max(*max_element(all(dp2)), *max_element(all(dp))) + emergencia);

  long long int ans = 0;

  /*for (li i = 0; i < n; i++) {
    ans += i - bit.query(dp2[i]);
    bit.update(dp[i], 1);
  }*/

  for (li i = n - 1; ~i; i--) {
    ans += bit.query(dp[i] - 1);
    bit.update(dp2[i], 1);
  }
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}