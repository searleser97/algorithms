// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

struct mipar {
  li first, second;
};

struct mipar2 {
  li first, second;
};

bool operator<(const mipar &a, const mipar &b) {
  if (a.first == b.first) return a.second > b.second;
  return a.first > b.first;
}

bool operator<(const mipar2 &a, const mipar2 &b) {
  if (a.first == b.first) return a.second < b.second;
  return a.first < b.first;
}

vector<vector<li>> mem;

li dp(int pos, int k, vector<int> &v) {
  if (pos >= v.size() - 1) {
    return 0;
  }
  if (mem[pos][k] != -1) {
    return mem[pos][k];
  }
  li ans = -(1ll << 62);
  
  unordered_map<li, li> freq;

  set<mipar> mysetHigh; set<mipar2> mysetLow;
  for (int i = pos; i < v.size(); i++) {
    mysetHigh.erase({freq[v[i]], v[i]});
    mysetLow.erase({freq[v[i]], v[i]});
    freq[v[i]]++;
    mysetHigh.insert({freq[v[i]], v[i]});
    mysetLow.insert({freq[v[i]], v[i]});
    li high = (*mysetHigh.begin()).second;
    li low = (*mysetLow.begin()).second;
    if (k > 1)
      ans = max(ans, high - low + dp(i + 1, k - 1, v));
    else
      ans = high - low;
  }
  return mem[pos][k] = ans;
}

void _main(int tc) {
  li n, k;
  cin >> n >> k;
  vector<int> v(n);
  mem.assign(105, vector<li>(105, -1));
  rep(i, n) cin >> v[i];
  cout << dp(0, k, v) << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}