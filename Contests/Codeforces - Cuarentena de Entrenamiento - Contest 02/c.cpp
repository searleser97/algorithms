// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

// 7
struct UnionFind {
  int n;
  vector<int> dad, size;

  UnionFind(int N) : n(N), dad(N), size(N, 1) {
    while (N--) dad[N] = N;
  }
  // 4
  // O(lg*(N))
  int root(int u) {
    if (dad[u] == u) return u;
    return dad[u] = root(dad[u]);
  }
  // 8
  // O(1)
  void join(int u, int v) {
    int Ru = root(u), Rv = root(v);
    if (Ru == Rv) return;
    if (size[Ru] > size[Rv]) swap(Ru, Rv);
    --n, dad[Ru] = Rv;
    size[Rv] += size[Ru];
  }
  // 4
  // O(lg*(N))
  bool areConnected(int u, int v) {
    return root(u) == root(v);
  }
  // 4
  int getSize(int u) { return size[root(u)]; }

  int numberOfSets() { return n; }
};


void _main(int tc) {
  int n, m, q;
  cin >> n >> m >> q;
  UnionFind uf(n);
  vector<pairii> connections(n - 1);
  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    connections[i] = {u, v};
  }
  set<pairii> broken;
  vector<pairii> brokenA(m);
  rep(i, m) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    broken.insert({u, v});
    brokenA[i] = make_pair(u, v);
  }
  rep(i, n - 1)
    if (!broken.count(connections[i]))
      uf.join(connections[i].fi, connections[i].se);
  
  vector<tuple<int, int, int>> queries(q);
  rep(i, q) {
    int x, t;
    cin >> x >> t;
    t--;
    queries.pb({x, t, i});
  }
  sort(all(queries));
  reverse(all(queries));
  reverse(all(brokenA));
  int currTime = m;
  int j = 0;
  vector<int> ans(q);
  rep(i, q) {
    while (get<0>(queries[i]) < currTime) {
      uf.join(brokenA[j].fi, brokenA[j].se);
      j++;
      currTime--;
    }
    ans[get<2>(queries[i])] = uf.getSize(get<1>(queries[i]));
  }
  rep(i, q) cout << ans[i] << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}