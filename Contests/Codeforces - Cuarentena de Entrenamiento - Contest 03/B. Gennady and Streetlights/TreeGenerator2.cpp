#include <bits/stdc++.h>
using namespace std;

#define fi first
#define se second
#define pb push_back
#define mod(n,k) ( ( ((n) % (k)) + (k) ) % (k))
#define forn(i,a,b) for(int i = a; i < b; i++)
#define forr(i,a,b) for(int i = a; i >= b; i--)
#define all(x) (x).begin(), (x).end()

typedef long long ll;
typedef long double ld;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int maxn = 20;
const int maxh = 5;

int getAleatorio(int l,int r){
	return l + (rand() % (r - l + 1));
}
int main(){
	//ios_base::sync_with_stdio(0); cin.tie(0);
	srand(time(0));
	int T = 1;
	int ncaso = 1;
	while(T--){
		string nombreArchivo = "input"+to_string(ncaso++)+".in";
		ofstream fout(nombreArchivo);
		int N = getAleatorio(maxn,maxn);
		fout << N << '\n';
		for(int i=0; i < N; i++){
			int val = getAleatorio(0,1);
			fout << val;
			if(i != N-1) fout << ' ';
		}
		fout << '\n';

		forn(i,2,N+1){
			int u = i;
			int v = getAleatorio(1,i-1);
			fout << u << ' ' << v << '\n';
		}
		int q = getAleatorio(maxn,maxn);
		fout << q << '\n';
		for(int i=0; i < q; i++){
			int t = getAleatorio(1,2);
			int x = getAleatorio(1,N);
			fout << t << ' ' << x << '\n';
		}
	}
	return 0;
}
/*
__builtin_mul_overflow(x,y,&x)
-fsplit-stack
*/
