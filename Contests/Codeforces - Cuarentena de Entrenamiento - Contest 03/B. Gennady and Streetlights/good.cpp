#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define __ ios_base::sync_with_stdio(0);cin.tie(0);
#define fi first
#define se second
#define pb push_back
#define all(x) x.begin(),x.end()
#define forn(i,a,n) for(int i=a; i < n; i++)
typedef long long int lli;
typedef long double Double;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef vector<vi> vvi;
#define MAXN 100005
int cont,n;
vi adj[MAXN];
pii subtree[MAXN];
int val[MAXN],s[MAXN];
int st[4*MAXN],lazy[4*MAXN];

void build(int l, int r, int pos){
	if(l == r){
		st[pos] = val[l];
		return;
	}
	int m = (l+r)/2;
	build(l, m, 2*pos+1);
	build(m+1,r,2*pos+2);
	st[pos] = st[2*pos+1] + st[2*pos+2];
}

void propagate(int l, int r,int pos){
	st[pos] = (r - l + 1) - st[pos];
	lazy[pos] = 0;
	if(l != r){
		lazy[2*pos+1] ^= 1;
		lazy[2*pos+2] ^= 1;
	}
}

void update(int l, int r, int ini, int fin, int pos){
	if(lazy[pos]){
		propagate(ini,fin,pos);
		lazy[pos] = 0;
	}
	if( fin < l || ini > r) return;
	
	if( ini >= l && fin <= r){
		lazy[pos] = 1;
		propagate(ini,fin,pos);
		return;
	}
	int m = (ini + fin)/2;
	update(l,r,ini,m,2*pos+1);
	update(l,r,m+1,fin,2*pos+2);
	st[pos] = st[2*pos+1] + st[2*pos+2];
}

void update(int x){
	update(subtree[x].fi,subtree[x].se,0,n-1,0);
}

int query(int l, int r, int ini, int fin, int pos){
	if(lazy[pos]){
		propagate(ini,fin,pos);
		lazy[pos] = 0;
	}
	if( fin < l || ini > r) return 0;

	if(ini >= l && fin <= r){
		return st[pos];
	}
	int m = (ini + fin)/2;
	return query(l,r, ini, m ,2*pos+1) + query(l,r,m+1,fin,2*pos+2);
}

int query(int x){
	
	return (subtree[x].se - subtree[x].fi + 1) - query(subtree[x].fi,subtree[x].se,0,n-1,0);
}

void dfs(int u, int p){
	val[cont] = s[u];
	subtree[u].fi = cont++;
	for(int v : adj[u]){
		if(v == p) continue;
		dfs(v,u);
	}
	subtree[u].se = cont-1;
}

int main(){__
	int q;
	cin >> n;
	for(int i =1; i <=n ; i++){
		int x;
		cin >> x;
		s[i] = x;
	}
	for(int i =0; i < n-1; i++){
		int u,v;
		cin >> u >> v;
		adj[u].pb(v);
		adj[v].pb(u);
	}
	cont  =0;
	dfs(1,0);
	build(0,n-1,0);
	forn(i, 0, n) {
		cout << query(i, i, 0, n - 1, 0) << " ";
	}
	cout << endl;
	cin >> q;
	for(int i =0; i < q; i++){
		int t,x;
		cin >> t >> x;
		cout << t << " " << x << " " << subtree[x].fi << " " << subtree[x].se << endl;
		if(t == 1){
			update(x);
		}
		else{
			cout << query(x) << endl;
		}
	}
	return 0;
}

