// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
template <class T>
struct SegmentTree {
  T neutro = 0;
  int N, H;
  vector<T> st;
  vector<char> u;
  function<T(T, T)> F;
  // 5
  SegmentTree(
      int n, T val, T f(T, T) = [](T a, T b) { return a + b; })
      : st(2 * n, val), u(n), F(f) {
    H = sizeof(int) * 8 - __builtin_clz(N = n);
  }
  // 7
  void apply(int i, T val, int k) {
    st[i] = k - st[i];
    if (i < N) u[i] ^= 1;
  }
  // 3
  inline void calc(int i) {
    if (!u[i]) st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 4
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--) calc(i);
  }
  // 4
  // O(lg(N))
  void build(int p) {
    while (p > 1) p >>= 1, calc(p);
  }
  // 12
  // O(lg(N))
  void push(int p) {
    for (int s = H, k = 1 << (H - 1); s; s--, k >>= 1) {
      int i = p >> s;
      if (u[i]) {
        apply(i << 1, 0, k);
        apply(i << 1 | 1, 0, k);
        u[i] = 0;
      }
    }
  }
  // 10
  // O(lg(N)), [l, r]
  void update(int l, int r, T val) {
    push(l += N), push(r += N);
    int ll = l, rr = r, k = 1;
    for (; l <= r; l >>= 1, r >>= 1, k <<= 1) {
      if (l & 1) apply(l++, val, k);
      if (~r & 1) apply(r--, val, k);
    }
    build(ll), build(rr);
  }
  // 10
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    push(l += N), push(r += N);
    T ans = neutro;
    for (; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};


vector<int> firstPos, lastPos;
vector<vector<int>> adj;

void init(int N) {
  adj.assign(N, vector<int>());
  firstPos.assign(N, -1);
  lastPos.assign(N, -1);
}
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}

map<int, int> Map;
// 10
// O(N)
int id = 0;
void eulerTour(int u, int p) {
  firstPos[u] = id;
  Map[id] = u;
  id++;
  for (int v : adj[u])
    if (v != p)
      eulerTour(v, u);
  lastPos[u] = id - 1;
}

// 12
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> s(n);
  init(n);
  rep(i, n) {
    cin >> s[i];
  }
  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    addEdge(u, v);
  }
  SegmentTree<int> st(n, 0);

  eulerTour(0, -1);
  rep(i, n) {
    st[i] = s[Map[i]];
  }
  st.build();
  int q;
  cin >> q;
  rep(i, q) {
    int t, x;
    cin >> t >> x;
    x--;
    if (t == 1) {
      st.update(firstPos[x], lastPos[x], 0);
    } else {
      cout << lastPos[x] - firstPos[x] + 1 - st.query(firstPos[x], lastPos[x]) << endl;
    }
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
}