// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

li mod(li a, li b) { return (b + (a % b)) % b; }

int sign(li x) { return x < 0 ? -1 : x > 0; }

li floor(li a, li b) {
  if (sign(a) != sign(b) && a % b)
    return a / b - 1;
  else
    return a / b;
}

tuple<li, li, li> extendedGCD(li a, li b) {
  if (!a) return {b, 0, 1};
  li d, x, y;
  tie(d, x, y) = extendedGCD(mod(b, a), a);
  return {d, y - floor(b, a) * x, x};
}

vector<li> factorials(int n, li m) {
  vector<li> fact(++n);
  fact[0] = 1;
  for (int i = 1; i < n; i++) fact[i] = (fact[i - 1] * i) % m;
  return fact;
}

// O(lg(N))
li modInv(li n, li m) {
  li g, x, y;
  tie(g, x, y) = extendedGCD(n, m);
  if (g != 1) throw "No solution";
  return (x % m + m) % m;
}

// O(lg(p))
li nCk(int n, int k, li p, vector<li> &fact) {
  return fact[n] * modInv(fact[k], p) % p * modInv(fact[n - k], p) % p;
}

const int modulo = 998244353;
void _main(int tc) {
  li n, x, a, b;
  cin >> n >> x >> a >> b;
  auto fact = factorials(2e6, modulo);
  vector<li> v(n);
  rep(i, n) cin >> v[i];

  li pntsToA = 0;
  rep(i, n) {
    if (v[i] < a) pntsToA += (a - v[i]);
  }

  li k = x - pntsToA;
  if (k < 0) {
    cout << 0 << endl;
    return;
  }

  li ans1 = nCk(n + k - 1, k, modulo, fact);

  // ==============================================

  li pntsToB = 0;
  rep(i, n) {
    if (v[i] < b) pntsToB += (b - v[i]);
  }

  k = x - pntsToB;
  if (k < 0) {
    cout << ans1 << endl;
    return;
  }
  li ans2 = nCk(n + k - 1, k, modulo, fact);

  li ans = mod(ans1 - ans2, modulo);
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}