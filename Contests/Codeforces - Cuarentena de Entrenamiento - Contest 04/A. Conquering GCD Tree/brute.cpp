// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
vector<vector<int>> adj;
vector<pairii> dp;
vector<int> h;
void init(int N) {
  adj.assign(N, vector<int>());
  dp = vector<pairii>(N);
  h = vector<int>(N);
}

void addEdge(int u, int v) { adj[u].pb(v); }

pairii recursiva(int u, int p) {
  int sz = 1;
  int g = h[u];
  for (auto& v : adj[u]) {
    if (v != p) {
      auto res = recursiva(v, u);
      sz += res.first;
      g = __gcd(g, res.se);
    }
  }
  return dp[u] = {sz, g};
}

void _main(int tc) {
  int n;
  cin >> n;
  init(n);
  rep(i, n) cin >> h[i];
  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    addEdge(u, v);
    addEdge(v, u);
  }

  pairii ans = {0, 0};
  vector<tuple<int, int, int>> output;
  rep(j, n) {
    recursiva(j, j);
    rep(i, n) {
      if (dp[i].first > ans.first && dp[i].second > 1) ans = dp[i];
      if (dp[i].se > 1) output.pb({dp[i].fi, dp[i].se, i});
    }
  }
  // cout << ans.first << endl;
  // cout << ans.se << endl;
  sort(all(output));
  reverse(all(output));
  for (auto &e : output) {
    cout << get<0>(e) << " " << get<1>(e) << " " << get<2>(e) << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}