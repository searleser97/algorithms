// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;

// 13
vector<int> sieve, primes;

// ~O(N * lg(lg(N)))
void primeSieve(int n) {
  sieve.assign(n + 1, 0);
  primes = {};
  for (int i = 3; i * i <= n; i += 2)
    if (!sieve[i])
      for (int j = i * i; j <= n; j += 2 * i)
        if (!sieve[j]) sieve[j] = i;
  primes.push_back(2);
  for (int i = 3; i < n; i++)
    if (!sieve[i] && (i & 1)) primes.push_back(i);
}

struct Graph {
  map<int, int> m;
  int id = 0;
  vector<vector<int>> adj;
  Graph() {}
  Graph(int n) : adj(n) {}
  void addEdge(int u, int v) {
    if (!m.count(u)) m[u] = id++;
    if (!m.count(v)) m[v] = id++;
    u = m[u], v = m[v];
    adj[u].pb(v);
  }
};

vector<int> h, tsize;
vector<vector<pairii>> uv;
vector<set<int>> nodes;
int maxh = 0;

void init(int N) {
  h = vector<int>(N);
  uv = vector<vector<pairii>>(1e6);
  nodes = vector<set<int>>(1e6);
  primeSieve(1e6);
}

int getSz(int u, int p, Graph& g) {
  int sz = 1;
  for (auto& v : g.adj[u]) {
    if (v == p) continue;
    sz += getSz(v, u, g);
  }
  return tsize[u] = sz;
}

// O(lg(N)) n <= sieve.size()
void getPrimeFactors(int n, int u, int v) {
  while (n > 1) {
    int p = n & 1 ? sieve[n] : 2, c;
    if (!p) p = n;
    while (n % p == 0) n /= p;
    uv[p].pb({u, v});
    nodes[p].insert(u), nodes[p].insert(v);
  }
}

void _main(int tc) {
  int n;
  cin >> n;
  init(n);
  rep(i, n) {
    cin >> h[i];
  }
  short flag = 0;
  rep(i, n) {
    if (h[i] > 1) flag = 1;
  }
  if (flag == 0) {
    cout << 0 << endl;
    return;
  }

  if (n == 1) {
    cout << 1 << endl;
    return;
  }

  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    int g = __gcd(h[u], h[v]);
    getPrimeFactors(g, u, v);
  }
  int ans = flag;
  for (auto & pr : primes) {
    Graph g(nodes[pr].size());
    for (auto& edge : uv[pr]) {
      g.addEdge(edge.fi, edge.se);
      g.addEdge(edge.se, edge.fi);
    }
    tsize.assign(nodes[pr].size(), -1);
    for (int u = 0; u < nodes[pr].size(); u++) {
      if (tsize[u] == -1) {
        ans = max(ans, getSz(u, u, g));
      }
    }
  }
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}