// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12

const ld eps = 1e-9;
bool lte(ld a, ld b) { return b - a >= -eps; }

bool comp(const vector<int> &a, const vector<int> &b) { return a[0] < b[0]; }

void _main(int tc) {
  int n;
  cin >> n;
  vector<vector<int>> circles(n);
  rep(i, n) {
    int x, r;
    cin >> x >> r;
    circles[i] = {x, r, i};
  }
  sort(all(circles));
  vector<int> ans(n, -1);
  int m;
  cin >> m;
  int cnt = 0;
  rep(i, m) {
    int x, y;
    cin >> x >> y;
    int indexRight =
        upper_bound(all(circles), vector<int>{x, 0, 0}, comp) - circles.begin();
    if (indexRight < n) {
      int center = circles[indexRight][0];
      int r = circles[indexRight][1];
      if (ans[circles[indexRight][2]] == -1 && lte(sqrt(y * y + (center - x) * (center - x)), r)) {
        ans[circles[indexRight][2]] = i + 1;
        cnt++;
      }
    }
    int indexLeft = indexRight - 1;
    if (indexLeft >= 0) {
      int center = circles[indexLeft][0];
      int r = circles[indexLeft][1];
      if (ans[circles[indexLeft][2]] == -1 && lte(sqrt(y * y + (center - x) * (center - x)), r)) {
        ans[circles[indexLeft][2]] = i + 1;
        cnt++;
      }
    }
  }
  cout << cnt << endl;
  rep(i, n) { cout << ans[i] << " \n"[i + 1 == n]; }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0);
  return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}