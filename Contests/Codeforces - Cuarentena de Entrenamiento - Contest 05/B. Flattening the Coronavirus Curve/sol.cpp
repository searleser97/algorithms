// 26
#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define debug(e) cerr << "\033[48;5;196m\033[38;5;15m" << e << "\033[0m";
#else
#define debug(e)
#endif
// 4
template <class A, class B>
ostream &operator<<(ostream &os, pair<A, B> &p) {
  return os << "(" << p.fi << " " << p.se << ")";
}
// 8
template <class T>
ostream &operator<<(ostream &os, vector<T> &v) {
  rep(i, (int)v.size()) os << setw(7) << left << v[i];
  return os << endl;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &os, map<A, B> &m) {
  for (auto &kv : m) os << kv;
  return os;
}

const int inf = 1 << 30;
// 9
struct fw {
  // d = distance
  typedef int T;  // sum of costs might overflow
  vector<vector<T>> d;

  fw() {}

  fw(int n) {
    d.assign(n, vector<T>(n, inf));
    for (int i = 0; i < n; i++) d[i][i] = 0;
  }
  // 2
  // Assumes Directed Graph
  void addEdge(int u, int v, T c) { d[u][v] = c; }
  // 7
  // O(V^3)
  void floydwarshall() {
    for (int k = 0; k < (int)d.size(); k++)
      for (int u = 0; u < (int)d.size(); u++)
        for (int v = 0; v < (int)d.size(); v++)
          d[u][v] = min(d[u][v], d[u][k] + d[k][v]);
  }
};
void _main(int tc) {
  int n, m, q;
  cin >> n >> m >> q;
  vector<fw> v(m);
  rep(M, m) {
    v[M] = fw(n);
    rep(i, n) {
      rep(j, n) { cin >> v[M].d[i][j]; }
    }
  }

  vector<vector<fw>> mat(m, vector<fw>())

  rep(i, q) {
    int s, t, limit;
    cin >> s >> t >> limit;
    s--, t--;
    int ans = inf;
    rep(M, m) { ans = min(ans, v[M].d[s][t]); }
    cout << ans << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}