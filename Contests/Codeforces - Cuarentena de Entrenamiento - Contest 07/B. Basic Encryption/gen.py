def main():
    n, s = map(int, input().split())

    for _  in range(n):
        line = input()
        for c in line:
            if (c.isalpha()):
              if (c.isupper()):
                  index = ord(c) - ord('A')
                  moduled = ((index - s) % 26) + ord('A')
                  print(chr(moduled), end="")
              else:
                  index = ord(c) - ord('a')
                  moduled = ((index - s) % 26) + ord('a')
                  print(chr(moduled), end="")
            else:
                print(c, end="")
        print()

main()
