// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;
  // 6
  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
    // return a * b;
  }
  // 5
  // Inverse of F
  inline T I(T a, T b) {
    return a - b;
    // return a / b;
  }
  // 7
  // O(N)
  void build() {
    for (int i = 1; i < len(bit); i++) {
      int j = i + (i & -i);
      if (j < len(bit)) bit[j] = F(bit[j], bit[i]);
    }
  }
  // 4
  // O(lg(N))
  void update(int i, T val) {
    for (i++; i < len(bit); i += i & -i) bit[i] = F(bit[i], val);
  }
  // 6
  // O(lg(N))
  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }
  // 6
  // O(lg(N)), [l, r]
  T query(int l, int r) { return I(query(r), query(--l)); }
  T &operator[](int i) { return bit[++i]; }
};
const int MAX = 1e5 + 10;

int block;

struct Query {
  int L, R, X, Y, index, result;
  Query(int l, int r, int x, int y, int idx, int res)
      : L(l), R(r), X(x), Y(y), index(idx), result(res) {}
};
ostream &operator<<(ostream &os, const Query &q) {
  return os << "[" << q.L << " " << q.R << " " << q.X << " " << q.Y << " "
            << q.index << " " << q.result << "]";
}
bool compare(Query x, Query y) {
  if (x.L / block != y.L / block) return x.L / block < y.L / block;

  // Same block, sort by R value
  return x.R < y.R;
}

bool compare1(Query x, Query y) { return x.index < y.index; }

void queryResults(vector<int> &a, int n, vector<Query> &q, int m) {
  block = (int)sqrt(n);
  sort(all(q), compare);
  int currL = 0, currR = 0;
  vector<int> freq(MAX);
  BIT<int> ft(MAX);

  for (int i = 0; i < m; i++) {
    int L = q[i].L, R = q[i].R;
    while (currL < L) {
      freq[a[currL]]--;
      if (freq[a[currL]] == 0) {
        ft.update(a[currL], -1);
      }
      currL++;
    }
    while (currL > L) {
      freq[a[currL - 1]]++;
      if (freq[a[currL - 1]] == 1) {
        ft.update(a[currL - 1], 1);
      }
      currL--;
    }
    while (currR <= R) {
      freq[a[currR]]++;
      if (freq[a[currR]] == 1) {
        ft.update(a[currR], 1);
      }
      currR++;
    }
    while (currR > R + 1) {
      freq[a[currR - 1]]--;
      if (freq[a[currR - 1]] == 0) {
        ft.update(a[currR - 1], -1);
      }
      currR--;
    }
    q[i].result = ft.query(q[i].X, q[i].Y);
  }
}

void printResults(vector<Query> &q, int m) {
  sort(all(q), compare1);
  for (int i = 0; i < m; i++) {
    cout << q[i].result << endl;
  }
}

// 3
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  vector<int> a(n);
  rep(n) cin >> a[i];
  vector<Query> q;
  rep(m) {
    int l, r, x, y;
    cin >> l >> r >> x >> y;
    l--, r--;
    q.emplace_back(l, r, x, y, i, 0);
  }
  queryResults(a, n, q, m);
  printResults(q, m);
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}