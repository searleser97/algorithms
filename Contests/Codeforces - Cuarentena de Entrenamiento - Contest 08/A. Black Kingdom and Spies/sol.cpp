// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

// 7
// level[a] = level in graph of node a
// pathMinCap = capacity bottleneck for a path (s->t)
// icap = initial capacity, f[u] = flow of u
// adj[u][i] = edge from u to v = adj[u][i]
// cap[u][i] = capacity of edge u->adj[u][i]
// rev[u][i] = index of u in adj[v]
// rev is used to find the corresponding reverse edge
// 5
typedef li T;
const T inf = 1e18;
vector<vector<int>> adj, rev;
vector<vector<T>> cap;
vector<int> level;
// 4
void init(int N) {
  adj = rev = vector<vector<int>>(N);
  cap = vector<vector<T>>(N);
}
// 7
// Assumes Directed Graph
void addEdge(int u, int v, T icap) {
  rev[u].push_back(adj[v].size());
  rev[v].push_back(adj[u].size());
  adj[u].push_back(v), adj[v].push_back(u);
  cap[u].push_back(icap), cap[v].push_back(0);
}
// 17
bool levelGraph(int s, int t) {
  level.assign(adj.size(), 0);
  queue<int> q;
  level[s] = 1, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = 0; i < adj[u].size(); i++) {
      int v = adj[u][i];
      if (!level[v] && cap[u][i]) {
        q.push(v);
        level[v] = level[u] + 1;
      }
    }
  }
  return level[t];
}
// 14
T blockingFlow(int u, int t, T pathMinCap) {
  if (u == t) return pathMinCap;
  for (int i = 0; i < adj[u].size(); i++) {
    int v = adj[u][i];
    if (level[v] == (level[u] + 1) && cap[u][i])
      if (T pathMaxFlow = blockingFlow(v, t, min(pathMinCap, cap[u][i]))) {
        cap[u][i] -= pathMaxFlow;
        cap[v][rev[u][i]] += pathMaxFlow;
        return pathMaxFlow;
      }
  }
  return 0;
}
// 9
// O(V^2 * E)
T maxFlowMinCut(int s, int t) {
  if (s == t) return inf;
  T maxFlow = 0;
  while (levelGraph(s, t))
    while (T flow = blockingFlow(s, t, inf)) maxFlow += flow;
  return maxFlow;
}

int in(int u) { return 2 * u; }

int out(int u) { return 2 * u + 1; }

void _main(int tc) {
  int n, m;
  cin >> n >> m;
  init(2 * n);
  rep(n - 1) {
    int c;
    cin >> c;
    debug(i, in(i), out(i));
    addEdge(in(i), out(i), c);
  }
  addEdge(in(n - 1), out(n - 1), inf);
  rep(m) {
    int u, v, w;
    cin >> u >> v >> w;
    u--, v--;
    addEdge(out(u), in(v), w);
    addEdge(out(v), in(u), w);
  }
  cout << maxFlowMinCut(in(0), in(n - 1)) << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}