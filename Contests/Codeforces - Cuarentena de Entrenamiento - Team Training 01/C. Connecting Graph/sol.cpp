// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

struct UnionFind {
  int Time = 0;
  vector<int> dad, size, jTime;

  UnionFind(int N) : dad(N), size(N, 1), jTime(N) {
    while (N--) dad[N] = N;
  }
  // 5
  // O(lg(N))
  int root(int u, int t) {
    while (jTime[u] <= t && u != dad[u]) u = dad[u];
    return u;
  }
  // 10
  // O(1)
  void join(int u, int v, bool newTime = 1) {
    int Ru = root(u, Time), Rv = root(v, Time);
    if (newTime) Time++;
    if (Ru == Rv) return;
    if (size[Ru] > size[Rv]) swap(Ru, Rv);
    jTime[Ru] = Time;
    dad[Ru] = Rv;
    size[Rv] += size[Ru];
  }
  // 4
  // O(lg(N))
  bool areConnected(int u, int v, int t) { return root(u, t) == root(v, t); }
  // 4
  // O(lg(N))
  int getLastVersionSize(int u) { return size[root(u, Time)]; }
  // 13
  // O(lg(Time) * lg(N))
  int joinTime(int u, int v) {
    int l = 0, r = Time, ans = -1;
    while (l <= r) {
      int mid = (l + r) >> 1;
      if (areConnected(u, v, mid))
        ans = mid, r = mid - 1;
      else
        l = mid + 1;
    }
    return ans;
  }
};

void _main(int tc) {
  int n, m;
  cin >> n >> m;
  UnionFind uf(n);
  rep(m) {
    int opc, u, v;
    cin >> opc >> u >> v;
    u--, v--;
    if (opc == 1) {
      uf.join(u, v);
    } else {
      uf.join(0, 0);
      cout << uf.joinTime(u, v) << endl;
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}