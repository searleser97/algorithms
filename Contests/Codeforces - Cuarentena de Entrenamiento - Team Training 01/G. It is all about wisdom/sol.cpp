// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

typedef li T;  // sum of costs might overflow
typedef pair<T, int> DistNode;
const T inf = 1e18;
vector<vector<int>> adj;
vector<vector<T>> cost, wisdom;
// 4
void init(int N) {
  adj = vector<vector<int>>(N);
  cost = vector<vector<T>>(N);
  wisdom = vector<vector<T>>(N);
}
// 4
// Assumes Directed Graph
void addEdge(int u, int v, T c, T w) {
  adj[u].push_back(v);
  cost[u].push_back(c);
  wisdom[u].push_back(w);
}
// 17
// ~ O(E * lg(V))
vector<T> dijkstra(int s, int wis) {
  vector<T> dist(adj.size(), inf);
  priority_queue<DistNode> q;
  q.push({0, s}), dist[s] = 0;
  while (q.size()) {
    DistNode top = q.top();
    q.pop();
    int u = top.second;
    if (dist[u] < -top.first) continue;
    for (int i = 0; i < adj[u].size(); i++) {
      if (wis < wisdom[u][i]) continue;
      int v = adj[u][i];
      T d = dist[u] + cost[u][i];
      if (d < dist[v]) q.push({-(dist[v] = d), v});
    }
  }
  return dist;
}

void _main(int tc) {
  int n, m;
  T k;
  cin >> n >> m >> k;
  init(n);
  rep(m) {
    int u, v;
    T c, w;
    cin >> u >> v >> c >> w;
    u--, v--;
    addEdge(u, v, c, w);
    addEdge(v, u, c, w);
  }
  li l = 1, r = 1e9 + 100, mid;
  bool ispossible = false;
  while (l <= r) {
    mid = l + (r - l) / 2;
    auto dist = dijkstra(0, mid);
    if (dist[n - 1] < k) ispossible = true;
    if (dist[n - 1] > k - 1)
      l = mid + 1;
    else
      r = mid - 1;
  }
  if (ispossible) {
    cout << l << endl;
  } else
    cout << -1 << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}