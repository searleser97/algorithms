// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}

template <class T>
struct SegmentTree {
  T neutro;
  int N;
  vector<T> st;
  function<T(T, T)> F;
  // 3
  SegmentTree() {}
  SegmentTree(int n, T neut,
      T f(T, T) = [](T a, T b) { return a + b; })
      : neutro(neut), st(2 * n, neutro), N(n), F(f) {}
  // 5
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--)
      st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 5
  // O(lg(2N)), works like replacing arr[i] with val
  void update(int i, T val) {
    for (st[i += N] = val; i > 1; i >>= 1)
      st[i >> 1] = F(st[i], st[i ^ 1]);
  }
  // 9
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    T ans = neutro;
    for (l += N, r += N; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};

// 7
// p = parent
vector<int> firstPos;
vector<pair<int, int>> tour;
vector<vector<int>> adj;
SegmentTree<pairii> st;

void init(int N) { adj.assign(N, vector<int>()); }
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}
// 10
// O(N)
void eulerTour(int u, int p, int h) {
  firstPos[u] = tour.size();
  tour.push_back({h, u});
  for (int v : adj[u])
    if (v != p) {
      eulerTour(v, u, h + 1);
      tour.push_back({h, u});
    }
}
// 11
// O(N * lg(N))
void preprocess(int r) {
  tour.clear();
  firstPos.assign(adj.size(), -1);
  vector<int> roots = {r};
  for (auto &root : roots) eulerTour(root, -1, 0);
  st = SegmentTree<pairii>(len(tour), {1e9, 1e9}, [](pairii a, pairii b) { return a.first < b.first ? a : b; });
  rep(len(tour)) {
    st[i] = tour[i];
  }
  st.build();
  // st = SegmentTree<pairii>(
  //     tour, [](pairii a, pairii b) { return a.first < b.first ? a : b; });
}
// 6
// O(1)
int lca(int u, int v) {
  int l = min(firstPos[u], firstPos[v]);
  int r = max(firstPos[u], firstPos[v]);
  return st.query(l, r).second;
}

void _main(int tc) {
  int n, m;
  cin >> n >> m;
  init(n);
  vector<vector<int>> p(m);
  int root = -1;
  rep(n) {
    int x;
    cin >> x;
    p[x - 1].pb(i);
  }

  rep(n) {
    int u;
    cin >> u;
    if (u == 0) {
      root = i;
      continue;
    }
    u--;
    addEdge(u, i);
  }
  preprocess(root);
  vector<int> preid(n), depth(n);
  int idx = -1;
  function<void(int, int, int)> preorder = [&](int u, int par, int dist) -> void {
    depth[u] = dist;
    preid[u] = ++idx;
    for (auto & v : adj[u]) {
      if (v == par) continue;
      preorder(v, u, dist + 1);
    }
  };
  preorder(0, 0, 1);

  rep(z, len(p)) {
    auto &v = p[z];
    sort(all(v), [&](int a, int b) { return preid[a] < preid[b]; });
    int ans = depth[v[0]];
    rep(len(v) - 1) {
      auto lc = lca(v[i], v[i + 1]);
      ans += depth[v[i + 1]] - depth[lc];
    }
    cout << ans << " \n"[z + 1 == len(p)];
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}