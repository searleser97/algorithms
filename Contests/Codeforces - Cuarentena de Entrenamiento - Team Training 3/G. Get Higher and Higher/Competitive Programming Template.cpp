// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
}
#else
#define debug(...)
#endif

int inf = (1 << 30) - 1;
vector<vector<int>> adj;

void init(int N) { adj.assign(N, vector<int>()); }
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}
// 16
int bfs(int u) {
  vector<int> dist(adj.size(), inf);
  queue<int> q;
  q.push(u), dist[u] = 0;
  while (q.size()) {
    int u = q.front();
    q.pop();
    for (int& v : adj[u])
      if (dist[v] == inf)
        q.push(v), dist[v] = dist[u] + 1;
  }
  int node, maxx = -inf;
  for (int u = 0; u < adj.size(); u++)
    if (dist[u] > maxx) maxx = dist[u], node = u;
  return node;
}
// 16
vector<int> diameter() {
  int u = bfs(0), v = bfs(u);
  vector<int> path = {v}, vis(adj.size());
  function<bool(int)> dfs = [&](int a) {
    if (a == v) return true;
    vis[a] = 1;
    for (int& b : adj[a]) {
      if (vis[b] || !dfs(b)) continue;
      path.push_back(a);
      return true;
    }
    return false;
  };
  dfs(u);
  return path;
}

// 3
void _main(int tc) {
  int n;
  cin >> n;
  init(n);
  rep(n - 1) {
    int u, v;
    cin >> u >> v;
    addEdge(u - 1, v - 1);
  }
  auto d1 = len(diameter());
  int m;
  cin >> m;
  init(m);
  rep(m - 1) {
    int u, v;
    cin >> u >> v;
    addEdge(u - 1, v - 1);
  }
  auto d2 = len(diameter());
  if (d1 > (d2 + 1) / 2) {
    cout << "GGEZ" << endl;
  } else {
    cout << "FF" << endl;
  }
  
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}