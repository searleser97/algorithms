s = input()
aux2 = int(s)
s = list(s)
ans = -1
for i in range(len(s)):
  for j in range(i + 1, len(s)):
    aux = s[i]
    s[i] = s[j]
    s[j] = aux
    aux3 = int(''.join(s))
    if (s[0] != '0' and  aux3 < aux2):
      ans = max(ans, aux3)
    aux = s[i]
    s[i] = s[j]
    s[j] = aux

if (ans == aux2):
  print(-1)
else:
  print(ans)
