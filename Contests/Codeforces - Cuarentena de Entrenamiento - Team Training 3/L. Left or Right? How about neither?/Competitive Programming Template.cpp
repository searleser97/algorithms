// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
}
#else
#define debug(...)
#endif
// 3

typedef ld T;  // sum of costs might overflow
typedef pair<T, int> DistNode;
const T inf = 1e18;
vector<vector<int>> adj;
vector<vector<T>> cost;
// 4
void init(int N) {
  adj = vector<vector<int>>(N);
  cost = vector<vector<T>>(N);
}
// 4
// Assumes Directed Graph
void addEdge(int u, int v, T c) { adj[u].push_back(v), cost[u].push_back(c); }
// 17
// ~ O(E * lg(V))
vector<T> dijkstra(int s) {
  vector<T> dist(adj.size(), inf);
  priority_queue<DistNode> q;
  q.push({0, s}), dist[s] = 0;
  while (q.size()) {
    DistNode top = q.top();
    q.pop();
    int u = top.second;
    if (dist[u] < -top.first) continue;
    for (int i = 0; i < adj[u].size(); i++) {
      int v = adj[u][i];
      T d = dist[u] + cost[u][i];
      if (d < dist[v]) q.push({-(dist[v] = d), v});
    }
  }
  return dist;
}

template <class T>  // don't pass n as param
vector<int> compress(vector<T> &v, int n = 0) {
  map<T, int> Map;

  set<T> s(v.begin(), v.end());
  vector<int> c(v.size());
  for (auto &e : s) Map[e] = n++;
  for (int i = 0; i < v.size(); i++) c[i] = Map[v[i]];
  return c;
}

void _main(int tc) {
  int n, l, r, c;
  cin >> n >> l >> r >> c;
  int s, t;
  cin >> s >> t;
  s--, t--;

  vector<int> v(n);

  rep(n) { cin >> v[i]; }
  set<int> st(all(v));

  init(n + len(st));

  int id = n;
  map<int, int> mp;
  for (auto &e : st) {
    mp[e] = id++;
  }

  rep(n - 1) { addEdge(i, i + 1, r); }
  rep(i, 1, n) { addEdge(i, i - 1, l); }

  rep(n) {
    addEdge(i, mp[v[i]], c / 2.0);
    addEdge(mp[v[i]], i, c / 2.0);
  }

  cout << (li)dijkstra(s)[t] << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}