// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
}
#else
#define debug(...)
#endif
// 3
template <class T>
struct SegmentTree {
  T neutro = 0;
  int N;
  vector<T> st;
  function<T(T, T)> F;
  SegmentTree() {}
  // 3
  SegmentTree(int n, int neut,
      T f(T, T) = [](T a, T b) { return a + b; })
      : neutro(neut), N(n), st(2 * n, neut), F(f) {}
  // 5
  // O(lg(2N)), works like replacing arr[i] with val
  void update(int i, T val) {
    for (st[i += N] = val; i > 1; i >>= 1)
      st[i >> 1] = F(st[i], st[i ^ 1]);
  }
  // 9
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    T ans = neutro;
    for (l += N, r += N; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};

template <class T>
struct HLD {
  SegmentTree<T> st;
  function<T(T, T)> F;
  vector<int> dad, heavy, depth, root, stPos;
  // 23
  HLD(vector<vector<int>>& adj, T initVal,
      T f(T, T) = [](T a, T b) { return a + b; })
      : F(f) {
    dad = root = stPos = depth = heavy = vector<int>(adj.size(), -1);
    st = SegmentTree<T>(adj.size(), initVal);
    function<int(int)> dfs = [&](int u) {
      int size = 1, maxSubtree = 0;
      for (int& v : adj[u]) {
        if (v == dad[u]) continue;
        dad[v] = u, depth[v] = depth[u] + 1;
        int subtree = dfs(v);
        if (subtree > maxSubtree)
          heavy[u] = v, maxSubtree = subtree;
        size += subtree;
      }
      return size;
    };
    dad[0] = -1, depth[0] = 0, dfs(0);
    for (int i = 0, pos = 0; i < adj.size(); i++)
      if (dad[i] < 0 || heavy[dad[i]] != i)
        for (int j = i; ~j; j = heavy[j])
          stPos[j] = pos++, root[j] = i;
  }

  // 13
  // O(lg^2 (N))
  void processPath(int u, int v,
                   function<void(T, T)> op) {
    for (; root[u] != root[v]; v = dad[root[v]]) {
      if (depth[root[u]] > depth[root[v]]) swap(u, v);
      op(stPos[root[v]], stPos[v]);
    }
    if (depth[u] > depth[v]) swap(u, v);
    // for values on edges
    if (u != v) op(stPos[u] + 1, stPos[v]);
    // for values on nodes
    // op(stPos[u], stPos[v]);
  }
  // 8
  // O(lg^2 (N))
  void update(int u, int v, int val) {
    if (depth[u] > depth[v]) swap(u, v);
    if (u != v) st.update(stPos[v], val);
    // processPath(u, v, [&](int l, int r) {
    //   st.update(l, r, val);
    // });
  }
  // 9
  // O(lg^2 (N))
  T query(int u, int v) {
    T ans = 0;
    processPath(u, v, [&](int l, int r) {
      ans = F(ans, st.query(l, r));
    });
    return ans;
  }
};

void _main(int tc) {
  int n;
  cin >> n;
  vector<vector<int>> adj(n);

  vector<vector<int>> edges(n - 1, vector<int>(3));
  rep(n - 1) {
    int u, v, l;
    cin >> u >> v >> l;
    u--, v--;
    adj[u].pb(v);
    adj[v].pb(u);
    l %= 2;
    edges[i] = {u, v, l};
  }
  HLD<int> hld(adj, 0);
  rep(n - 1) {
    hld.update(edges[i][0], edges[i][1], edges[i][2]);
  }
  int q;
  cin >> q;
  rep(q) {
    int type; 
    cin >> type;
    if (type == 1) {
      int u, v;
      cin >> u >> v;
      u--, v--;
      if (hld.query(u, v) & 1 ^ 1) {
        cout << "JAKANDA FOREVER" << endl;
      } else {
        cout << "WE NEED BLACK PANDA" << endl;
      }
    } else {
      int j, l;
      cin >> j >> l;
      j--;
      l %= 2;
      hld.update(edges[j][0], edges[j][1], l);
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}