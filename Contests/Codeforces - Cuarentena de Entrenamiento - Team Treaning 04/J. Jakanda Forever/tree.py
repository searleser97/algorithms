from random import randint
from sys import stdout
import subprocess

n = randint(2, 10)
cout = f"{n}\n"

for i in range(2, n + 1):
    u = i
    v = randint(1, i - 1)
    l = randint(1, 30)
    cout += f"{u} {v} {l}\n"

q = randint(5, 10)
cout += f"{q}\n"
for i in range(q):
    opt = randint(1, 2)
    if opt == 2:
        edge = randint(1, n)
        length = randint(1, 30)
        cout += f"{opt} {edge} {length}\n"
    else:
        u = randint(1, n)
        v = randint(1, n)
        cout += f"{opt} {u} {v}\n"
print(cout)