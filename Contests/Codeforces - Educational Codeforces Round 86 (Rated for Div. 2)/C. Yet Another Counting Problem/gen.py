a = 4
b = 6
l = 0
r = 15

ans = 0
for x in range(l, r + 1):
    print(str(x).rjust(4, ' '), str(x % a).rjust(4, ' '), str((x % b) % a).rjust(4, ' '), str((x % b)).rjust(4, ' '))
    if (x % a != (x % b) % a):
        ans += 1

print(ans)