// 26
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" s << "\033[0m"
#else
#define cerr(s)
#endif
// 12
const int inf = 1e9;

void _main(int tc) {
  string s, t;
  cin >> s >> t;
  vector<int> rm(t.size() + 1);
  rm[t.size()] = -inf;
  int j = s.size() - 1;

  rrep(i, t.size()) {
    while (t[i] != s[j]) j--;
    rm[i] = j--;
  }

  int ans = rm[0], l = 0;
  rep(i, s.size()) {
    if (t[l] == s[i]) {
      ans = max(ans, rm[l + 1] - 1 - i);
      l++;
    }
    if (l == t.size()) {
      ans = max(ans, (int)s.size() - 1 - i);
      break;
    }
  }
  cout << ans << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}