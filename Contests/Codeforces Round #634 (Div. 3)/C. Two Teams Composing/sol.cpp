// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
const int inf = 1e9;
void _main(int tc) {
  int n;
  cin >> n;
  map<int, int> m;
  rep(i, n) {
    int x;
    cin >> x;
    m[x]++;
  }
  int maxim = -inf;
  int nummaxim = 0;
  for (auto & e : m) {
    if (e.se > maxim) {
      maxim = e.se, nummaxim = e.fi;
    }
  }
  int distinct = m.size() - 1;
  cerr(distinct << " " << maxim) << endl;
  int aux = min(distinct, maxim);
  int r = maxim - aux;
  if (r >= 2) {
    cout << aux + 1 << endl;
  } else {
    cout << aux << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}