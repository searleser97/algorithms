// 22
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
// 12
void _main(int tc) {
  int n = 9;
  vector<vector<char>> mat(9, vector<char>(9));
  rep(i, n) {
    rep(j, n) {
      cin >> mat[i][j];
      mat[i][j] -= '0';
    }
  }
  mat[0][0] = (mat[0][0] + 1) % 10;
  mat[1][3] = (mat[1][3] + 1) % 10;
  mat[2][6] = (mat[2][6] + 1) % 10;

  mat[3][1] = (mat[3][1] + 1) % 10;
  mat[4][4] = (mat[4][4] + 1) % 10;
  mat[5][7] = (mat[5][7] + 1) % 10;

  mat[6][2] = (mat[6][2] + 1) % 10;
  mat[7][5] = (mat[7][5] + 1) % 10;
  mat[8][8] = (mat[8][8] + 1) % 10;

  rep(i, n) {
    rep(j, n) {
      if (mat[i][j] == 0) mat[i][j] = 1;
      cout << (char)(mat[i][j] + '0');
    }
    cout << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  // _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}