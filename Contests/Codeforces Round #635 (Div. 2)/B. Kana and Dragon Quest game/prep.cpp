#include <bits/stdc++.h>
using namespace std;
void _main(int tc) {
  int x, n, m;
  cin >> x >> n >> m;
  while (true) {
    if (x / 2 + 10 < x && n > 0) {
      x = x / 2 + 10;
      n--;
    } else {
      break;
    }
  }
  if (x - m * 10 <= 0) {
    cout << "YES" << '\n';
  } else {
    cout << "NO" << '\n';
  }
}
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  int tc;
  cin >> tc;
  for (int i = 0; i < tc; i += 1) _main(i + 1);
}
