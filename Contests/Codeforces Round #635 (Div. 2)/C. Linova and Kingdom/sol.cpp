// 26
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define fors(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fors(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define forrs(_, x, n, s) for (int _ = x; _ > n; _ -= s)
#define forrn(_, x, n) forrs(_, x, n, 1)
#define rrep(_, n) forrn(_, n - 1, -1)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef LOCAL
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
#else
#define cerr(s)
#endif
// 12
void _main(int tc) {
  int n, k;
  cin >> n >> k;
  vector<vector<int>> adj(n);
  rep(i, n - 1) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    adj[u].pb(v);
    adj[v].pb(u);
  }
  vector<int> dist(n), leaves;
  dist[0] = -1;
  function<void(int, int)> dfs = [&](int u, int p) {
    dist[u] = dist[p] + 1;
    if (u != p && adj[u].size() == 1) leaves.pb(u);
    for (auto& v : adj[u]) {
      if (v == p) continue;
      dfs(v, u);
    }
  };
  dfs(0, 0);
  li happiness = 0;

  sort(all(leaves), [&](int a, int b) { return dist[a] < dist[b]; });

  forrn(i, leaves.size() - 1, max(-1, (int)leaves.size() - 1 - k)) {
    happiness += dist[leaves[i]];
  }

  int curK = k - leaves.size();

  if (curK > 0) {
    int i = leaves.size() - 1;
    while (curK > 0) {
      if (curK <= dist[leaves[i]] - 1) {
        happiness = happiness - dist[leaves[i]];
        happiness = happiness + (curK + 1) * (dist[leaves[i]] - curK);
        break;
      } else {
        curK -= dist[leaves[i]] - 1;
        i--;
      }
    }
    cout << happiness << endl;
  } else {
    cout << happiness << endl;
  }
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}