# time-limit: 3from math import *
import sys
cin = lambda: sys.stdin.readline().strip()
cout = sys.stdout.write

def main(tc):
    n = int(input())
    cnt = 0
    while (n > 0):
        cnt += n // 10 * 10
        if (n // 10 == 0):
            cnt += n % 10
            break
        n = n // 10 + n % 10
    print(cnt)

# main(0); exit(0)
for i in range(int(input())): main(i + 1)
