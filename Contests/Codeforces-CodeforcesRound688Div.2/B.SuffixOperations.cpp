// time-limit: 1000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(n) {
    cin >> v[i];
  }

  // initially there is a difference between a_i and a_{i+1} 
  // (which could be 0 also) therefore, if we want to make all the numbers
  // equal, then we have no other option but to make those differences equal 0.
  // hence the minimum amount of operations without changing any number initially is
  // `abs(a_1 - a_2) + abs(a_2 - a_3) + abs(a_3 - a_4) + ... + abs(a_{n - 1}, a_n)`
  // this is because every operation will not change the difference between any `a_i` and `a_{i + 1}`
  
  li initMinim = 0;
  rep(n - 1) {
    initMinim += abs(v[i] - v[i + 1]);
  }

  // the problem also gives us the option to change one number initially.
  // we can test each a_i and see how much it minimizes when we make it equal to a_{i-1} and
  // when we make it equal to a_{i+1}
  // https://searleser97.gitlab.io/competitive-programming-notes/Codeforces/1453B.svg

  li best = initMinim;
  rep(n) {
    int aux = 0, aux2 = 0;
    if (i + 1 < n) {
      aux = abs(v[i] - v[i + 1]);
    }
    if (i - 1 >= 0) {
      aux += abs(v[i] - v[i - 1]);
    }
    if (i + 1 < n && i - 1 >= 0) {
      aux2 = abs(v[i - 1] - v[i + 1]);
    }
    best = min(best, initMinim - aux + aux2);
  }
  cout << best << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
