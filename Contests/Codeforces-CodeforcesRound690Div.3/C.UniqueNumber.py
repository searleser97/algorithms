# time-limit: 3000
from math import *
import sys
cin = lambda: sys.stdin.readline().strip()
cout = sys.stdout.write

limit = 9
arr = [-1] * 51
i = 1
pref = ""
while(i < 46):
    for j in range(1, limit + 1):
        arr[i] = (pref + str(j))[::-1]
        i += 1
    pref += str(limit)
    limit -= 1


def main(tc):
    n = int(input())
    print(arr[n])

# main(0); exit(0)
for i in range(int(input())): main(i + 1)
