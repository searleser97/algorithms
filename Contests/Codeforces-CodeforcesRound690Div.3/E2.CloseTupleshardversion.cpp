
// time-limit: 2000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
const int MOD = 1e9 + 7;
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 5
// 9

int sign(li x) { return x < 0 ? -1 : x > 0; }

//          a / b
li floor(li a, li b) {
  if (sign(a) != sign(b) && a % b) return a / b - 1;
  else return a / b;
}

// if m is positive the answer is positive
// if m is negative the answer is negative
li mod(li a, li m) { return (m + (a % m)) % m; }
// 11
//#include "../../Util/Modulo with negative numbers.cpp"
//#include "../../Util/Floor Of Division Between Integers.cpp"

// gcd(a, b) = ax + by
array<li, 3> extendedGCD(li a, li b) {
  if (!a) return {b, 0, 1};
  auto [d, x, y] = extendedGCD(mod(b, a), a);
  return {d, y - floor(b, a) * x, x};
}  // {gcd(a, b), x, y}
// 10
// n = number, m = modulo
//#include "Extended Euclidean Algorithm.cpp"

// O(lg(N))
li modInv(li n, li m) {
  auto [g, x, y] = extendedGCD(n, m);
  if (g != 1) throw "No solution";
  return (x % m + m) % m;
}


// 7
vector<li> fact;

void computeFact(int n, li m) {
  int i = max(1, int(fact.size()));
  fact.resize(++n), fact[0] = 1;
  for (; i < n; i++)
    fact[i] = fact[i - 1] * i % m;
}

// 11
//#include "../Number Theory/Modular Multiplicative Inverse.cpp"
//#include "Factorials Modulo M.cpp"

// O(lg(p))
li nCk(int n, int k, li p) {
  if (fact.size() <= n) computeFact(n, p);
  if (n < k) return 0;
  return fact[n] * modInv(fact[k], p) % p *
         modInv(fact[n - k], p) % p;
}

// 10
// ans[0] = true if e is in v else false
// ans[1] = index pointing to the first element in
// the range [l, r) which compares > to e.
template <class T>
vector<int> upperBound(vector<T>& v, int l, int r,
                       T e) {
  auto it = v.begin();
  int i = upper_bound(it + l, it + r, e) - it;
  return {v[i - 1] == e, i};
}

// 3
void _main(int tc) {
  int n, m, k;
  cin >> n >> m >> k;
  vector<int> v(n);

  rep(n) {
    cin >> v[i];
  }
  debug(v);
  sort(all(v));
  li ans = 0;
  int prevLength = 0;
  rep(n) {
    int index = upperBound(v, i, len(v), v[i] + k)[1];
    int length = index - i;
    debug(i, v[i], index, length, prevLength - 1);
    ans += nCk(length, m, MOD) - nCk(prevLength - 1, m, MOD);
    ans = mod(ans, MOD);
    prevLength = length;
  }
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}

