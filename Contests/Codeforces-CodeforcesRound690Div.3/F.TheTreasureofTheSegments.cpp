// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 10
// ans[0] = true if e is in v else false
// ans[1] = index pointing to the first element in
// the range [l, r) which compares >= to e.
template <class T>
array<int, 2> lowerBound(vector<T>& v, int l, int r,
                       T e) {
  auto it = v.begin();
  int i = lower_bound(it + l, it + r, e) - it;
  return {v[i] == e, i};
}
// 10
// ans[0] = true if e is in v else false
// ans[1] = index pointing to the first element in
// the range [l, r) which compares > to e.
template <class T>
array<int, 2> upperBound(vector<T>& v, int l, int r,
                       T e) {
  auto it = v.begin();
  int i = upper_bound(it + l, it + r, e) - it;
  return {v[i - 1] == e, i};
}
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<pii> segs(n);
  vector<int> lefts(n), rights(n);
  rep(n) {
    cin >> lefts[i] >> rights[i];
    segs[i] = {lefts[i], rights[i]};
  }

  sort(all(lefts));
  sort(all(rights));
  int ans = 1 << 30;
  for (auto &[l, r] : segs) {
    // 
    int beforeLeft = lowerBound(rights, 0, n, l)[1];
    int afterRight = n - upperBound(lefts, 0, n, r)[1];
    ans = min(ans, beforeLeft + afterRight);
  }
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
