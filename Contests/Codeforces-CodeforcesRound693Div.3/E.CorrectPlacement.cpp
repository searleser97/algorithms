// time-limit: 4000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
// 9
// pref = prefix sum
// lte = less than or equal, 1-indexed
// A = hi = max_element(from, to)
// lcount = left children count
// lo = min_element(from, to)
struct WaveletTree {
  WaveletTree *l, *r;
  int lo, hi;
  vector<int> lcount, pref;
  // 17
  // O(N*lg(A))
  WaveletTree(vector<int>::iterator from,
              vector<int>::iterator to, int lo,
              int hi) {
    this->lo = lo, this->hi = hi;
    if (lo == hi or from >= to) return;
    int mid = (lo + hi) >> 1;
    auto f = [mid](int x) { return x <= mid; };
    lcount.reserve(to - from + 1);
    pref.reserve(to - from + 1);
    lcount.push_back(0);
    pref.push_back(0);
    for (auto it = from; it != to; it++)
      lcount.push_back(lcount.back() + f(*it)),
          pref.push_back(pref.back() + *it);
    auto pivot = stable_partition(from, to, f);
    l = new WaveletTree(from, pivot, lo, mid);
    r = new WaveletTree(pivot, to, mid + 1, hi);
  }
  // 9
  // O(lg(A)) frequency of k in [a, b]
  int freq(int a, int b, int k) {
    if (a > b or k < lo or k > hi) return 0;
    if (lo == hi) return b - a + 1;
    int lc = lcount[a - 1], rc = lcount[b];
    if (k > ((lo + hi) >> 1))
      return r->freq(a - lc, b - rc, k);
    return l->freq(lc + 1, rc, k);
  }
  // 10
  // O(lg(A)) kth-Smallest element in [a, b]
  int kth(int a, int b, int k) {
    if (a > b) return 0;
    if (lo == hi) return lo;
    int lc = lcount[a - 1], rc = lcount[b],
        inleft = rc - lc;
    if (k > inleft)
      return r->kth(a - lc, b - rc, k - inleft);
    return l->kth(lc + 1, rc, k);
  }
  // 8
  // O(lg(A)) count of elements <= to k in [a, b]
  int lte(int a, int b, int k) {
    if (a > b or k < lo) return 0;
    if (hi <= k) return b - a + 1;
    int lc = lcount[a - 1], rc = lcount[b];
    return l->lte(lc + 1, rc, k) +
           r->lte(a - lc, b - rc, k);
  }
  // 8
  // O(lg(A)) sum of numbers <= to k in [a, b]
  int sumlte(int a, int b, int k) {
    if (a > b or k < lo) return 0;
    if (hi <= k) return pref[b] - pref[a - 1];
    int lc = lcount[a - 1], rc = lcount[b];
    return l->sumlte(lc + 1, rc, k) +
           r->sumlte(a - lc, b - rc, k);
  }
};
// 10
// ans[0] = true if e is in v else false
// ans[1] = index pointing to the first element in
// the range [l, r) which compares > to e.
template <class T>
array<int, 2> upperBound(vector<T>& v, int l, int r,
                         T e) {
  auto it = v.begin();
  int i = upper_bound(it + l, it + r, e) - it;
  return {v[i - 1] == e, i};
}
void _main(int tc) {
  int n;
  cin >> n;
  vector<pair<int, pii>> hw(n);
  vector<pii> hwOrig(n);
  rep(n) {
    cin >> hw[i].first >> hw[i].se.fi;
    if (hw[i].fi < hw[i].se.fi) {
      swap(hw[i].fi, hw[i].se.fi);
    }
    hw[i].se.se = i;
    hwOrig[i].fi = hw[i].fi;
    hwOrig[i].se = hw[i].se.fi;
  }
  sort(all(hw));
  vector<int> w(n);
  rep(n) {
    w[i] = hw[i].se.fi;
  }

  WaveletTree wt(all(v), *min_element(all(w)), *max_element(all(w)));
  vector<int> sans(n);
  rep(n) {
    int currH = hwOrig[i].fi, currW = hwOrig[i].se;
    auto ans = upperBound(hw, 0, len(hw), currH);
    if (!ans[0]) {
      sans[i] = -1;
      continue;
    }
    
  }


}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
