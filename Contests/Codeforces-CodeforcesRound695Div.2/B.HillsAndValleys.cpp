// time-limit: 1000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
/*
https://searleser97.gitlab.io/competitive-programming-notes/Codeforces/1467B.svg
*/
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> v(n);
  rep(n) {
    cin >> v[i];
  }
  auto isHill = [&](int i) {
    if (1 <= i && i <= n - 2)
      return v[i - 1] < v[i] && v[i] > v[i + 1];
    else
      return false;
  };
  auto isValley = [&](int i) {
    if (1 <= i && i <= n - 2)
      return v[i - 1] > v[i] && v[i] < v[i + 1];
    else
      return false;
  };

  auto is = [&](int i) {
    return isHill(i) or isValley(i);
  };
  int valley = 0, hill = 0;
  int sub = 0;
  auto comp = [](int a, int b) {
    if (a > b) return 1;
    else if (a < b) return -1;
    else return 0;
  };
  rep(i, 1, n - 1) {
    int possible = 0;
    if (is(i)) {
      possible = 1;
    }
    if (is(i) && is(i + 1)) {
      possible = 2;
      int l = v[i - 1], r = v[i + 2];
      int mn = min(v[i], v[i + 1]), mx = max(v[i], v[i + 1]);
      if ((mn < l && l < mx) && (mn < r && r < mx)) {
        if (i - 2 >= 0 && i + 3 < n) {
          if (comp(v[i - 2], v[i - 1]) == comp(v[i - 1], v[i]) && comp(v[i + 1], v[i + 2]) == comp(v[i + 2], v[i + 3])) {
            possible = 1;
          }
        }
      }
    }
    if (is(i - 1) && is(i) && is(i + 1)) {
      possible = 3;
    }
    sub = max(sub, possible);
    if (isValley(i)) {
      valley++;
    } else if (isHill(i)) {
      hill++;
    }
  }
  debug(valley, hill, hill + valley);

  cout << valley + hill - sub << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
