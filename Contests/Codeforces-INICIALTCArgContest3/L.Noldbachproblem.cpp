// time-limit: 2000
// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
    M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
    for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
    return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
    rep(i, len(v)) o << setw(7) << right << v[i];
    return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
    for (auto &kv : m) o << kv;
    return o;
}
// 7
void debug(const auto &e, const auto &... r) {
    cout << coutc << e;
    ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
// 18
// sieve[i] = lowest prime factor of i
// sieve[even number] = 0
// sieve[prime] = 0

vector<int> sieve, primes;

// ~O(N * lg(lg(N)))
void primeSieve(int n) {
    sieve.assign(n + 1, 0);
    sieve[2] = 0;
    rep(i, 4, n + 1, 2) {
        sieve[i] = 2;
    }

    for (int i = 3; i * i <= n; i += 2)
        if (!sieve[i])
            for (int j = i * i; j <= n; j += 2 * i)
                if (!sieve[j]) sieve[j] = i;
    primes.push_back(2);
    for (int i = 3; i <= n; i += 2)
        if (!sieve[i]) primes.push_back(i);
}

void _main(int tc) {
    int n, k;
    cin >> n >> k;
    primeSieve(1e5);
    int cnt = 0;
    rep(len(primes) - 1) {
        if (primes[i] + primes[i + 1] + 1 > n) break;
        debug(sieve[primes[i] + primes[i + 1] + 1], primes[i] + primes[i + 1] + 1, primes[i], primes[i + 1]);
        if (sieve[primes[i] + primes[i + 1] + 1] == 0) {
            cnt++;
        }
    }
    debug(cnt, k);
    if (cnt >= k) {
        cout << "YES" << endl;
    } else {
        cout << "NO" << endl;
    }

}
// 7
int main() {
    ios_base::sync_with_stdio(0), cin.tie(0);
    _main(0), exit(0);
    int tc;
    cin >> tc;
    rep(i, tc) _main(i + 1);
}
