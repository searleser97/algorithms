# time-limit: 3
from math import *
import sys
cin = lambda: sys.stdin.readline().strip()
cout = sys.stdout.write

def comp1(a, b):
    for i in range(min(len(a), len(b))):
        if (a[i] > b[i]) return True
    return False

def comp2(a, b):
    return len(a) < len(a)

def main(tc):
    n = int(input())
    l = []
    for i in range(n):
        s = input()
        auxl = list(s)
        auxl.sort()
        s = ''.join(auxl)
        s = s[::-1]
        l.append(s)
    
    l.sort(key = lambda x: ()
    l = l[::-1]
    ans = ''.join(l)
    print(ans)

main(0); exit(0)
for i in range(int(input())): main(i + 1)
