// time-limit: 3000
// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
using pii = pair<int, int>;
using li = long long int;
using ld = long double;  // using lli = __int128_t;
#ifdef DEBUG
string to_string(char c) {
  return string({c});
}
// 7
template <class... Ts>
ostream& operator<<(ostream& o, tuple<Ts...> t) {
  string s = "(";
  apply(
      [&](auto&&... r) {
        ((s += to_string(r) + ", "), ...);
      },
      t);
  return o << s.substr(0, len(s) - 2) + ")";
}
// 4
ostream& operator<<(ostream& o, pair<auto, auto> p) {
  return o << "(" + to_string(p.fi) + ", " + to_string(p.se) + ")";
}
// 7
template <class C, class T = typename C::value_type, typename std::enable_if<!std::is_same<C, std::string>::value>::type* = nullptr>
ostream& operator<<(ostream& o, C c) {
  for (auto& e : c) o << setw(7) << right << e;
  return o << endc << endl << coutc;
}
// 7
void debug(const auto& e, const auto&... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3

void _main(int tc) {
  string a, b;
  cin >> a >> b;
  if (a == b) {
    cout << 1 << endl;
    return;
  }
  if (len(a) != len(b)) {
    cout << 3 << endl;
    return;
  }

  int n = 3, m = 9;
  vector<vector<char>> mat(3, vector<char>(9));
  rep(n) {
    rep(j, m) {
      mat[i][j] = (i * m + j) + 'a';
    }
  }
  auto getPos = [&](char c) -> pii {
    int row = (c - 'a') / 9;
    int col = (c - 'a') % 9;
    return {row, col};
  };

  vector<vector<int>> movs = {
    {0, 1}, {1, 0}, {-1, 0}, {0, -1},
    {1, 1}, {-1, -1}, {-1, 1}, {1, -1}
  };

  auto isValid = [&](int i, int j) {
    if (i < 0 or j >= m or j < 0 or i >= n) {
      return false;
    }
    if (i == 2 && j == 8) {
      return false;
    }
    return true;
  };

  auto isNeigh = [&](char a, char b) -> bool {
    if (a == b) return true;
    pii pos = getPos(a);
    for (auto mv : movs) {
      int newI = pos.fi + mv[0], newJ = pos.se + mv[1];
      if (isValid(newI, newJ)) {
        if (b == mat[newI][newJ]) {
          return true;
        }
      }
    }
    return false;
  };
  rep(i, len(a)) {
    if (!isNeigh(a[i], b[i])) {
      cout << 3 << endl;
      return;
    }
  }
  cout << 2 << endl;
}
// 5
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
