// https://lpc.ucfprogrammingteam.org/localFiles/local2017Problems.pdf
// time-limit: 3000
// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
using pii = pair<int, int>;
using li = long long int;
using ld = long double;  // using lli = __int128_t;
#ifdef DEBUG
string to_string(char c) {
  return string({c});
}
// 7
template <class... Ts>
ostream& operator<<(ostream& o, tuple<Ts...> t) {
  string s = "(";
  apply(
      [&](auto&&... r) {
        ((s += to_string(r) + ", "), ...);
      },
      t);
  return o << s.substr(0, len(s) - 2) + ")";
}
// 4
ostream& operator<<(ostream& o, pair<auto, auto> p) {
  return o << "(" + to_string(p.fi) + ", " + to_string(p.se) + ")";
}
// 7
template <class C, class T = typename C::value_type, typename std::enable_if<!std::is_same<C, std::string>::value>::type* = nullptr>
ostream& operator<<(ostream& o, C c) {
  for (auto& e : c) o << setw(4) << right << e;
  return o << endc << endl << coutc;
}
// 7
void debug(const auto& e, const auto&... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
vector<vector<int>> adj, movs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
vector<vector<int>> mat;
vector<int> acum;

int isValid(pair<int, int> u) {
  if (u.first < 0 or u.first >= (int)mat.size())
    return -1;  // invalid position
  if ((u.second < 0 && u.first == 0) or (u.second >= int(mat[u.first].size()) && u.first == (int)mat.size() - 1))
    return -2; // invalid position
  if (u.second >= (int)mat[u.first].size() or u.second < 0)
    return 0;  // invalid with connection possibilities
  return 1;    // completely valid position
}

int getId(pair<int, int> u, int dir = -1) {
  switch (isValid(u)) {
    case 1: return acum[u.first] + u.second;
    case 0: 
            if (0 <= dir && dir < 2) // up or down
              return acum[u.first + 1] - 1;
            else if (dir == 2) // right
              return acum[u.first + 1];
            else // if (dir == 3) // left
              return acum[u.first] - 1;
    default: return -1;
  }
}

void addEdge(int u, int v) {
  adj[u].push_back(v);
}

void addEdge(pair<int, int> u, pair<int, int> v, int dir) {
  int uId = getId(u), vId = getId(v, dir);
  mat[u.first][u.se] = uId;
  if (uId != -1 && vId != -1)
    addEdge(uId, vId);
}

void toGraph() {
  acum = vector<int>(mat.size() + 1);
  for (int i = 1; i <= mat.size(); i++) acum[i] = mat[i - 1].size() + acum[i - 1];
  debug("acum", acum);
  adj = vector<vector<int>>(acum.back());
  for (int i = 0; i < mat.size(); i++) {
    for (int j = 0; j < mat[i].size(); j++) {
      for (int k = 0; k < movs.size(); k++) {
        addEdge({i, j}, {i + movs[k][0], j + movs[k][1]}, k);
      }
    }
  }
}

// 3
void _main(int tc) {
  int r;
  cin >> r;
  mat = vector<vector<int>>(r);
  rep(r) {
    int size;
    cin >> size;
    mat[i] = vector<int>(size + 1);
  }
  pii s, t;
  cin >> s.fi >> s.se >> t.fi >> t.se;
  toGraph();
  debug(mat);
  debug(adj[209]);

  s.fi--, t.fi--;
  int sId = getId(s), tId = getId(t);
  debug(s, sId);
  debug(t, tId);
  vector<int> dist(adj.size(), -1);
  deque<int> q = {sId};
  dist[sId] = 0;
  while (q.size()) {
    int u = q.front();
    q.pop_front();
    for (auto& v : adj[u]) {
      if (dist[v] == -1) {
        q.push_back(v);
        dist[v] = dist[u] + 1;
      }
    }
  }
  cout << dist[tId] << endl;

}
// 5
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc; cin >> tc; rep(i, tc) _main(i + 1);
}
