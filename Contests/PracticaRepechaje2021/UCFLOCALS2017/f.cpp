// time-limit: 3000
// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
using pii = pair<int, int>; using li = long long int;
using ld = long double; // using lli = __int128_t;
#ifdef DEBUG
string to_string(char c) { return string({c}); }
// 7
template<class... Ts>
ostream& operator<<(ostream& o, tuple<Ts...> t) {
  string s = "(";
  apply([&](auto&&... r) {
    ((s += to_string(r) + ", "), ...); }, t);
  return o << s.substr(0, len(s) - 2) + ")";
}
// 4
ostream& operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" + to_string(p.fi) + ", " +
                    to_string(p.se) + ")";
}
// 7
template<class C, class T = typename C::value_type,
typename std::enable_if<!std::is_same<C, std::string>
::value>::type* = nullptr>
ostream& operator<<(ostream &o, C c) {
  for (auto& e : c) o << setw(7) << right << e;
  return o << endc << endl << coutc;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 5
// s = source
typedef long long T;  // sum of costs might overflow
typedef pair<T, int> DistNode;
const T inf = 1 << 30;
vector<vector<int>> adj;
vector<vector<T>> cost;
// 4
void init(int N) {
  adj = vector<vector<int>>(N);
  cost = vector<vector<T>>(N);
}
// 4
// Assumes Directed Graph
void addEdge(int u, int v, T c) {
  adj[u].push_back(v), cost[u].push_back(c);
}
// 17
// ~ O(E * lg(V))
vector<T> dijkstra(int s) {
  vector<T> dist(adj.size(), inf);
  priority_queue<DistNode> q;
  q.push({0, s}), dist[s] = 0;
  while (q.size()) {
    DistNode top = q.top(); q.pop();
    int u = top.second;
    if (dist[u] < -top.first) continue;
    for (int i = 0; i < adj[u].size(); i++) {
      int v = adj[u][i];
      T d = dist[u] + cost[u][i];
      if (d < dist[v]) q.push({-(dist[v] = d), v});
    }
  }
  return dist;
}
// 5
// val = value
typedef string Val;
map<Val, int> intForVal;
map<int, Val> valForInt;
int mapId = 0;
// 5
int Map(Val val) {
  if (intForVal.count(val)) return intForVal[val];
  valForInt[mapId] = val;
  return intForVal[val] = mapId++;
}

Val IMap(int n) { return valForInt[n]; }
// 5
void initMapping() {
  mapId = 0;
  intForVal.clear();
  valForInt.clear();
}
// 3
void _main(int tc) {
  initMapping();
  int n;
  cin >> n;
  init(5 * n + 13);
  int extraIdx = n;
  /*
   * 0. air
   * 1. boat
   * 2. rail
   * 3. truck
   */
  vector<int> nodescost(n);
  rep(n) {
    string s;
    cin >> s;
    li switchCost;
    cin >> switchCost;
    int u = Map(s);
    debug(s, u);
    nodescost[u] = switchCost;
    addEdge(extraIdx, u, switchCost);
    addEdge(u, extraIdx++, switchCost);
    addEdge(extraIdx, u, switchCost);
    addEdge(u, extraIdx++, switchCost);
    addEdge(extraIdx, u, switchCost);
    addEdge(u, extraIdx++, switchCost);
    addEdge(extraIdx, u, switchCost);
    addEdge(u, extraIdx++, switchCost);
  }

  auto getModeId = [&](string &mode) {
    switch(mode[0]) {
      case 'A':
        return 0;
      case 'B':
        return 1;
      case 'S':
        return 1;
      case 'R':
        return 2;
      default:
        return 3;
    }
  };

  int m;
  cin >> m;
  rep(m) {
    string u, v, mode;
    cin >> u >> v >> mode;
    li cost;
    cin >> cost;
    cost *= 2;
    int mu = Map(u), mv = Map(v);
    
    int from = n + 4 * mu + getModeId(mode);
    int to = n + 4 * mv + getModeId(mode);
    addEdge(from, to, cost);
    addEdge(to, from, cost);
    debug(mode);
    debug(u, mu);
    debug(v, mv);
    debug("from-to", from, to);
  }

  string o, d;
  cin >> o >> d;
  int oo = Map(o), dd = Map(d);
  debug("res");
  debug(o, oo);
  debug(d, dd);
  auto dist = dijkstra(oo);
  cout << (dist[dd] - nodescost[oo] - nodescost[dd]) / 2 << endl;
}
// 5
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc; cin >> tc; rep(i, tc) _main(i + 1);
}
