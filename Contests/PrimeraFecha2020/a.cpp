// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
void _main(int tc) {
  int n, q;
  cin >> n >> q;
  vector<pii> maximPerProduct(n);
  pair<int, pii> globalMaxim = {-1, {-1, -1}};
  char globalVeredict = 0;
  map<pii, int> mp;
  vector<char> veredict(n);
  rep(k, q) {
    char opt;
    cin >> opt;
    int i, j, aux;
    switch(opt) {
      case 'R':
        cin >> i >> j;
        i--, j--;
        aux = ++mp[{i, j}];
        if (aux == maximPerProduct[i].se) {
          veredict[i] = 1; // multiple
        } else if (aux > maximPerProduct[i].se) {
          veredict[i] = 2; // unique
          maximPerProduct[i] = {j, aux};
        }
        if (aux == globalMaxim.fi) {
          globalVeredict = 1; // multiple
        } else if (aux > globalMaxim.fi) {
          globalVeredict = 2; // unique
          globalMaxim = {aux, {i, j}};
        }
        break;
      case 'Q':
        cin >> i;
        i--;
        if (veredict[i] == 0) {
          cout << "No info" << endl;
        } else if (veredict[i] == 1) {
          cout << "Multiple" << endl;
        } else {
          cout << maximPerProduct[i].fi + 1 << endl;
        }
        break;
      case 'B':
        if (globalVeredict == 0) {
          cout << "No info" << endl;
        } else if (globalVeredict == 1) {
          cout << "Multiple" << endl;
        } else {
          cout << globalMaxim.se.fi + 1 << " " << globalMaxim.se.se + 1 << endl;
        }
        break;
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
