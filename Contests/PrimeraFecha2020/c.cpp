// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 5
// val = value
typedef string Val;
map<Val, int> intForVal;
map<int, Val> valForInt;
int mapId = 0;
// 5
int Map(Val val) {
  if (intForVal.count(val)) return intForVal[val];
  valForInt[mapId] = val;
  return intForVal[val] = mapId++;
}

Val IMap(int n) { return valForInt[n]; }
// 5
void initMapping() {
  mapId = 0;
  intForVal.clear();
  valForInt.clear();
}
// 4
// comp = component
int compId;
vector<vector<int>> adj;
vector<int> getComp;
// 5
void init(int N) {
  adj.assign(N, vector<int>());
  getComp.assign(N, -1);
  compId = 0;
}
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}
// 6
void dfsCC(int u, vector<int> &comp) {
  if (getComp[u] > -1) return;
  getComp[u] = compId;
  comp.push_back(u);
  for (auto &v : adj[u]) dfsCC(v, comp);
}
// 10
// O(N)
vector<vector<int>> connectedComponents() {
  vector<vector<int>> comps;
  for (int u = 0; u < adj.size(); u++) {
    vector<int> comp;
    dfsCC(u, comp);
    if (!comp.empty())
      comps.push_back(comp), compId++;
  }
  return comps;
}
// 3
void _main(int tc) {
  int n;
  cin >> n;
  init(2e5 + 13);
  rep(n) {
    string u, v;
    cin >> u >> v;
    int uu = Map(u);
    int vv = Map(v);
    debug(uu, vv);
    addEdge(uu, vv);
  }
  auto comps = connectedComponents();
  debug(comps);

  map<string, string> mp;
  rep(len(comps)) {
    auto &cmp = comps[i];
    string minim = "";
    rep(j, len(cmp)) {
      auto s = IMap(cmp[j]);
      if (minim == "" or len(s) < len(minim)) {
        minim = s;
      } else if (len(s) == len(minim) && s < minim) {
        minim = s;
      }
    }

    rep(j, len(cmp)) {
      mp[IMap(cmp[j])] = minim;
    }
  }


  debug(mp);

  string word;
  string ans;
  while(cin >> word) {
    if (mp.count(word)) {
      ans += mp[word] + " ";
    } else {
      ans += word + " ";
    }
  }
  ans.pop_back();
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
