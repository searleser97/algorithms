// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 25
// 9
// 11

// O(lg(b))
li multiply(li a, li b, li m) {
  li ans = 0;
  for (a %= m; b;
       b >>= 1, a <<= 1, a = (a < m ? a : a - m))
    if (b & 1)
      ans += a, ans = (ans < m ? ans : ans - m);
  return ans;
}
//#include "./Modular Multiplication.cpp"

// O(lg(p))
li pow(li a, li p, li m) {
  li ans = 1;
  for (a %= m; p; a = multiply(a, a, m), p >>= 1)
    if (p & 1LL) ans = multiply(ans, a, m);
  return ans;
}
//#include "../Number Theory/Modular Exponentiation.cpp"

bool isPrime(li p, int k = 20) {
  if (p == 2 || p == 3) return 1;
  if ((~p & 1) || p == 1) return 0;
  li d = p - 1, phi = d, r = 0;
  while (~d & 1) d >>= 1, r++;
  while (k--) {
    // set seed with: int main() { srand(time(0)); }
    li a = 2 + rand() % (p - 3);  // [2, p - 2]
    li e = pow(a, d, p), r2 = r;
    if (e == 1 || e == phi) continue;
    bool flag = 1;
    while (--r2) {
      e = multiply(e, e, p);
      if (e == 1) return 0;
      if (e == phi) {
        flag = 0;
        break;
      }
    }
    if (flag) return 0;
  }
  return 1;
}
// 3
void _main(int tc) {
  string s;
  cin >> s;
  int n = len(s);
  int inf = 1 << 30;
  int prod = inf;
  sort(all(s));
  do {
    if (s[0] == '0') {
      continue;
    }
    rep(n - 2) {
      string first = string(s.begin(), s.begin() + i + 1);
      rep(j, i + 1, n - 1) {
        string second = string(s.begin() + i + 1, s.begin() + j + 1);
        string third = string(s.begin() + j + 1, s.begin() + n + 1 );
        if (first[0] == '0' || second[0] == '0' || third[0] == '0') {
          continue;
        }
        debug(first, second, third);
        int n1 = stoi(first), n2 = stoi(second), n3 = stoi(third);
        if (isPrime(n1) && isPrime(n2) && isPrime(n3)) {
          prod = min(prod, n1 * n2 * n3);
        }
      }
    }

  } while (next_permutation(all(s)));
  if (prod == inf) {
    cout << "Bob lies" << endl;
  } else {
    cout << prod << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
