// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 7
// when reading lines, don't mix 'cin' with
// 'getline' just use getline and split
string input() {
  string ans;
  getline(cin, ans);
  return ans;
}
// 8
vector<string> split(string str, char token) {
  stringstream ss(str);
  vector<string> v;
  while (getline(ss, str, token)) v.push_back(str);
  return v;
}
// 3
void _main(int tc) {
  int n = 18;
  vector<int> v(n);
  auto aux = split(input(), ' ');
  rep(n) {
    v[i] = stoi(aux[i]);
  }
  int cnt = 0;
  rep(n) {
    string s = input();
    if (s[0] == 'h') {
      cnt++;
    } else if (s[0] == 'c') {
      cnt += v[i] - 4;
    } else if (s[0] == 'a') {
      cnt += v[i] - 3;
    } else if (s[0] == 'e') {
      cnt += v[i] - 2;
    } else if (s[0] == 'b' && s[1] == 'i') {
      cnt += v[i] - 1;
    } else if (s[0] == 'p') {
      cnt += v[i];
    } else if (s[0] == 'b') {
      cnt += v[i] + 1;
    } else if (s[0] == 'd') {
      cnt += v[i] + 2;
    } else if (s[0] == 't') {
      cnt += v[i] + 3;
    }
  }
    cout << cnt << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
