// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 7
struct UnionFind {
  int n;
  vector<int> dad, size;

  UnionFind(int N) : n(N), dad(N), size(N, 1) {
    while (N--) dad[N] = N;
  }
  // 4
  // O(lg*(N))
  int root(int u) {
    if (dad[u] == u) return u;
    return dad[u] = root(dad[u]);
  }
  // 8
  // O(1)
  void join(int u, int v) {
    int Ru = root(u), Rv = root(v);
    if (Ru == Rv) return;
    if (size[Ru] > size[Rv]) swap(Ru, Rv);
    --n, dad[Ru] = Rv;
    size[Rv] += size[Ru];
  }
  // 4
  // O(lg*(N))
  bool areConnected(int u, int v) {
    return root(u) == root(v);
  }
  // 4
  int getSize(int u) { return size[root(u)]; }

  int numberOfSets() { return n; }
};
// 8
// N = number of nodes, Edge = Weighted Edge
//#include "Data Structures/Union Find.cpp"
typedef int T;
typedef pair<int, int> Edge;
vector<Edge> mst;
vector<Edge> edges, complement;
UnionFind uf(0);
// 5
void init(int N) {
  mst.clear();
  edges.clear();
  uf = UnionFind(N);
}
// 3
void addEdge(int u, int v) {
  edges.push_back({u, v});
}
// 13
void kruskal() {
  sort(edges.begin(), edges.end(), [&](pii a, pii b) {
    return a.fi == b.fi ? a.se < b.se : a.fi < b.fi;
  });
  for (Edge &edge : edges) {
    int u = edge.first, v = edge.second;
    if (!uf.areConnected(u, v))
      uf.join(u, v), mst.push_back(edge);
    else
      complement.pb(edge);
  }
}
// 3
void _main(int tc) {
  int n, m, k;
  cin >> n >> m >> k;
  if (k > m) {
    cout << "Impossible" << endl;
    return;
  }
  init(n);
  rep(m) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    addEdge(u, v);
  }
  kruskal();
  if (uf.numberOfSets() > 1) {
    cout << "Impossible" << endl;
  } else {
    vector<int> v(n);
    for (auto &edge: mst) {
      v[edge.fi]++;
      v[edge.se]++;
    }
    if (len(mst) < k) {
      rep(k - len(mst)) {
        v[complement[i].fi]++;
        v[complement[i].se]++;
      }
    }
    rep(n) {
      cout << v[i] << " \n"[i + 1 == n];
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
