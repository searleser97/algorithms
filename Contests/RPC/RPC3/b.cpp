// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

// 3
// indeg0 = indegree 0

vector<li> e;
vector<vector<int>> adj;
vector<int> indegree, toposorted;
// 4
void init(int n) {
  adj.assign(n, vector<int>());
  e.resize(n);
  indegree.assign(n, 0), toposorted.clear();
}
// 4
void addEdge(int u, int v) {
  adj[u].pb(v);
  indegree[v]++;
}
// 16
// returns false if there is a cycle
// O(V * lg(V) + E)
int befcnt = 0;
li ans = -1;

bool toposort() {
  auto cmp = [](const int &a, const int &b) { return e[a] > e[b]; };
  priority_queue<int, vector<int>, decltype(cmp)> indeg0(cmp);
  for (int u = 0; u < len(adj); u++)
    if (!indegree[u]) indeg0.push(u);
  int cnt = 0;
  while (indeg0.size()) {
    auto u = indeg0.top();
    indeg0.pop();
    ans = max(ans, befcnt + e[u]);
    befcnt++;
    toposorted.push_back(u);
    for (auto &v : adj[u]) {
      if (!--indegree[v]) indeg0.push(v);
    }
    cnt++;
  }
  return cnt < len(adj) ? false : true;
}

void _main(int tc) {
  int n;
  cin >> n;
  init(n);
  rep(i, n) {
    int d;
    cin >> e[i] >> d;
    rep(j, 0, d) {
      int u;
      cin >> u;
      u--;
      addEdge(i, u);
    }
  }
  toposort();
  reverse(all(toposorted));
  ans = -1;
  rep(len(toposorted)) {
    ans = max(ans, i + e[toposorted[i]]);
  }
  cout << ans << endl;
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}