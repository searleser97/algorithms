// time-limit: 1000
// 23
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
using pii = pair<int, int>; using li = long long int;
using ld = long double; // using lli = __int128_t;
#ifdef DEBUG
string to_string(char c) { return string({c}); }
// 7
template<class... Ts>
ostream& operator<<(ostream& o, tuple<Ts...> t) {
  string s = "(";
  apply([&](auto&&... r) {
    ((s += to_string(r) + ", "), ...); }, t);
  return o << s.substr(0, len(s) - 2) + ")";
}
// 4
ostream& operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" + to_string(p.fi) + ", " +
                    to_string(p.se) + ")";
}
// 7
template<class C, class T = typename C::value_type,
typename std::enable_if<!std::is_same<C, std::string>
::value>::type* = nullptr>
ostream& operator<<(ostream &o, C c) {
  for (auto e : c) o << setw(7) << right << e;
  return o << endc << endl << coutc;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 6
// APB = articulation points and bridges
// Ap = Articulation Point
// br = bridges, p = parent
// disc = discovery time, st = stack
// low = lowTime, ch = children
// nup = number of edges from u to p
// 5
int Time;
vector<vector<int>> adj, bcomps;
vector<int> disc, low, st;
vector<pair<int, int>> stEdges;
vector<vector<pair<int, int>>> bcompsEdges;

void init(int N) { 
  adj = vector<vector<int>>(N);
}

// 3
void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 22
int dfsAPB(int u, int p) {
  int ch = 0, nup = 0;
  low[u] = disc[u] = ++Time, st.push_back(u);
  for (int &v : adj[u]) {
    if (v == p && !nup++) continue;
    if (!disc[v] or disc[u] > disc[v])
      stEdges.push_back({u, v});
    if (!disc[v]) {
      int stlen = st.size();
      ++ch, dfsAPB(v, u);
      if (disc[u] <= low[v]) {
        vector<int> bcomp = {u};
        while (stlen < st.size()) {
          bcomp.push_back(st.back());
          st.pop_back();
        }
        bcomps.push_back(bcomp);
        vector<pair<int, int>> edges;
        pair<int, int> thisEdge = {u, v};
        while (edges.empty() or edges.back() != thisEdge) {
          edges.push_back(stEdges.back());
          stEdges.pop_back();
        }
        bcompsEdges.push_back(edges);
      }
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
  return ch;
}
// 8
// O(N)
void biComponents() {
  low = disc = vector<int>(adj.size());
  bcomps.clear(), bcompsEdges.clear();
  Time = 0;
  for (int u = 0; u < adj.size(); u++) {
    if (!disc[u]) {
      st.clear(), stEdges.clear();
      if (dfsAPB(u, u) == 0) {
        bcomps.push_back({u});
        bcompsEdges.push_back({}); // to keep consistency
      }
    }
  }
}

// 3
void _main(int tc) {
  int n, m, b;
  cin >> n >> m >> b;
  debug(n, m, b);
  init(n);
  vector<int> energyCity(n);
  rep(n) {
    cin >> energyCity[i];
  }
  vector<int> energyBuzz(b);
  rep(b) {
    cin >> energyBuzz[i];
  }
  debug(energyCity);
  debug(energyBuzz);

  rep(m) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    debug(u, v);
    addEdge(u, v);
  }
  debug("");
  biComponents(); // biconnected components
  debug(bcomps); // nodes of each component
  debug(bcompsEdges); // edges of each component

  vector<vector<int>> getComps(n);

  rep(len(bcomps)) {
    auto& thiscomp = bcomps[i];
    rep(j, len(thiscomp)) {
      int thisnode = thiscomp[j];
      getComps[thisnode].pb(i);
    }
  }
  vector<int> singles;
  rep(n) {
    bool isSingle = true;
    rep(j, len(getComps[i])) {
      int thiscompid = getComps[i][j];
      auto& thiscomp = bcomps[thiscompid];
      if (thiscomp.size() > 2) isSingle = false;
    }
    if (isSingle) singles.pb(i);
  }
  debug(singles);

  int cyclicComps = 0;
  rep(len(bcomps)) {
    if (bcomps[i].size() > 2) cyclicComps++;
  }
  int totalcomps = cyclicComps + singles.size();
  debug(totalcomps);

  cout << "Buzz Mission " << tc << ": " << totalcomps << " ";
  if (totalcomps > b) {
    cout << "No" << endl;
    return;
  }
  vector<int> energyRegion;
  rep(len(bcomps)) {
    if (bcomps[i].size() > 2) {
      int energy = 0;
      rep(j, len(bcomps[i])) {
        int thisCity = bcomps[i][j];
        energy += energyCity[thisCity];
      }
      energyRegion.pb(energy);
    }
  }

  rep(len(singles)) {
    energyRegion.pb(energyCity[singles[i]]);
  }

  sort(allr(energyRegion));
  sort(allr(energyBuzz));
  debug(energyRegion);
  debug(energyBuzz);
	
	vector<vector<int>> mem(energyRegion.size(), vector<int>(energyBuzz.size(), -1));
  function<int(int, int)> dp = [&](int i, int j) -> int {
    if (i == energyRegion.size()) return 0;
    if (j == energyBuzz.size()) return (int)1e9;
    
    int &res = mem[i][j];
    if (res != -1) return res;
    
    return res = min({
      dp(i, j + 1),
      max({
        dp(i + 1, j + 1), 
        abs(energyRegion[i] - energyBuzz[j])
      })
    });
  };

	cout << dp(0, 0) << endl;

}
// 5
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc; cin >> tc; rep(i, tc) _main(i + 1);
}

