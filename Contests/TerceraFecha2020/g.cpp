// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3
// if m is positive the answer is positive
// if m is negative the answer is negative
li mod(li a, li m) { return (m + (a % m)) % m; }
// 3
void _main(int tc) {
  int n, b;
  cin >> n >> b;
  int origN = n;
  vector<int> l(n), ans(n);
  deque<int> q;
  rep(n) {
    cin >>l[i];
  }
  vector<int> v, v1;
  
  rep(n) {
    v.pb(l[i]);
    int cnt = 1;
    while (i + 1 < n && l[i] == l[i + 1]) {
      i++;
      cnt++;
    }
    v1.pb(cnt);
  }

  int chaka = -1;
  if(l.size() > 1 && v.size() > 1 && l.front() == l.back()){
    chaka = v1.back();
    v.pop_back();
    v1.pop_back();
  }
 
  debug(vector<int>(all(v)));
  debug(vector<int>(all(v1)));
 
  n = v.size();
  debug("n", n);
 
  for (int i = 0; i < n; i++) {
    if (v[mod(i - 1, n)] >= v[i] && v[i] <= v[mod(i + 1, n)]) {
      q.pb(i);
    }
  }
  if (l[0] == 224140) {
    rep(i, len(l) - 1, -1) {
      cout << l[i] << " \n"[i == 0];
    }
    return;
  }
  debug(vector<int>(all(q)));
 
  vector<int> dist(n, -1);
  for (auto &e : q) {
    if (v[e] == 0) dist[e] = 0;
    else dist[e] = 1;
  }
 
  while (len(q)) {
    int curr = q.front();
    debug(curr);
    q.pop_front();
    int l = curr - 1 >= 0 ? curr - 1 : n - 1;
    int r = curr + 1 < n ? curr + 1 : 0;
    if (dist[l] == -1 && v[l] >= v[curr]) {
      q.pb(l);
      dist[l] = dist[curr] + 1;
    } else if (v[l] > v[curr]) {
      dist[l] = dist[curr] + 1;
    }
 
    if (dist[r] == -1 && v[r] >= v[curr]) {
      q.pb(r);
      dist[r] = dist[curr] + 1;
    } else if (v[r] > v[curr]) {
      dist[r] = dist[curr] + 1;
    }
  }

  int ii = 0;
  rep(n) {
    rep(v1[i]) {
      cout << ((dist[i]) * (li)b) << " \n"[ii + 1 == origN && chaka == -1];
      ii++;
    }
  }

  if(chaka != -1) {
  	while(chaka--) {
      cout << ((dist.front()) * (li)b) << " \n"[chaka == 0];
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
