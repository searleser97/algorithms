// time-limit: 5000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 5
// st = segment tree, st[1] = root, H = height of d
// u = updates, d = delayed updates
// neutro = operation neutral val
// e.g. for sum is 0, for multiplication
// is 1, for gcd is 0, for min is INF, etc.
// 8
template <class T>
struct SegmentTree {
  T neutro;
  int N, H;
  vector<T> st, d;
  vector<char> u;
  function<T(T, T)> F;
  SegmentTree() {}
  // 5
  SegmentTree(int n, T neut,
      T f(T, T) = [](T a, T b) { return a + b; })
      : neutro(neut), st(2 * n), d(n), u(n), F(f) {
    H = sizeof(int) * 8 - __builtin_clz(N = n);
  }
  // 7
  void apply(int i, T val, int k, char type) {
    // operation to update st[i] in O(1) time
    //st[i] += val * k;  // updates the tree
    // operation to update d[i] in O(1) time
    // which updates values for future updates
    //if (i < N) d[i] += val, u[i] = 1;
    switch(type) {
      case 1:
        st[i] = 0;
        break;
      case 2:
        st[i] = k;
        break;
      case 3:
        st[i] = k - st[i];
        break;
    }
    if (i < N) {
      if (type != 3) {
        u[i] = type;
      } else {
        if (u[i] == 0) {
          u[i] = 3;
        } else if (u[i] == 1) {
          u[i] = 2;
        } else if (u[i] == 2) {
          u[i] = 1;
        } else if (u[i] == 3) {
          u[i] = 0;
        }
      }
    }
  }
  // 3
  void calc(int i) {
    if (!u[i]) st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 4
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--) calc(i);
  }
  // 4
  // O(lg(N))
  void build(int p) {
    while (p > 1) p >>= 1, calc(p);
  }
  // 12
  // O(lg(N))
  void push(int p) {
    for (int s = H, k = 1 << (H - 1); s;
         s--, k >>= 1) {
      int i = p >> s;
      if (!u[i]) continue;
      apply(i << 1, d[i], k, u[i]);
      apply(i << 1 | 1, d[i], k, u[i]);
      u[i] = 0, d[i] = 0;
    }
  }
  // 10
  // O(lg(N)), [l, r]
  void update(int l, int r, T val, char type) {
    push(l += N), push(r += N);
    int ll = l, rr = r, k = 1;
    for (; l <= r; l >>= 1, r >>= 1, k <<= 1) {
      if (l & 1) apply(l++, val, k, type);
      if (~r & 1) apply(r--, val, k, type);
    }
    build(ll), build(rr);
  }
  // 10
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    push(l += N), push(r += N);
    T ans = neutro;
    for (; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};
// 3
void _main(int tc) {
  int m;
  cin >> m;

  SegmentTree<int> st(1024000 + 13, 0);
  int stindex = 0;
  rep(m) {
    int t;
    string s;
    cin >> t >> s;
    rep(j, t) {
      rep(k, len(s)) {
        st[stindex++] = s[k] - '0';
      }
    }
  }

  debug(st.st);

  st.build();
  int q;
  cin >> q;
  int qCount = 1;
  cout << "Case " << tc << ":" << endl;
  rep(q) {
    char opt;
    int a, b;
    cin >> opt >> a >> b;
    switch (opt) {
      case 'F':
        st.update(a, b, 0, 2);
        break;
      case 'E':
        st.update(a, b, 0, 1);
        break;
      case 'I':
        st.update(a, b, 0, 3);
        break;
      case 'S':
        cout << "Q" << qCount << ": " << st.query(a, b) << endl;
        qCount++;
        break;
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  //_main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);

 }
