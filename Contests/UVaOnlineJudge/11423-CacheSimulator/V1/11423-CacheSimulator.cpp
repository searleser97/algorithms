// time-limit: 10000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &... r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 7
// 0-indexed
//#include "../../BITs Manipulation/Most Significant Set Bit.cpp";

template <class T>
struct BIT {
  T neutro = 0;
  vector<T> bit;
  // 6
  BIT(int n) { bit.assign(++n, neutro); }

  inline T F(T a, T b) {
    return a + b;
    // return a * b;
  }
  // 5
  // Inverse of F
  inline T I(T a, T b) {
    return a - b;
    // return a / b;
  }
  // 7
  // O(N)
  void build() {
    for (int i = 1; i < bit.size(); i++) {
      int j = i + (i & -i);
      if (j < bit.size()) bit[j] = F(bit[j], bit[i]);
    }
  }
  // 4
  // O(lg(N))
  void update(int i, T val) {
    for (i++; i < bit.size(); i += i & -i)
      bit[i] = F(bit[i], val);
  }
  // 6
  // O(lg(N))
  T query(int i) {
    T ans = neutro;
    for (i++; i; i -= i & -i) ans = F(ans, bit[i]);
    return ans;
  }
  // 6
  // O(lg(N)), [l, r]
  T query(int l, int r) {
    return I(query(r), query(--l));
  }
  // 10
  // O(lg(N)) binary search in prefix sum array
  //T lowerBound(T x) {
    //T acum = neutro;
    //int pos = 0;
    //for (int i = msb(bit.size() - 1); i; i >>= 1)
      //if ((pos | i) < bit.size() &&
          //F(acum, bit[pos | i]) < x)
        //acum = F(acum, bit[pos |= i]);
    //return pos;
  //}

  T& operator[](int i) { return bit[++i]; }
};
// 3
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> cacheSize(n);
  vector<int> missesPerCache(n);
  vector<int> addrAccessTime(1 << 24);
  BIT<int> bit(1e7 + 13);
  int time = 0;

  rep(n) {
    cin >> cacheSize[i];
  }

  auto access = [&](int addr) {
    if (addrAccessTime[addr] == 0) {
      rep(n) {
        missesPerCache[i]++;
      }
    } else {
      // count of addresses accessed between 'addr' last access time and current time.
      int cnt = bit.query(addrAccessTime[addr], time);
      for (int i = 0; i < n && cacheSize[i] < cnt; i++) {
        missesPerCache[i]++;
      }
      bit.update(addrAccessTime[addr], -1);
    }
    addrAccessTime[addr] = ++time;
    bit.update(time, 1);
  };
  string opt;
  while(cin >> opt && opt[0] != 'E') {
    switch(opt[0]) {
      case 'A':
        int x;
        cin >> x;
        access(x);
        break;
      case 'R':
        int b, y, N;
        cin >> b >> y >> N;
        rep(N) {
          access(b + y * i);
        }
        break;
      case 'S':
        rep(n) {
          cout << missesPerCache[i] << " \n"[i == n - 1];
        }
        missesPerCache.assign(n, 0);
        break;
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}
