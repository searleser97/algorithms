// time-limit: 10000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 3

/*
 * This solution is not feasible since just the creation of the arrays takes too long
 * 1 << 24 is approximately 1e7
 * However, this is solution is correct
 */
void _main(int tc) {
  int n;
  cin >> n;
  vector<int> cacheSize(n);
  vector<list<int>> v(n);
  vector<vector<list<int>::iterator>> inQueue(n, vector<list<int>::iterator>(1 << 24));
  vector<vector<bool>> isInQueue(n, vector<bool>(1 << 24));
  vector<int> missesPerCache(n);

  rep(n) {
    cin >> cacheSize[i];
  }

  auto access = [&](int addr) {
    rep(n) {
      if (!isInQueue[i][addr]) {
        missesPerCache[i]++;
        v[i].push_front(addr);
        inQueue[i][addr] = v[i].begin();
        isInQueue[i][addr] = true;
        if (len(v[i]) > cacheSize[i]) {
          isInQueue[i][v[i].back()] = false;
          v[i].pop_back();
        }
      } else {
        v[i].erase(inQueue[i][addr]);
        v[i].push_front(addr);
        inQueue[i][addr] = v[i].begin();
      }
    }
  };

  string opt;
  while (cin >> opt && opt[0] != 'E') {
    switch (opt[0]) {
      case 'A':
        int x;
        cin >> x;
        access(x);
        break;
      case 'R':
        int b, y, N;
        cin >> b >> y >> N;
        rep(N) {
          access(b + y * i);
        }
        break;
      case 'S':
        rep(n) {
          cout << missesPerCache[i] << " \n"[i == n - 1];
        }
        missesPerCache.assign(n, 0);
        break;
    }
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
  return 0;
}
