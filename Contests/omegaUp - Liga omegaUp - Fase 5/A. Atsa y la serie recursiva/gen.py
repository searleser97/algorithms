from math import *
from sys import stdin, stdout

def isPowerOf2(n):
    return n and (not bool(n & (n - 1)))

def calc(x):
    superans = 0
    while True:
        if (x <= 1):
            superans += x
            break
        left = 1 << (int(log2(x)) - 1)
        power = 0
        ans = 1 << (int(log2(x)))
        while left >= 1:
            ans += left * (1 << power)
            left /= 2
            power += 1
        if (isPowerOf2(x)):
            superans += ans
            break
        else:
            superans += ans
            x = x - (1 << (int(log2(x))))
    return int(superans)

q = int(stdin.readline())
for i in range(q):
    x, y = list(map(int, stdin.readline().split()))
    stdout.write(str(calc(y) - calc(x - 1)) + "\n")
