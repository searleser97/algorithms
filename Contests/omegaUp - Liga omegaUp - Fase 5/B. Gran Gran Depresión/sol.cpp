// 21
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef LOCAL
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3
const li inf = 1e18;
void _main(int tc) {
  li n, W = 1002;
  while (cin >> n) {
    vector<li> w(1000 + 1, inf), v(1000 + 1, -inf);
    rep(i, n) { cin >> v[i] >> w[i]; }
    vector<vector<li>> mem(n + 1, vector<li>(W + 1, 0));
    rep(i, n - 1, -1) {
      rep(k, W, -1) {
        if (k - w[i] >= 0)
          mem[i][k] = max(v[i] + mem[i + 1][k - w[i]], mem[i + 1][k]);
        else
          mem[i][k] = mem[i + 1][k];
      }
    }
    int q;
    cin >> q;
    li ans = 0;
    rep(i, q) {
      li vol;
      cin >> vol;
      ans += mem[0][vol];
    }
    cout << ans << endl;
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}