from math import *
t = int(input())

for _ in range(t):
  n = int(input())
  h = sqrt(n * n - n * n / 4)
  print("%.2f" % round(h, 2))