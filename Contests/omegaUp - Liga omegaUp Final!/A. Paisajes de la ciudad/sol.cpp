// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) \
  M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) \
  for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;

#define ordered_set                            \
  tree<int, null_type, less<int>, rb_tree_tag, \
       tree_order_statistics_node_update>

vector<pairii> printNGE(vector<int> &arr, int n) { 
  stack < int > s; 
  vector<pairii> ans;
  s.push(arr[0]); 
  for (int i = 1; i < n; i++) { 
    if (s.empty()) { 
      s.push(arr[i]); 
      continue; 
    } 
    while (!s.empty() && s.top() < arr[i])
    {          
        ans.pb({s.top(), arr[i]});
        s.pop(); 
    }
    s.push(arr[i]); 
  }
  while (s.empty() == false) {
    ans.pb({s.top(), -1});
    s.pop(); 
  }
  return ans;
} 

void _main(int tc) {
  int n;
  cin >> n;
  if (n == 0) return;
  vector<int> v(n);
  map<int, int> poss;
  rep(n) {
    cin >> v[i];
    poss[v[i]] = i;
  }
  poss[-1] = n;
  auto torightaux = printNGE(v, n);
  reverse(all(v));
  auto toleftaux = printNGE(v, n);
  reverse(all(v));

  vector<int> toright(n), toleft(n);

  for (auto & e : torightaux) {
    toright[poss[e.fi]] = poss[e.se];
  }

  debug(toright);

  for (auto & e : toleftaux) {
    toleft[poss[e.fi]] = poss[e.se];
  }

  debug(toleft);

  rep(n) {
    if (toleft[i] == n) {
      toleft[i] = -1;
    }
    cout << 1 + (toright[i] - i - 1) + (i - 1 - toleft[i]) << " \n"[i + 1 == n];
  }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}