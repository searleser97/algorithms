// 22
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define len(x) int(x.size())
#define lli __int128_t
#define li long long int
#define ld long double
// 4
template <class A, class B>
ostream &operator<<(ostream &o, const pair<A, B> &p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 5
template <class T>
ostream &operator<<(ostream &o, const vector<T> &v) {
  rep(i, len(v)) o << setw(7) << left << v[i];
  return o << endc << endl << coutc;
}
// 5
template <class A, class B>
ostream &operator<<(ostream &o, const map<A, B> &m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
template <class T, class... Ts>
void debug(const T &e, const Ts &... r) {
#ifdef DEBUG
  cerr << coutc << e;
  ((cerr << " " << r), ..., (cerr << endc << endl));
#endif
}
// 3
void _main(int tc) {
  string s, t;
  cin >> s >> t;
  vector<vector<pair<int, vector<int>>>> mem(
      2, vector<pair<int, vector<int>>>(t.size() + 1));
  rep(i, len(s) - 1, -1) {
    int newIndex = i % 2;
    rep(j, len(t) - 1, -1) {
      if (s[i] == t[j]) {
        mem[newIndex][j].fi = 1 + mem[1 - newIndex][j + 1].fi;
        mem[1 - newIndex][j + 1].se.pb(i);
        mem[1 - newIndex][j + 1].se.pb(j);
        mem[newIndex][j].se = mem[1 - newIndex][j + 1].se;
      } else {
        if (mem[1 - newIndex][j] > mem[newIndex][j + 1]) {
          mem[newIndex][j].fi = mem[1 - newIndex][j].fi;
          mem[newIndex][j].se = mem[1 - newIndex][j].se;
        } else {
          mem[newIndex][j].fi = mem[newIndex][j + 1].fi;
          mem[newIndex][j].se = mem[newIndex][j + 1].se;
        }
      }
    }
  }
  debug(mem);
  cout << mem[0][0].fi << endl;

  auto &ans = mem[0][0].se;
  reverse(all(ans));
  rep(i, 1, len(ans), 2) {
    cout << ans[i] << " " << ans[i - 1] << endl;
  }

  // vector<vector<int>> mem(s.size(), vector<int>(t.size()));

  // rep(i, len(s) - 1, -1) {
  //   rep(j, len(t) - 1, -1) {
  //     if (s[i] == t[j]) {
  //       if (i + 1 >= len(s) or j + 1 >= len(t))
  //         mem[i][j] = 1;
  //       else
  //         mem[i][j] = 1 + mem[i + 1][j + 1];
  //     } else {
  //       auto ans1 = i + 1 >= len(s) or j >= len(t) ? 0 : mem[i + 1][j];
  //       auto ans2 = i >= len(s) or j + 1 >= len(t) ? 0 : mem[i][j + 1];
  //       mem[i][j] = max(ans1, ans2);
  //     }
  //   }
  // }
  // cout << mem[0][0] << endl;
  // int i = 0, j = 0;
  // while (i < len(s) && j < len(t)) {
  //   if (s[i] == t[j]) {
  //     cout << i << " " << j << endl;
  //     i++, j++;
  //   } else {
  //     auto ans1 = i + 1 >= len(s) or j >= len(t) ? -1 : mem[i + 1][j];
  //     auto ans2 = i >= len(s) or j + 1 >= len(t) ? -1 : mem[i][j + 1];
  //     if (ans1 > ans2)
  //       i++;
  //     else
  //       j++;
  //   }
  // }
}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}