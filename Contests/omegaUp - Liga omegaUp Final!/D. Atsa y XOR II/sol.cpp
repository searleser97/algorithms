#include<bits/stdc++.h> 
using namespace std; 
  
#define INT_SIZE 64
  
struct TrieNode 
{ 
    long long int value;
    TrieNode *arr[2]; 
}; 
  
TrieNode *newNode() 
{ 
    TrieNode *temp = new TrieNode; 
    temp->value = 0; 
    temp->arr[0] = temp->arr[1] = NULL; 
    return temp; 
} 
  
void insert(TrieNode *root, long long int pre_xor) 
{ 
    TrieNode *temp = root; 
  
    for (int i=INT_SIZE-1; i>=0; i--) 
    { 
        bool val = pre_xor & (1ll<<i); 
  
        if (temp->arr[val] == NULL) 
            temp->arr[val] = newNode(); 
  
        temp = temp->arr[val]; 
    } 
  
    temp->value = pre_xor; 
} 
  
 
long long int query(TrieNode *root, long long int pre_xor) 
{ 
    TrieNode *temp = root; 
    for (int i=INT_SIZE-1; i>=0; i--) 
    { 
        bool val = pre_xor & (1ll<<i); 
  
        if (temp->arr[1-val]!=NULL) 
            temp = temp->arr[1-val]; 
  

        else if (temp->arr[val] != NULL) 
            temp = temp->arr[val]; 
    } 
    return pre_xor^(temp->value); 
} 

const long long int inf = 2e18;
long long int maxSubarrayXOR(long long int arr[], int n) 
{ 
    TrieNode *root = newNode(); 
    insert(root, 0); 
  
    long long int result = -inf, pre_xor =0; 
  
    for (int i=0; i<n; i++) 
    { 
        pre_xor = pre_xor^arr[i]; 
        insert(root, pre_xor); 
  
        result = max(result, query(root, pre_xor)); 
    } 
    return result; 
} 
  
int main() 
{ 
    int n;
    cin >> n;
    long long int arr[n];
    for (int i = 0; i < n; i++) cin >> arr[i];
    cout << maxSubarrayXOR(arr, n) << endl; 
    return 0; 
} 