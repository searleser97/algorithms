#include <bits/stdc++.h>
using namespace std;

void reverseSubstring(string &text, int left, int right) {
  if (left > right or left < 0 or right >= (int)text.size()) return;
  while (left < right) {
    swap(text[left], text[right]);
    left++, right--;
  }
}

void reverseWordsInText(string &text) {
  for (int l = 0, r = 0; r < text.size(); ) {
    while (r < (int)text.size() && text[r] != ' ') {
      r++;
    }
    reverseSubstring(text, l, r - 1);
    while (r < (int)text.size() && text[r] == ' ') {
      r++;
    }
    l = r;
  }
}

int main() {
  string text;
  getline(cin, text);
  reverseSubstring(text, 0, text.size() - 1);
  reverseWordsInText(text);
  cout << text << endl;
}
