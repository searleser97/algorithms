class SnapshotArray {
private:
    int snapId;
    vector<vector<pair<int, int>>> snapshots;
public:
    SnapshotArray(int length) {
        snapId = 0;
        snapshots.assign(length, {{0, 0}});
    }
    
    void set(int index, int val) {
        snapshots[index].push_back({snapId, val});
    }
    
    int snap() {
        return snapId++;
    }
    
    int get(int index, int snap_id) {
        auto &versions = snapshots[index];
        int l = 0, r = versions.size() - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (versions[mid].first <= snap_id) l = mid + 1;
            else r = mid - 1;
        }
        return versions[r].second;
    }
};

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * SnapshotArray* obj = new SnapshotArray(length);
 * obj->set(index,val);
 * int param_2 = obj->snap();
 * int param_3 = obj->get(index,snap_id);
 */
