/*
// Sample code to perform I/O:
#include <stdio.h>

int main(){
  int num;
  scanf("%d", &num);                    // Reading input from STDIN
  printf("Input number is %d.\n", num);       // Writing output to STDOUT
}

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
#include <bits/stdc++.h>
using namespace std;

int main() {
  int n;
  cin >> n;
  vector<string> mat(n);
  for (int i = 0; i < n; i++) {
    cin >> mat[i];
  }

  vector<vector<int>> movs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
  
  vector<vector<bool>> vis(n, vector<bool>(n));
  function<int(int, int)> dfs = [&](int i, int j) {
    if (i < 0 or i >= n or j < 0 or j >= n or vis[i][j] or mat[i][j] == 'W') return 0;
    vis[i][j] = 1;
    int cnt = 1;
    for (auto & mv : movs) {
      cnt += dfs(i + mv[0], j + mv[1]);
    }
    return cnt;
  };

  int ans = 0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      ans = max(ans, dfs(i, j));
    }
  }
  cout << ans << endl;
  return 0;
} 
