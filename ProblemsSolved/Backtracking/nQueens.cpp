/*
// Sample code to perform I/O:

#include <iostream>

using namespace std;

int main() {
  int num;
  cin >> num;                   // Reading input from STDIN
  cout << "Input number is " << num << endl;    // Writing output to STDOUT
}

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here

#include <bits/stdc++.h>
using namespace std;

bool canPlace(vector<vector<int>> &mat, int i, int j) {
  for (int ii = i - 1; ii >= 0; ii--) {
    if (mat[ii][j] == 1) return false;
  }

  for (int ii = i, jj = j; ii >= 0 && jj >= 0; ii--, jj--) {
    if (mat[ii][jj] == 1) return false;
  }

  for (int ii = i, jj = j; ii >= 0 && jj < (int)mat.size(); ii--, jj++) {
    if (mat[ii][jj] == 1) return false;
  }
  return true;
}

bool util(vector<vector<int>> &mat, int i) {
  if (i >= (int)mat.size()) return true;
  for (int j = 0; j < (int)mat.size(); j++) {
    if (canPlace(mat, i, j)) {
      mat[i][j] = 1;
      if (util(mat, i + 1)) return true;
      mat[i][j] = 0;
    }
  }
  return false;
}

int main() {
  int n;
  cin >> n;
  if (n == 2 or n == 3) {
    cout << "Not possible" << endl;
    return 0;
  }
  vector<vector<int>> mat(n, vector<int>(n));
  util(mat, 0);
  for (int i = 0; i < (int)mat.size(); i++) {
    for (int j = 0; j < (int)mat[0].size(); j++) {
      cout << mat[i][j] << " \n"[j + 1 == (int)mat[0].size()];
    }
  }
  return 0;
}
