/*
// Sample code to perform I/O:
#include <stdio.h>

int main(){
  int num;
  scanf("%d", &num);                    // Reading input from STDIN
  printf("Input number is %d.\n", num);       // Writing output to STDOUT
}

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here

#include <bits/stdc++.h>
using namespace std;

int main() {
  int n;
  cin >> n;
  vector<int> v = {1, 2, 3};
  vector<int> mem(n + 1, -1);
  function<long long(int)> f = [&](int i) {
    if (i > n) return 0;
    if (i == n) return 1;
    if (mem[i] != -1) return mem[i];
    int ans = 0;
    for (auto &op : v) {
      ans += f(i + op);
    }
    return mem[i] = ans;
  };
  cout << f(0) << endl;
  return 0;
} 
