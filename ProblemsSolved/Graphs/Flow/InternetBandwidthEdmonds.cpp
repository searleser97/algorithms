/*
    backward edges keep equilibrium of flow in graph
    they are used as technique to find new paths.
    it also helps by not allowing to find the same path more than once
    in edmons karp algorithm(BFS).

    Ford Fulkerson:
        DFS algorithm.
    Edmons Karp Algorithm:
        Runs BFS repeatedly on residual graph
        each time it finishes a BFS we add to the max-flow the urent max flow found 
        and we update the capacity of each edge (keeping equilibrium) considering backward edges.
    Dinic's Algorithm:
        https://www.slideshare.net/KuoE0/acmicpc-dinics-algorithm
        https://www.geeksforgeeks.org/dinics-algorithm-maximum-flow/

    // bottleneck = min(capacity - flow);
    // curpathMaxFlow

    // the 'v' node in blockingFlow dfs is not necessarily one level up
    // because of the updates on backward edges.
*/

/*
    PROBLEM:
        determine max flow of a bidirectional graph where 
        the capacity of u to v is the capacity for v to u;
*/

// cap[a][b] = Capacity from a to b
// flow[a][b] = flow occupied from a to b
// level[a] = level in graph of node a
// prev[a] = previous node of a



#include<bits/stdc++.h>

using namespace std;

// 6
// icap = initial capacity, f[u] = flow of u
// adj[u][i] = edge from u to v = adj[u][i]
// cap[u][i] = capacity of edge u->adj[u][i]
// rev[u][i] = index of u in adj[v]
// rev is used to find the corresponding reverse edge
// s = source, t = target
// 3
typedef int T;
vector<vector<int>> adj, cap, rev;
T inf = 1 << 30;
// 3
void init(int N) {
  adj = cap = rev = vector<vector<int>>(N);
}
// 7
// Assumes Directed Graph
void addEdge(int u, int v, T icap) {
  rev[u].push_back(adj[v].size());
  rev[v].push_back(adj[u].size());
  adj[u].push_back(v), adj[v].push_back(u);
  cap[u].push_back(icap), cap[v].push_back(0);
}
// 29
// O(N)
T maxFlowMinCut(int s, int t) {
  if (s == t) return inf;
  T maxFlow = 0;
  vector<T> f(adj.size());
  while (true) {
    vector<pair<int, int>> dad(adj.size(), {-1, -1});
    queue<int> q;
    q.push(s), f[s] = inf;
    while (q.size() && dad[t].first == -1) {
      int u = q.front(); q.pop();
      for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        if (dad[v].first == -1 && cap[u][i]) {
          f[v] = min(f[u], cap[u][i]);
          q.push(v), dad[v] = {u, i};
        }
      }
    }
    if (dad[t].first == -1) break;
    maxFlow += f[t];
    for (int v = t; v != s; v = dad[v].first) {
      auto u = dad[v];
      cap[u.first][u.second] -= f[t];
      cap[v][rev[u.first][u.second]] += f[t];
    }
  }
  return maxFlow;
}

// -----------------

typedef int Key;
unordered_map<int, int> id;
int nextId = 0;

int Map(Key key) {
    return id.count(key) ? id[key] : id[key] = nextId++;
}

void initMapping() {
    nextId = 0;
    id.clear();
}

// -----------------

int main() {
    ios_base::sync_with_stdio(0); cin.tie(0);
    int N;
    int s, t, c, i = 1;
    while (true) {
        cin >> N;
        if (!N)
            return 0;      
        initMapping();
        init(N);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        cin >> s >> t >> c;
        while (c--) {
            int a, b, c;
            cin >> a >> b >> c;
            Key u = Map(a);
            Key v = Map(b);
            addEdge(u, v, c);
            addEdge(v, u, c);
        }
        cout << "Network " << i << endl;
        cout << "The bandwidth is " << maxFlowMinCut(Map(s), Map(t)) << "." << endl;
        cout << endl;
        i++;
    }
    return 0;
}