/*
    backward edges keep equilibrium of flow in graph
    they are used as technique to find new paths.
    it also helps by not allowing to find the same path more than once
    in edmons karp algorithm(BFS).

    Ford Fulkerson:
        DFS algorithm.
    Edmons Karp Algorithm:
        Runs BFS repeatedly on residual graph
        each time it finishes a BFS we add to the max-flow the urent max flow
   found and we update the capacity of each edge (keeping equilibrium)
   considering backward edges. Dinic's Algorithm:
        https://www.slideshare.net/KuoE0/acmicpc-dinics-algorithm
        https://www.geeksforgeeks.org/dinics-algorithm-maximum-flow/

    // bottleneck = min(capacity - flow);
    // curpathMaxFlow

    // the 'v' node in blockingFlow dfs is not necessarily one level up
    // because of the updates on backward edges.
*/

/*
    PROBLEM:
        determine max flow of a bidirectional graph where
        the capacity of u to v is the capacity for v to u;
*/

// cap[a][b] = Capacity from a to b
// flow[a][b] = flow occupied from a to b
// level[a] = level in graph of node a
// prev[a] = previous node of a

#include <bits/stdc++.h>

using namespace std;

template <typename T>
struct flowEdge {
  int dest;
  T flow, capacity, cost;
  flowEdge *res;

  flowEdge() : dest(0), flow(0), capacity(0), cost(0), res(NULL) {}
  flowEdge(int dest, T flow, T capacity, T cost = 0)
      : dest(dest), flow(flow), capacity(capacity), cost(cost), res(NULL) {}

  void addFlow(T flow) {
    this->flow += flow;
    this->res->flow -= flow;
  }
};

template <typename T>
struct flowGraph {
  T inf = numeric_limits<T>::max();
  vector<vector<flowEdge<T> *>> adjList;
  vector<int> dist, pos;
  int V;
  flowGraph(int V) : V(V), adjList(V), dist(V), pos(V) {}
  ~flowGraph() {
    for (int i = 0; i < V; ++i)
      for (int j = 0; j < adjList[i].size(); ++j) delete adjList[i][j];
  }
  
  void addEdge(int u, int v, T capacity, T cost = 0) {
    flowEdge<T> *uv = new flowEdge<T>(v, 0, capacity, cost);
    flowEdge<T> *vu = new flowEdge<T>(u, capacity, capacity, -cost);
    uv->res = vu;
    vu->res = uv;
    adjList[u].push_back(uv);
    adjList[v].push_back(vu);
  }

  T blockingFlow(int u, int t, T flow) {
    if (u == t) return flow;
    for (int &i = pos[u]; i < adjList[u].size(); ++i) {
      flowEdge<T> *v = adjList[u][i];
      if (v->capacity > v->flow && dist[u] + 1 == dist[v->dest]) {
        T fv = blockingFlow(v->dest, t, min(flow, v->capacity - v->flow));
        if (fv > 0) {
          v->addFlow(fv);
          return fv;
        }
      }
    }
    return 0;
  }

  T dinic(int s, int t) {
    T maxFlow = 0;
    dist[t] = 0;
    while (dist[t] != -1) {
      fill(dist.begin(), dist.end(), -1);
      queue<int> Q;
      Q.push(s);
      dist[s] = 0;
      while (!Q.empty()) {
        int u = Q.front();
        Q.pop();
        for (flowEdge<T> *v : adjList[u]) {
          if (dist[v->dest] == -1 && v->flow != v->capacity) {
            dist[v->dest] = dist[u] + 1;
            Q.push(v->dest);
          }
        }
      }
      if (dist[t] != -1) {
        T f;
        fill(pos.begin(), pos.end(), 0);
        while (f = blockingFlow(s, t, inf)) maxFlow += f;
      }
    }
    return maxFlow;
  }
};

// -----------------

typedef int Key;
unordered_map<int, int> id;
int nextId = 0;

int Map(Key key) { return id.count(key) ? id[key] : id[key] = nextId++; }

void initMapping() {
  nextId = 0;
  id.clear();
}

void init(int N) {
  initMapping();
  // adj.assign(N, vector<int>());
  // cap.assign(N, vector<int>(N));
  // flow.assign(N, vector<int>(N));
}

// -----------------

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  int N;
  int s, t, c, i = 1;
  while (true) {
    cin >> N;
    if (!N) return 0;
    init(N);
    flowGraph<int> g(N);
    cin >> s >> t >> c;
    while (c--) {
      int a, b, c;
      cin >> a >> b >> c;
      Key u = Map(a);
      Key v = Map(b);
      g.addEdge(u, v, c);
      g.addEdge(v, u, c);
    }
    cout << "Network " << i << endl;
    cout << "The bandwidth is " << g.dinic(Map(s), Map(t)) << "." << endl;
    cout << endl;
    i++;
  }
  return 0;
}