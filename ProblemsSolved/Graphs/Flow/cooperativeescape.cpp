// 20
/****************************************************
https://searleser97.gitlab.io/algorithms/template.cpp
****************************************************/

#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define forr(_, x, n) for (int _ = x; _ > n; _--)
#define fos(_, x, n, s) for (int _ = x; _ < n; _ += s)
#define forn(_, x, n) fos(_, x, n, 1)
#define rep(_, n) forn(_, 0, n)
#define fi first
#define se second
#define pb push_back
#define pairii pair<int, int>
#define all(x) x.begin(), x.end()
#define cerr(s) cerr << "\033[48;5;196m\033[38;5;15m" << s << "\033[0m"
// typedef __int128_t lli;
typedef long long int li;
typedef long double ld;
#include <bits/stdc++.h>
using namespace std;
// 7
// icost = initial cost, icap = initial capacity
// adj[u][i] = edge from u to v = adj[u][i]
// cap[u][i] = capacity of edge u->adj[u][i]
// cost[u][i] = cost of edge u->adj[u][i]
// rev[u][i] = index of u in adj[v], s = source
// rev is used to find the corresponding reverse edge
// d = distance vector, f[u] = flow of u, t = target
// 3
typedef int T;
const T inf = 1 << 30;
vector<vector<int>> adj, cap, cost, rev;
// 6
void init(int N) {
  adj.assign(N, vector<int>());
  cap.assign(N, vector<int>());
  cost.assign(N, vector<int>());
  rev.assign(N, vector<int>());
}
// 8
// Assumes Directed Graph
void addEdge(int u, int v, T icap, T icost) {
  rev[u].push_back(adj[v].size());
  rev[v].push_back(adj[u].size());
  adj[u].push_back(v), adj[v].push_back(u);
  cap[u].push_back(icap), cap[v].push_back(0);
  cost[u].push_back(icost), cost[v].push_back(-icost);
}
// 35
// O(V^2 * E^2)
pair<T, T> minCostFlow(int s, int t, int k = inf) {
  if (s == t) return {k, 0};
  T flow = 0, fcost = 0;
  vector<int> inqueue(adj.size());
  vector<T> f(adj.size());
  while (flow < k) {
    vector<pair<int, int>> dad(adj.size(), {-1, -1});
    vector<T> d(adj.size(), inf);
    vector<int> its(adj.size());
    queue<int> q;
    q.push(s), d[s] = 0, its[s] = 1, f[s] = k - flow;
    while (q.size()) {
      int u = q.front();
      q.pop(), inqueue[u] = 0;
      for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        T dist = d[u] + cost[u][i];
        if (dist < d[v] && cap[u][i]) {
          d[v] = dist, dad[v] = {u, i};
          f[v] = min(f[u], cap[u][i]);
          if (!inqueue[v]++) q.push(v), its[v]++;
          if (its[v] == adj.size()) return {-1, -1};
        }
      }
    }
    if (dad[t].first == -1) break;
    flow += f[t], fcost += f[t] * d[t];
    for (int v = t; v != s; v = dad[v].first) {
      auto u = dad[v];
      cap[u.first][u.second] -= f[t];
      cap[v][rev[u.first][u.second]] += f[t];
    }
  }
  return {flow, fcost};
}

bool isOutOfBounds(int i, int j, int r, int c) {
  if (i < 0 or i >= r or j < 0 or j >= c) return true;
  return false;
}

struct Node {
  int in, out;
  char symbol;
};

void _main(int tc) {
  int r, c, offset = 2;
  cin >> r >> c;
  vector<vector<Node>> mat(r, vector<Node>(c));
  init(offset + 2 * r * c);
  Node bonnie, clyde, ford;
  int id = offset;
  rep(i, r) {
    rep(j, c) {
      char x;
      cin >> x;
      mat[i][j] = {id, id + 1, x};
      id += 2;
      if (x == '.') addEdge(mat[i][j].in, mat[i][j].out, 1, 0);
      else if (x == 'F') addEdge(mat[i][j].in, mat[i][j].out, 2, 0);
      else addEdge(mat[i][j].in, mat[i][j].out, 0, inf);
      
      if (x == 'B') bonnie = mat[i][j];
      else if (x == 'C')  clyde = mat[i][j];
      else if (x == 'F') ford = mat[i][j];
    }
  }
  vector<vector<int>> movs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
  rep(i, r) {
    rep(j, c) {
      for (auto &mov : movs) {
        int newI = i + mov[0];
        int newJ = j + mov[1];
        if (!isOutOfBounds(newI, newJ, r, c)) {
          if (mat[newI][newJ].symbol  != 'F')
            addEdge(mat[newI][newJ].out, mat[i][j].in, 1, 1);
        }
      }
    }
  }
  int source = 0;
  addEdge(source, bonnie.out, 1 , 0);
  addEdge(source, clyde.out, 1, 0);
  addEdge(ford.out, 1, 2, 0);
  auto ans = minCostFlow(0, 1, 2);
  if (ans.first < 2) cout << -1 << endl;
  else cout << ans.second << endl;
}

int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0); return 0;
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}