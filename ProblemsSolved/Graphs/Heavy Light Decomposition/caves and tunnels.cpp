#include <bits/stdc++.h>
using namespace std;

template <class T>
struct SegmentTree {
  T neutro = 0;
  int N;
  vector<T> st;
  function<T(T, T)> F;
  SegmentTree() {}
  // 3
  SegmentTree(int n, int neut,
      T f(T, T) = [](T a, T b) { return a + b; })
      : neutro(neut), N(n), st(2 * n, neutro), F(f) {}
  // 5
  // O(2N)
  void build() {
    for (int i = N - 1; i; i--)
      st[i] = F(st[i << 1], st[i << 1 | 1]);
  }
  // 5
  // O(lg(2N)), works like replacing arr[i] with val
  void update(int i, T val) {
    for (st[i += N] += val; i > 1; i >>= 1)
      st[i >> 1] = F(st[i], st[i ^ 1]);
  }
  // 9
  // O(lg(2N)), [l, r]
  T query(int l, int r) {
    T ans = neutro;
    for (l += N, r += N; l <= r; l >>= 1, r >>= 1) {
      if (l & 1) ans = F(ans, st[l++]);
      if (~r & 1) ans = F(ans, st[r--]);
    }
    return ans;
  }
  // 2
  T& operator[](int i) { return st[i + N]; }
};

// 8
template <class T>
struct HLD {
  SegmentTree<T> st;
  function<T(T, T)> F;
  vector<int> dad, heavy, depth, root, stPos, iStPos;
  bool inEdges;
  // 24
  // O(N)
  HLD(vector<vector<int>>& adj, T initVal, bool edges,
      T f(T, T) = [](T a, T b) { return a + b; })
      : F(f), heavy(adj.size(), -1), inEdges(edges) {
    dad = root = stPos = iStPos = depth = heavy;
    st = SegmentTree<T>(adj.size(), initVal, f);
    function<int(int)> dfs = [&](int u) {
      int size = 1, maxSubtree = 0;
      for (int& v : adj[u]) {
        if (v == dad[u]) continue;
        dad[v] = u, depth[v] = depth[u] + 1;
        int subtree = dfs(v);
        if (subtree > maxSubtree)
          heavy[u] = v, maxSubtree = subtree;
        size += subtree;
      }
      return size;
    };
    dad[0] = -1, depth[0] = 0, dfs(0);
    for (int i = 0, pos = 0; i < adj.size(); i++)
      if (dad[i] < 0 || heavy[dad[i]] != i)
        for (int j = i; ~j; j = heavy[j])
          iStPos[stPos[j] = pos++] = j, root[j] = i;
  }
  // 11
  // O(lg(N) * O(op))
  int lca(int u, int v,
      function<void(T, T)> op = [](T, T) {}) {
    for (; root[u] != root[v]; v = dad[root[v]]) {
      if (depth[root[u]] > depth[root[v]]) swap(u, v);
      op(stPos[root[v]], stPos[v]);
    }
    if (depth[u] > depth[v]) swap(u, v);
    op(stPos[u], stPos[v]);
    return u;
  }
  // 10
  // O(lg^2 (N))
  void update(int u, int v, int val) {
    /* for single node or edge update, don't use lazy */
    if (depth[u] > depth[v]) swap(u, v);
    st.update(stPos[v], val); /**/
    /* for update in a range of edges or nodes
    lca(u, v, [&](int l, int r) {
      st.update(l, r, val);
    }); */
  }
  // 8
  // O(lg^2 (N))
  T query(int u, int v) {
    T ans = 0;
    lca(u, v, [&](int l, int r) {
      ans = F(ans, st.query(l, r));
    });
    return ans;
  }
  // 9
  // O(lg(N))
  // kth node in the path from u to root
  int kth(int u, int k) {
    for (; stPos[u] - stPos[root[u]] < k;
         u = dad[root[u]])
      k -= stPos[u] - stPos[root[u]] + 1;
    return iStPos[stPos[u] - k];
  }
};

int main() {
  int n;
  scanf("%d", &n);
  vector<vector<int>> adj(n);
  vector<vector<int>> edges(n - 1, vector<int>(3));
  for (int i = 0; i < n - 1; i++) {
    int a, b;
    scanf("%d %d", &a, &b);
    a--, b--;
    adj[a].push_back(b);
    adj[b].push_back(a);
  }
  HLD<int> hld(adj, 0, 0, [](int a, int b) {return (((a) > (b))? (a) : (b)); });
  int q;
  scanf("%d", &q);
  while (q--) {
    char op[2];
    scanf("%s", op);
    if (op[0] == 'G') {
      int a, b;
      scanf("%d %d", &a, &b);
      cout << hld.query(a - 1, b - 1) << endl;
    } else {
      int i, ti;
      scanf("%d %d", &i, &ti);
      i--;
      hld.update(i, i, ti);
    }
  }
  return 0;
}