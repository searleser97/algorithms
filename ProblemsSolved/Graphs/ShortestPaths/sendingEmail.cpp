#include <bits/stdc++.h>
using namespace std;

// s = source
typedef long long int T;  // sum of costs might overflow
T inf = 1ll << 62;
typedef pair<T, int> DistNode;
vector<vector<int>> adj;
vector<vector<T>> cost;

void init(int N) {
  adj.assign(N, vector<int>());
  cost.assign(N, vector<T>());
}

// Assumes Directed Graph
void addEdge(int u, int v, T c) { adj[u].push_back(v), cost[u].push_back(c); }

// ~ O(E * lg(V))
vector<T> dijkstra(int s) {
  vector<T> dist(adj.size(), inf);
  priority_queue<DistNode> q;
  q.push({0, s}), dist[s] = 0;
  while (q.size()) {
    DistNode top = q.top();
    q.pop();
    int u = top.second;
    if (dist[u] < -top.first) continue;
    for (int i = 0; i < adj[u].size(); i++) {
      int v = adj[u][i];
      T d = dist[u] + cost[u][i];
      if (d < dist[v]) q.push({-(dist[v] = d), v});
    }
  }
  return dist;
}

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);
  int Te;
  cin >> Te;
  for (int l = 1; l <= Te; l++) {
    int n, m, s, t, a, b;
    double w;
    cin >> n >> m >> s >> t;
    init(n);
    while (m--) {
      cin >> a >> b >> w;
      addEdge(a, b, w);
      addEdge(b, a, w);
    }
    auto dist = dijkstra(s);

    cout << "Case #" << l << ": ";
    if (dist[t] == inf) {
      cout << "unreachable" << endl;
      continue;
    }

    cout << dist[t] << endl;
  }
  return 0;
}