#include <bits/stdc++.h>
using namespace std;
// 3
// indeg0 = indegree 0
vector<vector<int>> adj;
vector<int> indegree, toposorted;
// 4
void init(int n) {
  adj.assign(n, vector<int>());
  indegree.assign(n, 0), toposorted.clear();
}
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  indegree[v]++;
}
// 15
// O(V * lg(V) + E)
bool toposort() {
  set<int> indeg0;
  for (int u = 0; u < adj.size(); u++)
    if (!indegree[u]) indeg0.insert(u);
  int cnt = 0;
  while (indeg0.size()) {
    auto u = indeg0.begin();
    toposorted.push_back(*u);
    for (auto& v : adj[*u])
      if (!--indegree[v]) indeg0.insert(v);
    cnt++, indeg0.erase(u);
  }
  return cnt < adj.size() ? false : true;
}

int main() {
  int n, m;
  cin >> n >> m;
  init(n);
  while (m--) {
    int x, y;
    cin >> x >> y;
    x--, y--;
    addEdge(x, y);
  }
  if (toposort()) {
    for (int i = 0; i < toposorted.size(); i++) {
      if (i) cout << " ";
      cout << toposorted[i] + 1;
    }
    cout << endl;
  } else {
    cout << "Sandro fails." << endl;
  }
} 
