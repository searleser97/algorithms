#include <bits/stdc++.h>
using namespace std;

void printMat(vector<vector<int>>& v) {
  for (auto& r : v) {
    for (auto& c : r) { cout << c << " "; }
    cout << endl;
  }
}

// int editDistance(string& s, string& t, int i, int j, vector<vector<int>> & dp) {

//   if (i < 0 && j < 0) return 0;

//   if (i < 0) {
//     int cost = 0;
//     for (int i = 0; i < s.size(); i++) cost += s[i] - 'a' + 1;
//     return cost;
//   }
//   if (j < 0) {
//     int cost = 0;
//     for (int i = 0; i < t.size(); i++) cost += t[i] - 'a' + 1;
//     return cost;
//   }
//   if (dp[i][j] != -1) return dp[i][j];
//   int replace = editDistance(s, t, i - 1, j - 1, dp);
//   int insert = editDistance(s, t, i, j - 1, dp) + t[j] - 'a' + 1;
//   int remove = editDistance(s, t, i - 1, j, dp) + s[i] - 'a' + 1;
//   return dp[i][j] = min(replace + abs(s[i] - t[j]), min(insert, remove));
// }

int editDistance(string& s, string& t) {
  vector<vector<int>> dp(t.size() + 1,
                         vector<int>(s.size() + 1));
  for (int i = 1; i <= s.size(); i++) dp[0][i] = dp[0][i - 1] + s[i - 1] - 'a' + 1;
  for (int i = 1; i <= t.size(); i++) dp[i][0] = dp[i - 1][0] + t[i - 1] - 'a' + 1;
  for (int i = 0; i < t.size(); i++)
    for (int j = 0; j < s.size(); j++)
      dp[i + 1][j + 1] =
          min(dp[i][j] + abs(t[i] - s[j]),
              min(dp[i][j + 1] + t[i], dp[i + 1][j] + s[j]) - 'a' + 1);
  return dp[t.size()][s.size()];
}

int main() {
  
  string s, t;
  cin >> s >> t;
  cout << editDistance(s, t) << endl;
}
