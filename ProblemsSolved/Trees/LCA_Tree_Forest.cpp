// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 6
// st = sparse table, Arith = Arithmetic
template <class T>
struct SparseTable {
  vector<vector<T>> st;
  function<T(T, T)> F;
  SparseTable() {}
  // 12
  // O(N * lg(N))
  SparseTable(vector<T> &v,
      T f(T, T) = [](T a, T b) { return a + b; })
      : F(f) {
    st.assign(1 + log2(v.size()),
              vector<T>(v.size()));
    st[0] = v;
    for (int i = 1; (1 << i) <= v.size(); i++)
      for (int j = 0; j + (1 << i) <= v.size(); j++)
        st[i][j] = F(st[i - 1][j],
                     st[i - 1][j + (1 << (i - 1))]);
  }
  // 5
  // O(1), [l, r]
  T query(int l, int r) {
    int i = log2(r - l + 1);
    return F(st[i][l], st[i][r + 1 - (1 << i)]);
  }
  // 12
  // O(lg(N)), [l, r]
  T queryArith(int l, int r) {
    T ans = 0; // neutral value
    while (true) {
      int k = log2(r - l + 1);
      ans = F(ans, st[k][l]);
      l += 1 << k;
      if (l > r) break;
    }
    return ans;
  }
};
// 7
// p = parent
//#include "../Ranges/Data Structures/Sparse Table.cpp"
typedef pair<int, int> pairii;
vector<int> firstPos;
vector<pair<int, int>> tour;
vector<vector<int>> adj;
SparseTable<pairii> st;
vector<int> findRoot;
void init(int N) { adj.assign(N, vector<int>()); findRoot.resize(N); }
// 4
void addEdge(int u, int v) {
  adj[u].push_back(v);
  adj[v].push_back(u);
}
// 10
// O(N)
void eulerTour(int root, int u, int p, int h) {
  findRoot[u] = root;
  firstPos[u] = tour.size();
  tour.push_back({h, u});
  for (int v : adj[u])
    if (v != p) {
      eulerTour(root, v, u, h + 1);
      tour.push_back({h, u});
    }
}
// 10
// O(N * lg(N))
void preprocess(const vector<int> &roots) {
  tour.clear();
  firstPos.assign(adj.size(), -1);
  for (auto &root : roots) eulerTour(root, root, -1, 0);
  st = SparseTable<pairii>(
      tour, [](pairii a, pairii b) {
        return a.first < b.first ? a : b;
      });
}
// 6
// O(1)
int lca(int u, int v) {
  int l = min(firstPos[u], firstPos[v]);
  int r = max(firstPos[u], firstPos[v]);
  return st.query(l, r).second;
}
// 3
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  debug(n, m);
  init(n);

  rep(m) {
    int u, v;
    cin >> u >> v;
    debug(u, v);
    addEdge(u, v);
  }
  int rootcnt = n - m;
  debug(rootcnt);
  vector<int> roots(rootcnt);
  rep(rootcnt) {
    cin >> roots[i];
  }

  preprocess(roots);

  int q;
  cin >> q;
  rep(q) {
    int u, v;
    cin >> u >> v;
    if (findRoot[u] == findRoot[v]) {
      cout << lca(u, v) << endl;
    } else {
      cout << -1 << endl;
    }
  }

}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}

