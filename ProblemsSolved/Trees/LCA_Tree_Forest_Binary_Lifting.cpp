// time-limit: 3000
// 24
#include <bits/stdc++.h>
using namespace std;
#define coutc "\033[48;5;196m\033[38;5;15m"
#define endc "\033[0m"
#define endl '\n'
#define M(_1, _2, _3, _4, NAME, ...) NAME
#define rep(...) M(__VA_ARGS__, rep4, rep3, rep2, rep1)(__VA_ARGS__)
#define rep4(_, x, n, s) for (int _ = x; (s < 0) ? _ > n : _ < n; _ += s)
#define rep3(_, x, n) rep4(_, x, n, (x < n ? 1 : -1))
#define rep2(_, n) rep3(_, 0, n)
#define rep1(n) rep2(i, n)
#define fi first
#define se second
#define pb push_back
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define allr(x) (x).rbegin(), (x).rend()
#define len(x) int((x).size())
#define lli __int128_t
#define li long long int
#define ld long double
#ifdef DEBUG
// 3
ostream &operator<<(ostream &o, pair<auto, auto> p) {
  return o << "(" << p.fi << ", " << p.se << ")";
}
// 4
ostream &operator<<(ostream &o, vector<auto> v) {
  rep(i, len(v)) o << setw(7) << right << v[i];
  return o << endc << endl << coutc;
}
// 4
ostream &operator<<(ostream &o, map<auto, auto> m) {
  for (auto &kv : m) o << kv;
  return o;
}
// 7
void debug(const auto &e, const auto &...r) {
  cout << coutc << e;
  ((cout << " " << r), ..., (cout << endc << endl));
}
#else
#define debug(...)
#endif
// 6
// tin = time in, tout = time out, p = parent
int Time, logn;
vector<vector<int>> adj, up;
vector<int> tin, tout, findRoot;
void init(int N) { adj.assign(N, vector<int>()); findRoot.resize(N); }
// 3
void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 8
void dfs(int u, int p, int root) {
  tin[u] = ++Time, up[u][0] = p;
  findRoot[u] = root;
  for (int i = 1; i <= logn; ++i)
    up[u][i] = up[up[u][i - 1]][i - 1];
  for (int v : adj[u])
    if (v != p) dfs(v, u, root);
  tout[u] = ++Time;
}
// 7
// O(lg(N))
void preprocess(const vector<int> &roots) {
  Time = 0, logn = ceil(log2(adj.size()));
  tin = tout = vector<int>(adj.size());
  up.assign(adj.size(), vector<int>(logn + 1));
  for (auto &root : roots) dfs(root, root, root);
}
// 3
bool isAncestor(int u, int v) {  // is u ancestor of v
  return tin[u] <= tin[v] && tout[u] >= tout[v];
}
// 8
// O(lg(N))
int lca(int u, int v) {
  if (isAncestor(u, v)) return u;
  if (isAncestor(v, u)) return v;
  for (int i = logn; ~i; --i)
    if (!isAncestor(up[u][i], v)) u = up[u][i];
  return up[u][0];
}
// 3
void _main(int tc) {
  int n, m;
  cin >> n >> m;
  debug(n, m);
  init(n);

  rep(m) {
    int u, v;
    cin >> u >> v;
    debug(u, v);
    addEdge(u, v);
  }
  int rootcnt = n - m;
  debug(rootcnt);
  vector<int> roots(rootcnt);
  rep(rootcnt) {
    cin >> roots[i];
  }

  preprocess(roots);

  int q;
  cin >> q;
  rep(q) {
    int u, v;
    cin >> u >> v;
    if (findRoot[u] == findRoot[v]) {
      cout << lca(u, v) << endl;
    } else {
      cout << -1 << endl;
    }
  }

}
// 7
int main() {
  ios_base::sync_with_stdio(0), cin.tie(0);
  _main(0), exit(0);
  int tc;
  cin >> tc;
  rep(i, tc) _main(i + 1);
}

