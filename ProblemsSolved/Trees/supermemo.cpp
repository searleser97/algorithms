#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <queue>
#include <deque>
#include <bitset>
#include <iterator>
#include <list>
#include <stack>
#include <map>
#include <set>
#include <functional>
#include <numeric>
#include <utility>
#include <limits>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <iomanip>
using namespace std;
// 3
// su = sum update, ru = replace update, rev = reverse
// psu = pending sum update, sz = size
// pru = pending replace update
// 8
template <class T>
struct ImplicitTreap {
  struct Node {
    Node *l, *r;  // left, right
    T val, minim/*, sum*/, su, ru;
    long long int prior, sz, rev, psu, pru;
    Node(T e, int p = rand()) : val(e), prior(p) {
      l = NULL, r = NULL;
      minim = (1 << 30)/*, sum = 0*/, su = 0;
      sz = 1, rev = 0, psu = 0, pru = 0;
    }
    Node() { l = NULL, r = NULL ;}
  };
  // 2
  ImplicitTreap() : root(NULL) { root = NULL; }
  Node *root;
  // 13
  // O(1)
  void pull(Node *t) {
    if (!t) return;
    t->sz = 1, /*t->sum = t->val, */t->minim = t->val;
    if (t->l) {
      t->sz += t->l->sz/*, t->sum += t->l->sum*/;
      t->minim = min(t->minim, t->l->minim);
    }
    if (t->r) {
      t->sz += t->r->sz/*, t->sum += t->r->sum*/;
      t->minim = min(t->minim, t->r->minim);
    }
  }

  void applyAdd(Node *t, T su) {
    if (!t) return;
    t->val += su, t->psu = 1, t->su += su;
    /*t->sum += su * t->sz, */t->minim += su;
  }

  void applyRev(Node *t) {
    if (!t) return;
    t->rev ^= 1, swap(t->l, t->r);
  }

  void push(Node *t) {
    if (!t) return;
    if (t->psu) {
      applyAdd(t->l, t->su), applyAdd(t->r, t->su);
      t->psu = t->su = 0;
    }
    if (t->rev) {
      applyRev(t->l), applyRev(t->r);
      t->rev = 0;
    }
  }
  // 16
  // O(lg(N)), first <= idx < second
  pair<Node *, Node *> split(Node *t, int idx,
                             int cnt = 0) {
    if (!t) return {NULL, NULL};
    push(t);
    Node *left, *right;
    int idxt = cnt + (t->l ? t->l->sz : 0);
    if (idx < idxt) {
      pair<Node*, Node*> aux = split(t->l, idx, cnt);
      left = aux.first, t->l = aux.second, right = t;
    }
    else {
      pair<Node*, Node*> aux = split(t->r, idx, idxt + 1);
      t->r = aux.first, right = aux.second, left = t;
    }
    pull(t);
    return {left, right};
  }
  // 11
  // O(lg(N))
  void insert(Node *&t, Node *v, int idxv, int cnt) {
    int idxt = t ? cnt + (t->l ? t->l->sz : 0) : 0;
    push(t);
    if (!t) t = v;
    else if (v->prior > t->prior) {
      pair<Node*, Node*> aux = split(t, idxv, cnt);
      v->l = aux.first, v->r = aux.second, t = v;
    }
    else {
      cnt = idxv < idxt ? cnt : idxt + 1;
      insert(idxv < idxt ? t->l : t->r, v, idxv, cnt);
    }
    pull(t);
  }
  // 4
  // O(lg(N)), insert element in i-th position
  void insert(T e, int i) {
    insert(root, new Node(e), i - 1, 0);
  }
  // 11
  // O(lg(N)) asumes a.indexes < b.indexes
  Node *merge(Node *a, Node *b) {
    push(a), push(b);
    Node *ans;
    if (!a || !b) ans = a ? a : b;
    else if (a->prior > b->prior)
      a->r = merge(a->r, b), ans = a;
    else b->l = merge(a, b->l), ans = b;
    pull(ans);
    return ans;
  }
  // 10
  // O(lg(N))
  void erase(Node *&t, int i, int cnt = 0) {
    if (!t) return;
    push(t);
    int idxt = cnt + (t->l ? t->l->sz : 0);
    cnt = i < idxt ? cnt : idxt + 1;
    if (idxt == i) t = merge(t->l, t->r);
    else erase(i < idxt ? t->l : t->r, i, cnt);
    pull(t);
  }
  // 2
  // O(lg(N)), erase element at i-th position
  void erase(int i) { erase(root, i); }
  // 4
  // O(lg(N))
  void push_back(T e) {
    root = merge(root, new Node(e));
  }
  // 7
  // O(lg(N))
  // template<class Op>
  // void op(int l, int r, Op f) {
  //   Node *a, *b, *c;
  //   tie(a, b) = split(root, l - 1);
  //   tie(b, c) = split(b, r - l);
  //   f(b), root = merge(a, merge(b, c));
  // }
  // 4
  // O(lg(N)), reverses [l, ..., r]
  void reverse(int l, int r) {
    // op(l, r, [&](Node *&t) { t->rev ^= 1; });
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    applyRev(b);
    root = merge(a, merge(b, c));
  }
  // 9
  // O(lg(N)), rotates [l, ..., r] k times
  void rotate(int l, int r, int k) {
    int len = r - l + 1;
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    Node *left, *right;
    k %= len;
    aux = split(b, len - k - 1);
    left = aux.first, right = aux.second;
    b = merge(right, left);
    root = merge(a, merge(b, c));
  }
  // 5
  // O(lg(N)), adds val to [l, ..., r]
  void add(int l, int r, T val) {
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    applyAdd(b, val);
    root = merge(a, merge(b, c));
  }
  // 5
  // O(lg(N)), sets val to [l, ..., r]
  void replace(int l, int r, T val) {
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    b->pru = 1, b->ru = val;
    root = merge(a, merge(b, c));
  }
  // 6
  // O(lg(N)), minimum in [l, ..., r]
  T getMin(int l, int r) {
    // print(root);
    T ans;
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    ans = b->minim;
    // print(b);
    root = merge(a, merge(b, c));
    return ans;
  }
  // 6
  // O(lg(N)), sum in [l, ..., r]
  T getSum(int l, int r) {
    T ans;
    Node *a, *b, *c;
    pair<Node*, Node*> aux = split(root, l - 1);
    a = aux.first, b = aux.second;
    aux = split(b, r - l);
    b = aux.first, c = aux.second;
    ans = b->sum;
    root = merge(a, merge(b, c));
    return ans;
  }
  // 8
  // O(N)
  void print(Node *t, bool a) {
    if (!t) return;
    // push(t);
    print(t->l, false);
    cout << setfill(' ') << setw(4) << left << t->val;
    print(t->r, false);
  }

  void print(string s, Node *t, bool isleft) {
    if (!t) return;
    cout << s << (isleft ? "|__" : "\\__");
    cout << t->val << "~" << t->prior << endl;
    print(s + (isleft ? "|   " : "    "), t->l, 1);
    print(s + (isleft ? "|   " : "    "), t->r, 0);
  }
  // 2
  void print(Node *t) {
    for (int i = 0; i < t->sz; i++)
      cout << setfill(' ') << setw(4) << left << i;
    cout << endl;
    print(t, false); cout << endl;
    print("", t, false);
  }
};

int main() {
  ios_base::sync_with_stdio(0); cin.tie(0);
  int n, x;
  cin >> n;
  ImplicitTreap<int> t;
  for (int i = 0; i < n; i++) {
    cin >> x;
    t.push_back(x);
  }
  
  int m;
  cin >> m;
  while(m--) {
    string opt;
    cin >> opt;
    int l, r, v;
    if (opt[0] == 'A') {
      cin >> l >> r >> v;
      t.add(l - 1, r - 1, v);
    } else if (opt[0] == 'R' && opt[3] == 'E') {
      cin >> l >> r;
      t.reverse(l - 1, r - 1);
    } else if (opt[0] == 'R' && opt[3] == 'O') {
      cin >> l >> r >> v;
      t.rotate(l - 1, r - 1, v);
    } else if (opt[0] == 'I') {
      cin >> l >> v;
      t.insert(v, l);
    } else if (opt[0] == 'D') {
      cin >> v;
      t.erase(v - 1);
    } else {
      cin >> l >> r;
      cout << t.getMin(l - 1, r - 1) << endl;
    }
  }
  return 0;
}