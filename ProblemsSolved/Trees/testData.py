from random import randint, choice

N = 10
M = 6

A = [randint(1, 100) for _ in range(N)]
print(N)
print(' '.join(map(str, A)))

cmds = ['ADD', 'REVERSE', 'REVOLVE', 'INSERT', 'DELETE', 'MIN']
# cmds = ['INSERT', 'REVOLVE', 'MIN']

print(M)
for _ in range(M - 1):
    cmd = choice(cmds)

    if cmd == 'ADD':
        x = randint(1, N)
        y = randint(x, N)
        k = randint(1, 3)
        print(cmd, x, y, k)
    if cmd == 'REVERSE':
        x = randint(1, N)
        y = randint(x, N)
        print(cmd, x, y)
    if cmd == 'REVOLVE':
        x = randint(1, N)
        y = randint(x, N)
        k = randint(1, (y - x + 1))
        print(cmd, x, y, k)
    if cmd == 'INSERT':
        x = randint(1, N)
        val = randint(0, 10)
        print(cmd, x, val)
    if cmd == 'DELETE':
        x = randint(1, N)
        print(cmd, x)
        N -= 1
    if cmd == 'MIN':
        x = randint(1, N)
        y = randint(x, N)
        print(cmd, x, y)

print('MIN 1 {}'.format(N))