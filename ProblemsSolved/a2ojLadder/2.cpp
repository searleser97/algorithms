#include <bits/stdc++.h>
using namespace std;

int main() {
	int x, y;
	for (int i = 0; i < 5; i++) {
		bool flag = 0;
		for (int j = 0; j < 5; j++) {
			cin >> x;
			if (x == 1) {
				y = i, x = j;
				flag = true;
				break;
			}
		}
		if (flag) break;
	}
	int ans = abs(2 - y) + abs(2 - x);
	cout << ans << endl;
}
